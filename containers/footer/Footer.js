import React from 'react';
import Link from 'next/link';
import { useSelector, useDispatch } from 'react-redux';
import { is_Spinner_loading_true } from '../../store/spinner/action';
import { useRouter } from 'next/router';
const Footer = ({menu_text_color}) => {
  const dispatch = useDispatch();
  const footer = useSelector(state => state.global_locale.selected_lang.footer)
  const router = useRouter();
  const { pathname } = router;
  const startSpinner = (route) => {
    if(route !== pathname) {
      dispatch(is_Spinner_loading_true())
    }
    return;
  }
  return (
    <footer className="footer row mx-auto px-auto">
      <div className="col-12 mx-auto px-0 px-sm-3">
        <div className="footer-inner">
          <Link href="/politique">
            <a onClick={() => startSpinner('/politique')}>
              <span style={{color: menu_text_color}}>{footer && footer.link_1}</span>
            </a>
          </Link>

          <Link href="/termes-et-conditions">
            <a onClick={() => startSpinner('/termes-et-conditions')}>
              <span style={{color: menu_text_color}}>{footer && footer.link_2}</span>
            </a>
          </Link> 
          <span  className="small-footer font-weight-lighter footer-span" style={{color:`${menu_text_color}`}} >{footer && footer.link_3}</span>
        
        </div>
      </div>
    </footer>
  );
};

export default Footer;
