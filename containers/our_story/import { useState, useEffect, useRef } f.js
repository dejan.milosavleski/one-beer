import { useState, useEffect, useRef } from 'react';
import ItemsCarousel from 'react-items-carousel';
import Link from 'next/link';
import { useWindowSize } from '../hooks/UseWindowSize';
import arrow_white from '../styles/assets/icons/arrow-white.svg';
import { motion } from 'framer-motion';

const Carousel_Cont = ({ images }) => {
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const [beers, setBeers] = useState([]);
  const windowSize = useWindowSize();
  const chevronWidth = 40;
  const changeActiveItem = activeIndex => setActiveItemIndex(activeIndex);
  useEffect(() => {
    setBeers(images);
  }, []);

  useEffect(() => {
    if (activeItemIndex === beers.length - 1) {
      let arr = [];
      images.forEach((image, index) => {
        arr.push(image);
      });
      setBeers(prevState => [...prevState, ...arr]);
    }
    if (activeItemIndex < 0) {
      let arr = [];
      images.forEach((image, index) => {
        arr.push(image);
      });
      setBeers(prevState => [...arr, ...prevState]);
    }
  }, [activeItemIndex]);
  const chevronVariantsRight = {
    initial: { x: '100%', opacity: 0 },
    enter: { x: 0, opacity: 1, transition: { duration: `1` } },
    exit: { x: '-100%', opacity: 0, transition: { duration: `1` } },
  };
  const chevronVariantsLeft = {
    initial: { x: '100%', opacity: 0, rotate: '180deg' },
    enter: {
      x: 0,
      opacity: 1,
      transition: { duration: `1` },
      rotate: '180deg',
    },
    exit: {
      x: '-100%',
      opacity: 0,
      transition: { duration: `1` },
      rotate: '180deg',
    },
  };

  return (
    <>
      {images && (
        <ItemsCarousel
          requestToChangeActive={changeActiveItem}
          activeItemIndex={activeItemIndex}
          numberOfCards={
            windowSize.width <= 576 ? 3 : windowSize.width <= 768 ? 3 : 4
          }
          // onActiveStateChange={handleState}
          activePosition={'center'}
          infiniteLoop={true}
          gutter={0}
          leftChevron={
            activeItemIndex > 0 || windowSize.width < 576 ? (
              <motion.i
                variants={chevronVariantsLeft}
                className="arrow-white-left-icon"
              />
            ) : null
          }
          rightChevron={
            <motion.i
              variants={chevronVariantsRight}
              className="arrow-white-right-icon"
            />
          }
          outsideChevron
          chevronWidth={chevronWidth}
          alwaysShowChevrons={true}
          classes={{
            wrapper: 'wrapper',
            itemsWrapper: 'itemsWrapper',
            itemsInnerWrapper: 'itemsInnerWrapper',
            itemWrapper: 'itemWrapper',
            rightChevronWrapper: 'chevron chevron_right',
            leftChevronWrapper: 'chevron chevron_left',
          }}
        >
          {beers.map((beer, index) => (
            <motion.div
              key={index * 6432}
              variants={{
                initial: { x: '370%', opacity: 0 },
                enter: {
                  x: 0,
                  opacity: 1,
                  transition: { duration: `1.${index}` },
                },
                exit: {
                  x: '-370%',
                  opacity: 0,
                  transition: { duration: `1.${index}` },
                },
              }}
              key={index * 4783}
              className="d-flex flex-column"
            >
              <section
                className={`img-wrapper ${
                  activeItemIndex === index - 1 ||
                  (activeItemIndex < 0 &&
                    activeItemIndex + beers.length - 1 === index - 2)
                    ? 'active-wrapper'
                    : ''
                }`}
              >
                <Link href={`/${beer.title}`}>
                  <a className="beerWrapper">
                    <img src={beer.url} alt={beer.url} />
                  </a>
                </Link>
              </section>
            </motion.div>
          ))}
        </ItemsCarousel>
      )}
    </>
  );
};
export default Carousel_Cont;
