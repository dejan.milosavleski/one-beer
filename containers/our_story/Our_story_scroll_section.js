import { useState, useRef, Fragment } from 'react';
import Arrow_down from '../../components/Arrow_down';
import { uniqueId } from 'lodash';
import { replaceBrTag } from '../../hooks/useBrTag';
import { motion } from 'framer-motion';
// import {
//   blogVariants,
//   postPreviewVariants,
// } from '../../constants/transitions_Variants';

const Our_story_scroll_section = ({
  content,
  content_className,
  main_title,
}) => {
  const [scrollStart, setScrollStart] = useState(0);
  const contentRef = useRef();
  const handleScroll = () => {
    const pageYOffset = contentRef.current.scrollTop;

    setScrollStart(pageYOffset);
  };
  const transition = { duration: 0.7, ease: 'easeInOut' };
  const blogVariants = {
    enter: { transition: { staggerChildren: 0.2 } },
    exit: { transition: { staggerChildren: 0.2 } },
  };
  const postPreviewVariants = {
    initial: { y: 40, opacity: 0 },
    enter: { y: 0, opacity: 1, transition },
    exit: { y: 40, opacity: 0, transition },
  };
  const mainTitle = {
    initial: {  opacity: 0 },
    enter: { opacity: 1, transition },
    exit: { opacity: 0, transition }
  };

  return (
    <>
      {content && (
        <motion.div
          ref={contentRef}
          className={`content ${content_className}`}
          onScroll={handleScroll}
          initial="initial"
          animate="enter"
          exit="exit"
          key={`${content_className}`+4125}
          variants={blogVariants}
        >
          <motion.h1   key={`${main_title}`+ 94235} variants={mainTitle} className="mb-4">
            {main_title}
          </motion.h1>
          <motion.div
             key={`${content_className}`+4325}
            className="inside-div"
            variants={postPreviewVariants}
          >
            {content.map((section, index) => (
              <Fragment  key={index + uniqueId()}>
                {section.title &&
                  section.title.map(ttl => (
                    <h6
                      key={`${ttl}`+ uniqueId()}
                      variants={postPreviewVariants}
                      className="text-white font-weight-light my-4"
                    >
                      {ttl}
                    </h6>
                  ))}

                {section.text &&
                  section.text.map((txt, inx) => (
                  <Fragment  key={`${content_className}` + uniqueId()}>
                      <p  key={uniqueId()}>
                      {' '}
                      {replaceBrTag(txt, '<br/>', <br />)}
                    </p>
                  </Fragment>
                  ))}
              </Fragment>
            ))}
          </motion.div>
        </motion.div>
      )}
      <div style={{ opacity: scrollStart > 0 ? '1' : 0 }}>
        <Arrow_down className="font-weight-lighter" color="white" link="#" />
      </div>
    </>
  );
};

export default Our_story_scroll_section;
