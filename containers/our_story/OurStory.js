import { Social_icons } from '../../components/Social_icons';
import Our_story_scroll_section from './Our_story_scroll_section';
import ilustration_pic from '../../styles/assets/images/ILLUSTRATION/ILLUSTRATION-FOND_BLONDE.gif';
import Footer from '../footer/Footer';
import GlobalFlags from '../../components/GlobalFlags';
import ScrollButton from '../../components/ScrollButton';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { useRouter } from 'next/router';
import { motion } from 'framer-motion';
import { blogVariants, postPreviewVariants} from '../../constants/transitions_Variants';

const OurStory = ({ content, content_className, main_title }) => {
  const router = useRouter();
  const windowSize = useWindowSize();

  return (
    <div
    
     
      className="container big-container our-story"
      style={{overflowX: "hidden", position: "relative"}}
    >
      <div className="row mx-0 my-auto" >
        <div className="col-12 col-md-7 px-0 px-md-3">
          <Our_story_scroll_section
            content={content}
            content_className
            content_className={content_className}
            main_title={main_title}
          />
        </div>

        <div className="img-div">
          <img src={ilustration_pic} alt="ilustration_pic" />
        </div>
      </div>
      {windowSize.width && windowSize.width <= 576 && <ScrollButton />}
      <GlobalFlags />
      <Social_icons />
      <Footer menu_text_color="white" />
    </div>
  );
};

export default OurStory;
