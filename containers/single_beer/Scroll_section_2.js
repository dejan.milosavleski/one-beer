import React from 'react';
import Arrow_down from '../../components/Arrow_down';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { replaceBrTag } from '../../hooks/useBrTag';

const Scroll_section_2 = ({ fruit, section_2 }) => {
  const { header, text } = section_2;
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const { arrow_down_text } = locale;
  const router = useRouter();
  const { pathname } = router;
  const windowSize = useWindowSize();
  return (
    <div className="scroll_section_2">
      <div className="row h-100 mx-auto">
        <div className="col-12 d-flex flex-column justify-content-center align-items-center">
          <div className="text-div">
            {pathname === '/peche' ? (
              <i
                className={`icon beer-name ${
                  windowSize && windowSize.width <= 768
                    ? 'onebeer-peche-w-icon'
                    : 'beerName_peche-black-icon'
                } peche-style`}
              ></i>
            ) : pathname === '/rose' ? (
              <i
                className={`icon beer-name ${
                  windowSize && windowSize.width <= 768
                    ? 'beerName_rose-black-icon'
                    : 'beerName_rose-black-icon'
                } peche-style`}
              ></i>
            ) : (
              <h1>{header}</h1>
            )}

            <p className="mx-auto">{replaceBrTag(text, '<br/>', <br />)}</p>
          </div>
          {/* <div className="img-div">
            <div className="inner-img mx-auto mb-3">
              <img src={fruit} alt="fruit" />
            </div>
          </div> */}

          <Arrow_down
            arrow_down_text={arrow_down_text}
            link={`${pathname}#sectionThree`}
          />
        </div>
      </div>
    </div>
  );
};

export default Scroll_section_2;
