import { useDispatch } from 'react-redux';
import { change_lang } from '../../store/single_beer_locale/action';
const Flags = () => {
  const dispatch = useDispatch();

  const changeLang = lang => {
    dispatch(change_lang(lang));
  };
  return (
    <div className="flags d-flex">
      <i className="icon fr_flag-icon" onClick={() => changeLang('fr')} />
      <i className="icon gb_flag-icon" onClick={() => changeLang('gb')} />
      <i className="icon es_flag-icon" onClick={() => changeLang('es')} />
      <i className="icon de_flag-icon" onClick={() => changeLang('de')} />
      <i className="icon nl_flag-icon" onClick={() => changeLang('nl')} />
      <i className="icon ru_flag-icon" onClick={() => changeLang('ru')} />
      <i className="icon cn_flag-icon" onClick={() => changeLang('cn')} />
    </div>
  );
};

export default Flags;
