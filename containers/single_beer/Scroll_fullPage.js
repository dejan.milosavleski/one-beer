import { useState, useEffect, useRef } from 'react';
import { SectionsContainer, Section,  } from 'react-fullpage';
import Scroll_section_1 from './Scroll_section_1.js';
import Scroll_section_2 from './Scroll_section_2';
import Scroll_section_3 from './Scroll_section_3';
import Scroll_section_4 from './Scroll_section_4';
import Scroll_section_5 from './Scroll_section_5';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { usePageY_Offset } from '../../hooks/UsePageY_Offset';
import { useDimensions } from '../../hooks/useDimensions';
import { isEmpty } from 'lodash'
const Scroll_fullPage = ({
  logo,
  puces_1,
  puces_2,
  fruit,
  anchors,
  ilustration,
  text_pic,
  border_color,
  section_1,
  section_2,
  section_3,
  section_4,
  section_5,
  beer_name,
  menu_text,
}) => {
  const windowSize = useWindowSize();
  const myRef = useRef();


  useEffect(() => {
    if (windowSize && windowSize.width > 767) {
      const anchors = document.querySelectorAll('.active');
        const hashchange = window.location.hash;
    
      if (anchors &&  isEmpty(hashchange)) {
        document
          .querySelectorAll('.Navigation-Anchor')[0]
          .classList.add('active');
      } else {
        return;
      }
    }
  }, [windowSize]);


 
  
  const pageY = usePageY_Offset();
  let options = {
    sectionClassName: 'section',
    anchors: anchors,
    navigation: true,
    verticalAlign: false,
    delay: 700,
    sectionPaddingTop: '50px',
    sectionPaddingBottom: '50px',
    arrowNavigation: true,
    scrollBar: false,
  };

  return (
    <>
      <div
        className="img-ilustration"
        style={{
          backgroundImage: `url(${ilustration})`,
          visibility:
            windowSize && windowSize.width > 767
              ? 'visible'
              : pageY >= windowSize.height - 200
              ? 'visible'
              : 'hidden',
        }}
      />
      {windowSize &&
        windowSize.width <= 767 && (
          <>
            <Scroll_section_1
              logo={logo}
              border_color={border_color}
              section_1={section_1}
              beer_name={beer_name}
            />
            <Scroll_section_2 fruit={fruit} section_2={section_2} />
            <Scroll_section_3
              puces_1={puces_1}
              puces_2={puces_2}
              section_3={section_3}
              menu_text={menu_text}
            />
            <Scroll_section_4 section_4={section_4} />
            <Scroll_section_5 text_pic={text_pic} section_5={section_5} />
          </>
        )}
      {windowSize && windowSize.width > 767 && (
        <SectionsContainer {...options} ref={myRef}>
          <Section>
            <Scroll_section_1
              logo={logo}
              border_color={border_color}
              section_1={section_1}
              beer_name={beer_name}
            />
          </Section>

          <Section>
            <Scroll_section_2 fruit={fruit} section_2={section_2} />
          </Section>

          <Section >
            <Scroll_section_3
              puces_1={puces_1}
              puces_2={puces_2}
              section_3={section_3}
            />
          </Section>

          <Section>
            <Scroll_section_4 section_4={section_4} />
          </Section>

          <Section>
            <Scroll_section_5 text_pic={text_pic} section_5={section_5} />
          </Section>
        </SectionsContainer>
      )}

    </>
  );
};

export default Scroll_fullPage;
