import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { Field, Form, reduxForm, reset } from 'redux-form';
import { useDispatch, useSelector } from 'react-redux';
import { inputField } from '../../components/Fields';
import { required, required_gb, email, email_gb } from '../../utills/validator';
import {
  is_Spinner_loading_true,
  is_Spinner_loading_false,
} from '../../store/spinner/action';
import Cookies from 'js-cookie';
import Toast from '../../components/Toast';
import { useWindowSize } from '../../hooks/UseWindowSize';
const Scroll_section_5_form = ({ handleSubmit, placeholder, link }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const lang = Cookies.get('lang');
  const windowSize = useWindowSize();
  const contact = useSelector(
    state => state.global_locale.selected_lang.contact
  );
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState('');

  const sendMail = async data => {
    try {
      const response = await fetch('/api/scroll_section5_form', {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(data),
      });
      const res = await response.json();
      if (res) {
      
        setShowToast(true);
        setTimeout(() => {
          setToast(res.message);
        },500)
        dispatch(is_Spinner_loading_false());
      }
    } catch (error) {
      setToast(error);
      dispatch(is_Spinner_loading_false());
    }
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      setToast('');
      setShowToast(false);
    }, 3000);

    return () => clearTimeout(timeout);
  }, [toast]);


  const submitForm = values => {
    sendMail(values);
    dispatch(is_Spinner_loading_true());
    dispatch(reset('Scroll_section_5_form'));
  };

  return (
    <>
     
      <div className="display-toast" style={{display: showToast ? "block" : "none"}}>
        <Toast message={toast} right_px={windowSize && windowSize.width <=576 ? "-9px" : "92px" } />
      </div>
    

      <Form
        noValidate
        className="needs-validation"
        onSubmit={handleSubmit(submitForm)}
      >
        <div className="row mx-auto">
        <Field
          name="email"
          groupClassName="col-12 col-md-6 px-0"
          className="form-control"
          type="email"
          component={inputField}
          placeholder={placeholder}
          id="mail"
          validate={lang === 'fr' ? [required, email] : [required_gb, email_gb]}
        />
        <div className="col-12 col-md-6 px-0">
          <button type="submit" className="btn btn-bolck">
            {link}
            <i className="icon arrow-white-no-bg-icon"></i>
          </button>
        </div>
        </div>
       
      </Form>
    </>
  );
};

export default reduxForm({
  form: 'Scroll_section_5_form', // a unique identifier for this form
})(Scroll_section_5_form);
