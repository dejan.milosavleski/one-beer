import {useState, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { motion } from 'framer-motion';
import { usePageY_Offset } from '../../hooks/UsePageY_Offset';
const Scroll_section_4 = ({ section_4 }) => {
  const windowSize = useWindowSize();
  const { span_color, ul } = section_4;
  const router = useRouter();
  const { pathname } = router;
  const routeName = pathname.substring(1);
  const locale = useSelector(state => state.single_beer_locale);
  const [scrollTransition, setScrollTransition] = useState(false);
  const [contentOffsetTop, setContentOffsetTop] = useState(0);
  const pageYOffset = usePageY_Offset()
  const contentRef = useRef();
 
  useEffect(() => {
    const contentOffsetTop = contentRef.current.offsetTop;
    setContentOffsetTop(contentOffsetTop);
  }, []);
  
  useEffect(() => {
    if(pageYOffset > contentOffsetTop-300){

      setScrollTransition(true)
    }
    
  }, [pageYOffset]);

  useEffect(() => {
    function locationHashChanged() {
      if (location.hash === '#sectionFour') {
        setScrollTransition(true);
      }
    }

    window.addEventListener('hashchange', locationHashChanged);

    return () =>
      window.removeEventListener('hashchange', () =>
        setScrollTransition(false)
      );
  }, [router]);
  
  const container = {
    hidden: { opacity: 0, },
    show: {
      opacity: scrollTransition ? 1 : 0,
     
      transition: {
        delay: 0.1,
        staggerChildren: 0.2,
      },
    },
  };

  const items = {
    hidden: {  opacity:0,  },
    show: { opacity: scrollTransition ? 1 : 0,
    
      transition: { duration: 0.2}
    },
  };
  const buttonItem = {
    hidden: {  opacity:0, x:30 },
    show: { x: scrollTransition ? 0 : 30, opacity: scrollTransition ? 1 : 0,
    
      transition: { duration: 0.7}
    },
  };

  return (
    <div className="scroll_section_4" ref={contentRef}>
      <motion.div className="row w-100 h-100 mx-auto" variants={container}
            initial="hidden"
            animate="show">
        <div className="col-12 d-flex flex-column justify-content-center px-0 px-sm-3">
          <ul  
             className="d-flex flex-column mx-auto ">
            {ul.map((item, index) => (
              <motion.li variants={items} key={index * 434} className="nav-item">
                <section className="d-flex amotion.lign-items-center mt-3">
                  <div className="div-icon">
                    <i className={`icon ${item.icon}`}></i>
                  </div>
                  <div className="d-flex flex-column text-wrapper">
                    {' '}
                    <span className={`${span_color}`}>{item.title}</span>
                    <p>{item.excerpt}</p>
                  </div>
                </section>
              </motion.li>
            ))}
          </ul>
        </div>
     
          <div  className="col-12 d-flex justify-content-center">
            <motion.div variants={buttonItem}>
            <Link
              href={`/pdf_download/${locale.selected_lang.lang}/${routeName}.pdf`}
              target="_blank"
              download
            >
              <a
         
                className="btn btn-outline-dark text-uppercase download-btn"
                target="_blank"
                download
              >
                {locale.selected_lang[routeName] && locale.selected_lang[routeName].section_5 && locale.selected_lang[routeName].section_5.download}
              </a>
            </Link>
            </motion.div>
          </div>
        
      </motion.div>
    </div>
  );
};

export default Scroll_section_4;
