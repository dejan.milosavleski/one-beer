import { useState, useEffect, useRef } from 'react';
import Arrow_down from '../../components/Arrow_down';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { motion } from 'framer-motion';
import { usePageY_Offset } from '../../hooks/UsePageY_Offset';

const Scroll_section_3 = ({ section_3 }) => {
  const { header, ul } = section_3;
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const { arrow_down_text } = locale;
  const router = useRouter();
  const { pathname } = router;
  const windowSize = useWindowSize();
  const actualPathName = pathname.substring(1);
  const [scrollTransition, setScrollTransition] = useState(false);
  const [contentOffsetTop, setContentOffsetTop] = useState(0);
  const pageYOffset = usePageY_Offset();
  const contentRef = useRef();

  useEffect(() => {
    const contentOffsetTop = contentRef.current.offsetTop;
    setContentOffsetTop(contentOffsetTop);
  }, []);

  useEffect(() => {
    if (pageYOffset > contentOffsetTop - 300 && windowSize && windowSize.width <= 576) {
      console.log( 'vleguva');
      setScrollTransition(true);
    }
  }, [pageYOffset]);

  useEffect(() => {
    function locationHashChanged() {
      if (location.hash === '#sectionThree') {
        setScrollTransition(true);
      }
    }

    window.addEventListener('hashchange', locationHashChanged);

    return () =>
      window.removeEventListener('hashchange', () =>
        setScrollTransition(false)
      );
  }, [router]);

  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: scrollTransition ? 1 : 0,

      transition: {
        delay: 0.1,
        staggerChildren: 0.2,
      },
    },
  };

  const items = {
    hidden: { opacity: 0 },
    show: {
      opacity: scrollTransition ? 1 : 0,

      transition: { duration: 0.2},
    },
  };

  return (
    <div className="scroll_section_3" ref={contentRef}>
      <div className="row w-100 h-100 mx-auto">
        <div className="col-12 d-flex flex-column justify-content-center align-items-center">
          <motion.div
            className="text-div"
            variants={container}
            initial="hidden"
            animate="show"
          >
            <motion.h2 variants={items}>{header}</motion.h2>
            <ul className="d-flex flex-column">
              {ul.map((item, index) => (
                <motion.li
                  key={index * 434}
                  className="nav-item"
                  variants={items}
                >
                  <section className="d-flex align-items-center">
                    <i
                      className={`icon puces-${actualPathName}-${index + 1}`}
                    />

                    <div className="text-wrapper">
                      <p>{item}</p>
                    </div>
                  </section>
                </motion.li>
              ))}
            </ul>
          </motion.div>
          <Arrow_down
            arrow_down_text={arrow_down_text}
            link={`${pathname}#sectionTwo`}
          />
        </div>
      </div>
    </div>
  );
};

export default Scroll_section_3;
