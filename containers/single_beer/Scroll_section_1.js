import { useEffect, useState, useRef } from 'react';
import Arrow_down from '../../components/Arrow_down';
import Flags from './Flags';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';

const Scroll_section_1 = ({ logo, border_color, section_1, beer_name}) => {
  const { text, excerpt } = section_1;
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const router = useRouter();
  const { pathname } = router;
  const { arrow_down_text } = locale;
  const spanRef = useRef();
  const [spanHeight, setSpanHeight] = useState();
  useEffect(() => {
    const spanInner = spanRef.current.clientHeight;
    setSpanHeight(spanInner);
  }, [excerpt]);

  return (
    <>
      {section_1 && (
        <div className="scroll_section_1 ">
          <div className="row h-100 mx-auto ">
            <div className="col-12 d-flex flex-column justify-content-center align-items-center">
              {' '}
              <div className="img-div mr-3">
                <img src={logo} alt="fruit" />
              </div>
              <i className={`icon beer-name ${beer_name}`} />
              <div
                className="text-div "
                style={{
                  borderLeft: `1px solid ${border_color}`,
                  borderRight: `1px solid ${border_color}`,
                }}
              >
                <div className="d-flex justify-content-between w-100">
                  <div
                    className="box-up"
                    style={{ borderTop: `1px solid ${border_color}` }}
                  ></div>
                  <div
                    className="box-up"
                    style={{ borderTop: `1px solid ${border_color}` }}
                  ></div>
                </div>
                <div className="py-2 text-wrapper">
                  <p>{text}</p>
                  <span
                    ref={spanRef}
                    className="excerpt"
                    style={{
                      color: `${border_color}`,
                      bottom: spanHeight > 21 && '-20px',
                    }}
                  >
                    {excerpt}
                  </span>
                </div>
                <div className="d-flex justify-content-between w-100">
                  <div
                    className="box-down"
                    style={{ borderBottom: `1px solid ${border_color}` }}
                  ></div>

                  <div
                    className="box-down"
                    style={{ borderBottom: `1px solid ${border_color}` }}
                  ></div>
                </div>
              </div>{' '}
              <Flags />
              <Arrow_down arrow_down_text={arrow_down_text} link={`${pathname}#sectionTwo`}/>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Scroll_section_1;
