import { useEffect } from 'react';
import Scroll_full_page from './Scroll_fullPage';
import { Social_blue_icons } from '../../components/Social_icons';
import { useRouter } from 'next/router';
import Footer from '../footer/Footer';
import { useWindowSize } from '../../hooks/UseWindowSize';
import ScrollButton from '../../components/ScrollButton';

const Single_beer = ({
  logo,
  puces_1,
  puces_2,
  fruit,
  text_pic,
  bottles,
  beer_name,
  ilustration,
  border_color,
  section_1,
  section_2,
  section_3,
  section_4,
  section_5,
  menu_text,
}) => {
  const windowSize = useWindowSize();
  const router = useRouter();
  const { pathname } = router;

  const anchors = [
    'sectionOne',
    'sectionTwo',
    'sectionThree',
    'sectionFour',
    'sectionFive',
  ];

  useEffect(() => {
    const data = localStorage.getItem('date');

    if (data === null) {
      router.push('/');
    }
    const projector = document.getElementById('projector');
    projector.style.display = 'none';

  }, []);

  return (
    <div className="container-fluid single-beer px-0">
      <div className="left-pic-div d-flex justify-content-center align-items-center">
        {windowSize && windowSize.width <= 768 && pathname === '/peche' ? (
          <i className="icon beer-name beerName_peche-white-icon peche-style"></i>
        ) : windowSize && windowSize.width <= 768 && pathname === '/rose' ? (
          <i className="icon beer-name beerName_rose-white-icon peche-style"></i>
        ) : (
          <h1
            style={{
              color:
                pathname === '/blonde' &&
                windowSize &&
                windowSize.width <= 768 &&
                'white',
            }}
          >
            {section_2 && section_2.header}
          </h1>
        )}

        <div className="img-wrapper">
          <img src={bottles} alt={beer_name} />
        </div>
      </div>

      <Scroll_full_page
        section_1={section_1}
        section_2={section_2}
        section_3={section_3}
        section_4={section_4}
        section_5={section_5}
        border_color={border_color}
        logo={logo}
        puces_1={puces_1}
        puces_2={puces_2}
        fruit={fruit}
        bottles={bottles}
        beer_name={beer_name}
        anchors={anchors}
        ilustration={ilustration}
        text_pic={text_pic}
        menu_text={menu_text}
      />
      {windowSize.width && windowSize.width <= 768 && <ScrollButton />}
      <Social_blue_icons />

      <Footer menu_text_color={menu_text} />
    </div>
  );
};

export default Single_beer;
