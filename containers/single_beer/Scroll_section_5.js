import { useState } from 'react';
import Link from 'next/link';
import Lottie from 'react-lottie';
import Essayez from '../../lotties/Essayez_2.json';
import Tryit from '../../lotties/Try_2.json';
import { useSelector } from 'react-redux';
import Scroll_section_5_form from './Scroll_section_5_form';
import { useRouter } from 'next/router';

const Scroll_section_5 = ({ text_pic, section_5 }) => {
  const { excerpt, link, visit_page, placeholder, download } = section_5;
  const [inputVal, setInputVal] = useState('');
  const locale = useSelector(state => state.single_beer_locale);
  const router = useRouter();
  const { pathname } = router;
  const routeName = pathname.substring(1);
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: locale.selected_lang.lang === 'fr' ? Essayez : Tryit,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

  return (
    <div className="scroll_section_5">
      <div className="row mx-auto">
        <div className="col-md-9 mx-auto d-flex flex-column justify-content-center align-items-center">
          <Lottie options={defaultOptions} height={200} width={280} />
          <h5>{excerpt}</h5>

          <Scroll_section_5_form link={link} placeholder={placeholder} />

          <Link href="/our-beers ">
            <a className="btn btn-outline-dark text-uppercase mt-5">{visit_page}</a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Scroll_section_5;
