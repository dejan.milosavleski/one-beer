import { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Side_menu from '../side_menu/Side_Menu';
import logo from '../../styles/assets/images/logos/onebeer-logo-white.svg';
import mobile_logo from '../../styles/assets/images/Logos-Labels/onebeer-logo-black.svg';
import { useSelector, useDispatch } from 'react-redux';
import { is_Spinner_loading_true } from '../../store/spinner/action';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { usePageY_Offset } from '../../hooks/UsePageY_Offset';


const Menu = ({ menu_text, menu_icons, single_beer_logo }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const pathname = router.pathname;
  const [isSideMenuOpen, setIsSideMenuOpen] = useState(false);
  const spinner = useSelector(state => state.spinner.is_spinner_loading);
  const menu = useSelector(state => state.global_locale.selected_lang.menu);
  const windowSize = useWindowSize();
  const pageY = usePageY_Offset();

  const handleSpinner = slug => {
    if (pathname !== slug) {
      dispatch(is_Spinner_loading_true());
    }
  };


  return (
    <>
      {spinner && (
        <div className="spinner-wrapper rounded-circle">
          <div className="eclipse_spinner"></div>
        </div>
      )}
      <div
        className={`container-fluid menu ${
          pageY > 10 && windowSize.width <= 576 && 'menu-bg'
        }`}
      >
        <Link href="/home">
          <a className="navbar-brand">
            <div className="logo-wrapper  d-flex align-items-center">
              <img
                src={
                  pageY > 10 && windowSize.width <= 576
                    ? mobile_logo
                    : pathname === '/citron'
                    ? single_beer_logo
                    : pathname === '/blanche'
                    ? single_beer_logo
                    : pathname === '/peche'
                    ? single_beer_logo
                    : pathname === '/blanche'
                    ? single_beer_logo
                    : pathname === '/blanche'
                    ? single_beer_logo
                    : pathname === '/blanche'
                    ? single_beer_logo
                    : logo
                }
                alt=""
              />
            </div>
          </a>
        </Link>
        <div className="container big-container">
          <nav className={`row navbar`} id="navigation">
            {windowSize.width > 768 && (
              <div className="col-md-8 col-12 px-0">
                <ul className="d-flex">
                  {menu &&
                    menu.menu_titles.map((item, index) => (
                      <li key={index * 434} className="nav-item">
                        <Link href={`/${item.slug}`}>
                          <a
                            className="nav-link px-lg-4 px-md-3"
                            style={{
                              color: `${
                                (pathname === '/citron' ||
                                  pathname === '/blanche' ||
                                  pathname === '/peche' ||
                                  pathname === '/blonde' ||
                                  pathname === '/cerise' ||
                                  pathname === '/rose') &&
                                windowSize &&
                                windowSize.width > 992
                                  ? menu_text
                                  : pathname === '/citron' ||
                                    pathname === '/blanche' ||
                                    pathname === '/peche' ||
                                    pathname === '/blonde' ||
                                    pathname === '/cerise' ||
                                    pathname === '/rose'
                                  ? '#000'
                                  : ''
                              }`,
                            }}
                            onClick={() => handleSpinner(`/${item.slug}`)}
                          >
                            {item.title}
                          </a>
                        </Link>
                      </li>
                    ))}
                </ul>

                <div
                  className={`col-4 d-flex justify-content-${
                    pathname === '/home' ? 'start' : 'end'
                  }`}
                >
                  {/* prvicno iskluceni */}
                  <div className="menu-icons d-none py-2">
                    <i
                      className={`icon ${
                        menu_icons === 'blue' ? 'loupe-blue-icon' : 'loupe-icon'
                      }`}
                    ></i>
                    <i
                      className={`icon mx-4 ${
                        menu_icons === 'blue'
                          ? 'feather-user-blue-icon'
                          : 'icon feather-user-icon'
                      }`}
                    ></i>
                    <i
                      className={`icon ${
                        menu_icons === 'blue'
                          ? 'shopping-bag-blue-icon'
                          : 'shopping-bag-icon'
                      }`}
                    ></i>
                  </div>
                </div>
              </div>
            )}
          </nav>
        </div>
        <div className={`sandwich`}>
          {windowSize.width > 768 ? (
            <i
              className={`icon ${
                menu_icons === 'blue' ? 'sandwich-blue-icon' : 'sandwich-icon'
              }`}
              onClick={() => setIsSideMenuOpen(true)}
            ></i>
          ) : (
            <i
              className={`icon ${
                pageY > 10 ? 'sandwich-blue-icon' : 'sandwich-icon'
              }`}
              onClick={() => setIsSideMenuOpen(true)}
            ></i>
          )}
        </div>
      </div>
   
        <Side_menu
          setIsSideMenuOpen={setIsSideMenuOpen}
          isSideMenuOpen={isSideMenuOpen}
        />
    
    </>
  );
};

export default Menu;
