import { useEffect } from 'react';
import Link from 'next/link';
import logo from '../../styles/assets/images/logos/onebeer-logo-white.svg';
import Auth_Form from './Auth_Form';
import Footer from '../footer/Footer';
import { Social_icons } from '../../components/Social_icons';
import { useSelector, useDispatch } from 'react-redux';
import GlobalFlags from '../../components/GlobalFlags';
import { change_global_lang } from '../../store/global_locale/action';
import ilustration_pic from '../../styles/assets/images/ILLUSTRATION/ILLUSTRATION-FOND_BLONDE.gif';
import Cookies from 'js-cookie';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { motion } from 'framer-motion';
import {
  contOpSc2,
  childOpTD02
} from '../../constants/transitions_Variants';

const Auth_18 = () => {
  const spinner = useSelector(state => state.spinner.is_spinner_loading);
  const lang = Cookies.get('lang');
  const auth_18 = useSelector(
    state => state.global_locale.selected_lang.auth_18
  );
  const windowSize = useWindowSize();
  const dispatch = useDispatch();

  useEffect(() => {
    if (lang) {
      dispatch(change_global_lang(lang));
    } else {
      Cookies.set('lang', 'fr');
      const lang = Cookies.get('lang');
      dispatch(change_global_lang(lang));
    }
  }, []);


  return (
    <div className="container-fluid auth_18 d-flex justify-content-center align-items-center dark-blue px-0 px-sm-3">
      {spinner && (
        <div className="spinner-wrapper rounded-circle">
          <div className="eclipse_spinner"></div>
        </div>
      )}
      <span className="amanda" style={{position: 'fixed',top:'0px', left:'0', opacity:0, color: 'transparent'}}>amanda</span>
      <motion.div
        className="container h-100 d-flex flex-column justify-content-between px-0 px-sm-3"
        style={{ zIndex: '999' }}
        variants={contOpSc2} initial="hidden" animate="show"
      >
        <div className={`row justify-content-center my-auto mx-0  auth-content ${windowSize && windowSize.height < 500 && 'open-mobile-keyword'}`}>
          <div className="col-12 col-sm-9 px-0 px-sm-3">
            <div className="row mx-0">
              <motion.div className="col-12">
                <motion.div className="logo-wrapper mx-auto mb-4" variants={childOpTD02}>
                  <img src={logo} alt="" />
                </motion.div>
              </motion.div>
              <motion.div className="col-12"  variants={childOpTD02}>
                <h2 className="font-weight-light">
                  {auth_18 && auth_18.title_1}
                </h2>
              </motion.div>
              <motion.div className="col-12 mb-4 px-5 px-sm-0"  variants={childOpTD02}>
                <h6 className="font-weight-light">
                  {auth_18 && auth_18.title_2}
                </h6>
              </motion.div>
              <div className="col-12">
                <Auth_Form />
              </div>
            </div>
          </div>
        </div>

        {windowSize && windowSize.height > 500 && <GlobalFlags />}
     
        <Footer menu_text_color="white" />
        {windowSize && windowSize.height > 500 &&  <Social_icons />}
       
      </motion.div>
      <div className="img-div">
        <img src={ilustration_pic} alt="ilustration_pic" />
      </div>
    </div>
  );
};

export default Auth_18;
