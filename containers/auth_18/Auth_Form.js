import { useState, useRef, useEffect } from 'react';
import { findDOMNode } from 'react-dom';
import { useRouter } from 'next/router';
import Modal from '../../components/Modal';
import { Field, Form, reduxForm, reset, change } from 'redux-form';
import { useDispatch, useSelector } from 'react-redux';
import { inputField } from '../../components/Fields';
import {
  number,
  required,
  correctDateNum,
  correctYearNum,
  correctMonthNum,
  number_gb,
  required_gb,
  correctDateNum_gb,
  correctMonthNum_gb,
  correctYearNum_gb,
} from '../../utills/validator';
import { is_Spinner_loading_true } from '../../store/spinner/action';
import Cookies from 'js-cookie';
import { motion } from 'framer-motion';
import {
 contOpSc4,
 childOpTD04,
 xMotionOpX20TD1E
} from '../../constants/transitions_Variants';

const Auth_form = ({ handleSubmit }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  // const [input2Ref, setInput2Focus] = UseFocus();
  const [modalShow, setModalShow] = useState(false);
  const auth_18 = useSelector(
    state => state.global_locale.selected_lang.auth_18
  );
  const lang = Cookies.get('lang');
  const [notAllowed, setNotAllowed] = useState();
  const date = new Date();
  const yyyy = parseInt(date.getFullYear());
  const mm = date.getMonth() + 1;

  const go_to_page = (day, month, year) => {
    router.push('/home');
    dispatch(reset('Auth_form'));
    localStorage.setItem('date', `${day}/${month}/${year}`);
    dispatch(is_Spinner_loading_true());
    return;
  };
  const showAlert = () => {
    if (lang === 'gb') {
      setNotAllowed('you must be at least 18 years old');
      setModalShow(true);
      dispatch(reset('Auth_form'));
      return;
    } else {
      setNotAllowed(`vous devez avoir au moins 18 ans`);
      setModalShow(true);
      dispatch(reset('Auth_form'));
      return;
    }
  };

  const submitForm = values => {
    const { day, month, year } = values;
    let inputDay =
      day.substr(0, 1) === '0'
        ? parseInt(day.substr(day.length - 1))
        : parseInt(day);
    let inputMonth =
      month.substr(0, 1) === '0'
        ? parseInt(month.substr(month.length - 1))
        : parseInt(month);

    var dd = date.getDate();
    if (yyyy - parseInt(year) > 18) {
      go_to_page(day, month, year);
      return;
    }
    if (yyyy - parseInt(year) === 18) {
      if (inputMonth > mm) {
        go_to_page(day, month, year);
      } else {
        if (inputMonth >= mm) {
          if (inputDay >= dd) {
            go_to_page(day, month, year);
          } else {
            showAlert();
          }
        } else {
          showAlert();
        }
      }
    }
    if (yyyy - parseInt(year) < 18) {
      showAlert();
    }
  };

  const focusChangeToMonth = (values, fieldName) => {
    const firstNum = values.charAt(0);
    const secondNum = parseInt(values.charAt(1));
    const name = document.getElementsByName(fieldName);
    const toNum = parseInt(values);
    if (toNum > 31) {
      dispatch(change('Auth_form', 'day', ''));
    }
    if (firstNum === '0') {
      if (secondNum > 0 && secondNum <= 9) {
        return name[0].focus();
      }
    } else if (toNum <= 31 && toNum > 9) {
      name[0].focus();
      return;
    }
  };
  const focusChangeToYear = (values, fieldName) => {
    const firstNum = values.charAt(0);
    const secondNum = parseInt(values.charAt(1));
    const name = document.getElementsByName(fieldName);
    const toNum = parseInt(values);
    if (toNum > 12) {
      dispatch(change('Auth_form', 'month', ''));
    }
    if (firstNum === '0') {
      if (secondNum > 0 && secondNum <= 9) {
        return name[0].focus();
      }
    } else if (toNum <= 12 && toNum > 9) {
      name[0].focus();
      return;
    }
  };

  const focusChangeButton = (values, fieldName) => {
    const toNum = parseInt(values);
    const name = document.getElementById(fieldName);
    if (toNum >= yyyy) {
      dispatch(change('Auth_form', 'year', ''));
    }
    if (toNum > 1900 && yyyy - toNum > 18) {
      name.focus();
    }
  };

  return (
    <>
      <Modal
        content={
          notAllowed !== undefined
            ? notAllowed
            : auth_18 && auth_18.modal_message
        }
        modalShow={modalShow}
        setModalShow={setModalShow}
      />
      <motion.div variants={contOpSc4} initial="hidden" animate="show">
        
        <Form
          noValidate
          className="needs-validation row mx-auto justify-content-center"
          onSubmit={handleSubmit(submitForm)}
        >
          <div className="col-md-10 col-12 d-flex justify-content-between">
            <motion.div variants={childOpTD04} className="animate-div">
              <Field
                name="day"
                className="form-control py-4"
                type="text"
                component={inputField}
                placeholder={auth_18 && auth_18.form_fields.day}
                id="day"
                validate={
                  lang === 'fr'
                    ? [required, number, correctDateNum]
                    : [required_gb, correctDateNum_gb, number_gb]
                }
                feedback={values => focusChangeToMonth(values, 'month')}
              />
            </motion.div>
            <motion.div variants={childOpTD04} className="animate-div">
              <Field
                name="month"
                className="form-control py-4"
                type="text"
                component={inputField}
                placeholder={auth_18 && auth_18.form_fields.month}
                id="month"
                validate={
                  lang === 'fr'
                    ? [required, number, correctMonthNum]
                    : [required_gb, correctMonthNum_gb, number_gb]
                }
                feedback={values => focusChangeToYear(values, 'year')}
              />
            </motion.div>

            <motion.div variants={childOpTD04} className="animate-div">
              <Field
                name="year"
                className="form-control py-4"
                type="text"
                component={inputField}
                placeholder={auth_18 && auth_18.form_fields.year}
                id="year"
                validate={
                  lang === 'fr'
                    ? [required, number, correctYearNum]
                    : [required_gb, correctYearNum_gb, number_gb]
                }
                feedback={values => focusChangeButton(values, 'auth-submit')}
              />
            </motion.div>
          </div>
          <div className="col-md-10 col-12 mt-sm-4">
            <motion.button
              type="submit"
              id="auth-submit"
              className="btn btn-bolck py-2"
              variants={xMotionOpX20TD1E}
            >
              {auth_18 && auth_18.button}
            </motion.button>
          </div>
        </Form>
      </motion.div>
    </>
  );
};

export default reduxForm({
  form: 'Auth_form', // a unique identifier for this form
})(Auth_form);
