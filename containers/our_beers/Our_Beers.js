import Carousel from '../../components/Carousel';
import { Social_icons } from '../../components/Social_icons';
import Footer from '../footer/Footer';
import { useSelector } from 'react-redux';
import { replaceBrTag } from '../../hooks/useBrTag';
import GlobalFlags from '../../components/GlobalFlags';
import rose from '../../styles/assets/images/single-beers/rose.png';
import blonde from '../../styles/assets/images/single-beers/blonde.png';
import blanche from '../../styles/assets/images/single-beers/blanche.png';
import cerise from '../../styles/assets/images/single-beers/cerise.png';
import citron from '../../styles/assets/images/single-beers/citron.png';
import peche from '../../styles/assets/images/single-beers/peche.png';
import withTransition from '../../components/TransitionWrapper';
import { motion } from 'framer-motion';
import { blogVariants } from '../../constants/transitions_Variants';
import { useWindowSize } from '../../hooks/UseWindowSize';
const Our_beers = () => {
  const windowSize = useWindowSize();
  const our_beers = useSelector(
    state => state.global_locale.selected_lang.our_beers
  );
  const beers = [
    {
      url: blonde,
      title: 'blonde',
    },
    {
      url: blanche,
      title: 'blanche',
    },

    {
      url: cerise,
      title: 'cerise',
    },
    {
      url: citron,
      title: 'citron',
    },
    {
      url: peche,
      title: 'peche',
    },
    {
      url: rose,
      title: 'rose',
    },
  ];
  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.7,
      },
    },
  };
  const items = {
    hidden: { opacity: 0 },
    show: { opacity: 1, transition: { duration: 0.7 } },
  };

  const postPreviewVariants = {
    initial: { x: '100%', opacity: 0 },
    enter: { x: 0, opacity: 1, transition: { duration: 0.8 } },
    exit: { x: '-100%', opacity: 0, transition: { duration: 0.8 } },
  };

  return (
    <motion.div
      initial="initial"
      animate="enter"
      exit="exit"
      variants={blogVariants}
      className="our_beers "
      style={{ overflowX: 'hidden', position: 'relative' }}
    >
      <motion.div
        className="container big-container"
        variants={container}
        initial="hidden"
        animate="show"
        exit="hidden"
      >
        <div className="row main-titles">
          <div className="col-12">
            <motion.h5
              className="text-center text-white font-weight-light mt-5 mt-0"
              variants={items}
            >
              {our_beers && our_beers.main_title}
            </motion.h5>
            <motion.h5
              className="text-center text-white font-weight-light"
              variants={items}
            >
              <b>{our_beers && our_beers.second_title}</b>
            </motion.h5>
          </div>
        </div>
      </motion.div>

      <div   className="carousel row mx-0 my-md-auto my-4 py-4">
        <div className="col-12 p-0 h-100">
          <Carousel images={beers} />
        </div>
      </div>
      <GlobalFlags />
      
      <Footer menu_text_color="#fff" />

      <Social_icons />
    </motion.div>
  );
};
export default Our_beers;
