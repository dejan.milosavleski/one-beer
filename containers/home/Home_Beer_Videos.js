import React from 'react';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { uniqueId } from 'lodash';
import { motion } from 'framer-motion';
import { contOpTr13 } from '../../constants/transitions_Variants'
import Tilt from 'react-tilt';

const Home_Beer_Video = ({ showBeer }) => {
  const windowSize = useWindowSize();
  const beers = [
    {
      num: 0,
      id_desktop: uniqueId(),
      id_mobile: uniqueId(),
      

      url_desktop: '/new_export_2/BLONDE_VP9_2pass.webm',
      url_mobile: '/new_export_2/mobile/blonde_VP9_2pass.webm',
      poster_desktop: '/Main-Blonde.png',
      poster_mobile: '/blonde-crop.png',
      beers_desktop: 'beers-desktop',
      beers_mobile: 'beers-mobile',
    },
    {
      num: 1,
      id_desktop: uniqueId(),
      id_mobile: uniqueId(),
      url_desktop: '/new_export_2/BLANCHE_VP9_2pass.webm',
      url_mobile: '/new_export_2/mobile/blanche_VP9_2pass.webm',
      poster_desktop: '/Main-Blanche.png',
      poster_mobile: '/blanche-crop.png',
      beers_desktop: 'beers-desktop',
      beers_mobile: 'beers-mobile',
    },
    {
      num: 2,
      id_desktop: uniqueId(),
      id_mobile: uniqueId(),
      url_desktop: '/new_export_2/CERISE_VP9_2pass.webm',
      url_mobile: '/new_export_2/mobile/cerise_VP9_2pass.webm',
      poster_desktop: '/Main-Cerise.png',
      poster_mobile: '/cerise-crop.png',
      beers_desktop: 'beers-desktop',
      beers_mobile: 'beers-mobile',
    },
    {
      num: 3,
      id_desktop: uniqueId(),
      id_mobile: uniqueId(),
      url_desktop: '/new_export_2/CITRON_VP9_2pass.webm',
      url_mobile: '/new_export_2/mobile/citron_VP9_2pass.webm',
      poster_desktop: '/Main-Citron.png',
      poster_mobile: '/citron-crop.png',
      beers_desktop: 'beers-desktop',
      beers_mobile: 'beers-mobile',
    },
    {
      num: 4,
      id_desktop: uniqueId(),
      id_mobile: uniqueId(),
      url_desktop: '/new_export_2/PECHE_VP9_2pass.webm',
      url_mobile: '/new_export_2/mobile/peche_VP9_2pass.webm',
      poster_desktop: '/Main-Peche.png',
      poster_mobile: '/peche-crop.png',
      beers_desktop: 'beers-desktop',
      beers_mobile: 'beers-mobile',
    },
    {
      num: 5,
      id_desktop: uniqueId(),
      id_mobile: uniqueId(),
      url_desktop: '/new_export_2/ROSE_VP9_2pass.webm',
      url_mobile: '/new_export_2/mobile/rose_VP9_2pass.webm',
      poster_desktop: '/Main-Rose.png',
      poster_mobile: '/rose-crop.png',
      beers_desktop: 'beers-desktop',
      beers_mobile: 'beers-mobile',
    },
  ];


  return (
    <>
      {beers.map((beer, index) => (
        <motion.div
          variants={contOpTr13}
          animate={showBeer === beer.num ? 'show' : 'hidden'}
          style={{ display: showBeer === beer.num ? 'block' : 'none' }}
          className="w-100 h-100 beerSingleFrontWrapper"
          id="beer-1"
          key={`beerFront`+index * 67324}
        >
           <Tilt className="tilt"   options={{
          max: 2,
          scale:1.01, 
        
        }} maxGlare={0.1} >
          <video
            muted
            autoPlay={true}
            loop
            playsInline
            poster={beer.poster_desktop}
            className={`video-insert ${beer.beers_desktop}`}
          >
            <source src={beer.url_desktop} type="video/webm" />
          </video>
          <video
            muted
            autoPlay={true}
            loop
            playsInline
            poster={beer.poster_mobile}
            className={`video-insert ${beer.beers_mobile}`}
          >
            <source src={beer.url_mobile} type="video/webm" />
          </video>
          </Tilt>
          {/* <img
            src={beer.url_mobile}
            className={beer.beers_mobile}
            style={{ top: windowSize && windowSize.height > 815 && '12%' }}
          /> */}
        </motion.div>
      ))}
    </>
  );
};

export default Home_Beer_Video;
