import { useState, useEffect, useRef } from 'react';
import Link from 'next/link';
import { Social_icons } from '../../components/Social_icons';
import Footer from '../footer/Footer';
import { useSelector } from 'react-redux';
import { useWindowSize } from '../../hooks/UseWindowSize';
import { replaceBrTag } from '../../hooks/useBrTag';
import { useRouter } from 'next/router';
import GlobalFlags from '../../components/GlobalFlags';
import capsule_blanche from '../../styles/assets/images/ONE BEER_CAPSULES/CAPSULE_BLANCHE.png';
import capsule_blonde from '../../styles/assets/images/ONE BEER_CAPSULES/CAPSULE_BLONDE.png';
import capsule_cerise from '../../styles/assets/images/ONE BEER_CAPSULES/CAPSULE_CERISE.png';
import capsule_peche from '../../styles/assets/images/ONE BEER_CAPSULES/CAPSULE_PECHE.png';
import capsule_citron from '../../styles/assets/images/ONE BEER_CAPSULES/CAPSULE_CITRON.png';
import capsule_rose from '../../styles/assets/images/ONE BEER_CAPSULES/CAPSULE_ROSE.png';
import Home_Beer_Videos from './Home_Beer_Videos';
import { motion } from 'framer-motion';
import { contOpSc2, childOp } from '../../constants/transitions_Variants';

const HomePage = ({ menu_text_color }) => {
  const [showA, setshowA] = useState(false);
  const [showBeer, setShowBeer] = useState(0);
  const spinner = useSelector(state => state.spinner.is_spinner_loading);
  const home_page = useSelector(
    state => state.global_locale.selected_lang.home_page
  );
  const router = useRouter();
  const windowSize = useWindowSize();
  const myRef = useRef();

  useEffect(() => {
    const data = localStorage.getItem('date');
    if (data === null) {
      router.push('/');
    }

  }, []);

  const changeBeer = index => {
    if (index !== showBeer) {
      setShowBeer(index);
      setshowA(true);
    } else {
      return;
    }
  };

  const capsules = [
    {
      capsule: capsule_blonde,
      name: 'beerName_blonde-white-icon',
    },
    {
      capsule: capsule_blanche,
      name: 'beerName_blanche-white-icon',
    },
    {
      capsule: capsule_cerise,
      name: 'beerName_cerise-white-icon',
    },
    {
      capsule: capsule_citron,
      name: 'beerName_citron-white-icon',
    },
    {
      capsule: capsule_peche,
      name: 'beerName_peche-white-icon',
    },
    {
      capsule: capsule_rose,
      name: 'beerName_rose-white-icon',
    },
  ];

  return (

    <div className="container big-container home" style={{overflowX: "hidden", position: "relative"}}>
      {spinner && (
        <div className="spinner-wrapper rounded-circle">
          <div className="eclipse_spinner"></div>
        </div>
      )}

      <div className="row beer-row">
        {home_page &&
          home_page.beers.map((beer, index) => (
            <div
              ref={myRef}
              key={index * 52}
              className={`col-12 beer-col`}
              style={{
                display: showBeer === index ? 'flex' : 'none',
                margin: '0px',
                transition: 'all 2s',
              }}
            >
              <div className="beer row mx-0 ">
               
                  <motion.div
                  variants={contOpSc2}
                  initial="hidden"
                  animate="show"
                  ref={myRef}
                  key={index * 2390}
                  className="col-md-12 col-12 left-wrapper">
                    <div className="d-flex flex-column">
                      <div className="d-flex first-two-titles">
                        <motion.h3  variants={childOp}>
                          {' '}
                          {beer.title_1}
                        </motion.h3>
                        <motion.h3 variants={childOp}>
                         {beer.title_2}
                        </motion.h3>
                      </div>
                      <motion.h2 variants={childOp} className="amanda">
                      
                          {beer.title_3} <span className="points">...</span>
                     
                      </motion.h2>
                     <div>
                  
                     </div>
                     
                      <motion.p variants={childOp}>
                       {replaceBrTag(beer.small_text, '<br/>', <br />)}
                      </motion.p>

                      <div className="d-flex">
                       
                          <Link href="/our-beers">
                            <motion.a key={index * 91490} className="btn btn-outline-light" variants={childOp}>
                              {home_page && home_page.button}
                            </motion.a>
                          </Link>
                      
                      </div>
                    </div>

                    {/* {windowSize.width > 576 ?  */}
                 
                </motion.div>
              </div>
            </div>
          ))}

        <motion.div key={`beer-animate` + 8493264} className="beer-animate" variants={childOp}>
     
         <Home_Beer_Videos showBeer={showBeer} />
        </motion.div>

        <div className="col-md-8 col-6 mx-3 mx-md-0">
        <motion.div
                  variants={contOpSc2}
                  initial="hidden"
                  animate="show"
                  key={`capsule` + "1424" + 2390}
                >
          <ul className="capsule d-flex ">
            {capsules.map((capsule, index) => (
               <motion.li variants={childOp}  key={index * 641237}>
              <div
              
                className={`capsule-wrapper ${
                  showBeer === index && 'active-capsule'
                } `}
                onClick={() => changeBeer(index)}
              >
                <div className="p-0 d-flex flex-column align-items-center">
                  <div className="img-div">
                    <img src={capsule.capsule} alt="beer-capsule" />
                  </div>
                  {windowSize.width >= 994 && (
                    <i className={`icon ${capsule.name}`} />
                  )}
                </div>
              </div>
              </motion.li>
            ))}
          </ul>
          </motion.div>

        </div>

        <GlobalFlags />
        <Social_icons />
        <Footer menu_text_color={menu_text_color} menu_text />
      </div>
    </div>
  );
};

export default HomePage;
