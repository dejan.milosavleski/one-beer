import { useEffect, useRef, useState, Suspense } from 'react';
import { Canvas, useFrame, useThree } from '@react-three/fiber';
import {
  useGLTF,
  OrbitControls,
  ContactShadows,
  Environment,
  PerspectiveCamera,
} from '@react-three/drei';

function Bottle(props) {
  const { bottle } = props;

  const group = useRef();
  const { nodes, materials } = useGLTF(`${bottle.url}`);

  //#002b9e
  return (
    <>
      <Environment preset="night" />
      <group ref={group} {...props} dispose={null}>
        <mesh
          geometry={nodes.Cylinder001.geometry}
          material={materials['mat-Bottle.002']}
        >
          <meshStandardMaterial
            color="#101573"
            opacity={0.8}
            transparent
            roughness={0.2}
            metalness={0.2}
          />

          <mesh
            geometry={nodes.Cap001.geometry}
            material={materials['Cap.002']}
           
          />

          <mesh
            geometry={nodes.Cylinder002.geometry}
            material={materials['Label.002']}
           
          />
        </mesh>
      </group>
    </>
  );
}

const Scene = ({ position, bottle }) => {
  const {
    camera,
    gl: { domElement },
  } = useThree();

  return (
    <>
      <Bottle position={position} bottle={bottle} />
      <OrbitControls
        args={[camera, domElement]}
        enablePan={false}
        autoRotate={true}
        enableZoom={false}
        minPolarAngle={Math.PI / 2}
        maxPolarAngle={Math.PI / 2}
      />
    </>
  );
};

const Home_Beer_Animation = ({ bottle }) => {
  return (
    <Canvas dpr={[1, 2]} camera={{ position: [0, 0, 4], fov: 55 }}>
      <ambientLight intensity={1.13} />
      <spotLight
        intensity={0.5}
        angle={0.1}
        penumbra={1}
        position={[10, 10, 10]}
        castShadow
      />
      <Suspense fallback={null}>
        <Scene position={[0, -0.5, 0]} bottle={bottle} />
      </Suspense>
    </Canvas>
  );
};

export default Home_Beer_Animation;
