import { useState, useEffect } from 'react';
import ContactForm from './ContactForm';
import Footer from '../footer/Footer';
import { Social_icons } from '../../components/Social_icons';
import { useSelector } from 'react-redux';
import { useWindowSize } from '../../hooks/UseWindowSize';
import GlobalFlags from '../../components/GlobalFlags';
import { motion } from 'framer-motion';
import {
  contSc04,
  childOpTD07EIO,
} from '../../constants/transitions_Variants';
import Toast from '../../components/Toast';
const Contact = () => {
  const windowSize = useWindowSize();
  const spinner = useSelector(state => state.spinner.is_spinner_loading);
  const contact = useSelector(
    state => state.global_locale.selected_lang.contact
  );
  const [toast, setToast] = useState('');

  useEffect(() => {
    const timeout = setTimeout(() => {
      setToast('');
    }, 3000);
    return () => clearTimeout(timeout);
  }, [toast]);

 

  return (
    <>
      <motion.div
        variants={contSc04}
        initial="initial"
        animate="enter"
        exit="exit"
        className="container big-container contact"
        style={{ overflowX: 'hidden', position: 'relative' }}
      >
        <Toast message={toast} right_px="0px" />
        {spinner && (
          <div className="spinner-wrapper rounded-circle">
            <div className="eclipse_spinner"></div>
          </div>
        )}
        <div className="row align-items-center">
          <div className="col-md-6 col-12 ilustration my-3">
            <div className="text-wraper">
              <div className="col-md-12 px-0 px-md-3">
                <motion.h2
                  variants={childOpTD07EIO}
                  className="font-weight-light mb-4"
                >
                  {contact && contact.main_title}
                </motion.h2>

                <motion.div variants={childOpTD07EIO}>
                  <p className="mb-3 text-white font-weight-light">
                    {contact && contact.excerpt}
                  </p>
                  {
                    contact?.distributers.map((distributer, index) => 
                    <div key={index* 786123} className="mt-4">
                    <p
                       className="text-uppercase mb-0  d-inline-block   font-weight-bold"
                       style={{ color: distributer.color }}
                     >
                       {distributer.text_1}
                     </p>
                     <h6 className="text-white mt-1">
                       {distributer.text_2}
                     </h6>
                     <a
                       href="https://www.ibs-wholesale.com"
                       className="text-white d-block"
                     >
                       {distributer.email}
                     </a>
                    </div>
                    )
                  }
                </motion.div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12 form-div my-3">
            <ContactForm setToast={setToast} />
          </div>
        </div>
        {windowSize.width && windowSize.width > 576 && <GlobalFlags />}
        {windowSize.width && windowSize.width > 576 && <Social_icons />}

        <Footer menu_text_color="white" />
      </motion.div>
    </>
  );
};

export default Contact;
