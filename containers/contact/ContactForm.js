import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Modal from '../../components/Modal';
import { Field, Form, reduxForm, reset } from 'redux-form';
import { useDispatch, useSelector } from 'react-redux';
import { inputField, textareaField } from '../../components/Fields';
import { required, required_gb, email, email_gb } from '../../utills/validator';
import {
  is_Spinner_loading_true,
  is_Spinner_loading_false,
} from '../../store/spinner/action';
import Cookies from 'js-cookie';
import { motion } from 'framer-motion';
import { childOpTD09EO }  from '../../constants/transitions_Variants';

const Contct_form = ({ handleSubmit, setToast }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const lang = Cookies.get('lang');
  const contact = useSelector(
    state => state.global_locale.selected_lang.contact
  );

  const sendMail = async data => {
    try {
      const response = await fetch('/api/contact', {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(data),
      });
      const res = await response.json();
      if (res) {
        setToast(res.message);
        dispatch(is_Spinner_loading_false());
      }
    } catch (error) {
      setToast(error);
      dispatch(is_Spinner_loading_false());
    }
  };

  const submitForm = values => {
    sendMail(values);
    dispatch(is_Spinner_loading_true());
    dispatch(reset('Contct_form'));
  };

  return (
    <>
      <Form
        noValidate
        className="needs-validation row mx-auto"
        onSubmit={handleSubmit(submitForm)}
      >
        <motion.div  variants={childOpTD09EO} className="row d-flex justify-content-between">
          {/* <motion.div variants={formVariants} className="w-100"> */}
            <Field
              name="name"
              groupClassName="col-12"
              className="form-control"
              type="text"
              component={inputField}
              placeholder={contact && contact.form.name}
              id="nom"
              validate={lang === 'fr' ? [required] : [required_gb]}
            />
            <Field
              name="email"
              className="form-control"
              groupClassName="col-12"
              type="email"
              component={inputField}
              placeholder={contact && contact.form.email}
              id="mail"
              validate={
                lang === 'fr' ? [required, email] : [required_gb, email_gb]
              }
            />
            <Field
              name="message"
              className="form-control"
              groupClassName="col-12"
              type="text"
              component={textareaField}
              placeholder={contact && contact.form.message}
              id="year"
            />
          {/* </motion.div> */}
          <div className="col-12 mt-4">
            <button type="submit" className="btn btn-bolck py-2">
              {contact && contact.form.button}
            </button>
          </div>
        </motion.div>
      </Form>
    </>
  );
};

export default reduxForm({
  form: 'Contct_form', // a unique identifier for this form
})(Contct_form);
