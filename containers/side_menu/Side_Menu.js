import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { Social_blue_icons } from '../../components/Social_icons';
import { is_Spinner_loading_true } from '../../store/spinner/action';
import GlobalFlags from '../../components/GlobalFlags';
import { AnimatePresence, motion } from 'framer-motion';
import { uniqueId } from 'lodash';

const Side_Menu = React.memo(({ setIsSideMenuOpen, isSideMenuOpen }) => {

  const dispatch = useDispatch();
  const router = useRouter();
  const pathname = router.pathname;
  const side_menu = useSelector(
    state => state.global_locale.selected_lang.side_menu
  );

  const handleSpinnerCloseMenu = slug => {
    if (pathname !== slug) {
      dispatch(is_Spinner_loading_true());
    }
    setIsSideMenuOpen(false);
  }

  const variants = {
    hidden: { x: -100, opacity: 0, transition: { when: 'afterChildren' } },
    visible: {
      x: 0,
      opacity: 1,
      transition: { delay: 0.2, duration: 0.8, when: 'beforeChildren' },
    },
  };

  return (
    <AnimatePresence>
      {isSideMenuOpen && (
        <>
          <div className="side-menu d-flex" />
          <motion.div
            className={`inner_menu d-flex flex-column text-right`}
            initial={{
              x: '100%',
              transition: {
                duration: 0.4,
              },
            }}
            animate={{
              x: 0,
              transition: {
                duration: 0.4,
              },
            }}
            exit={{
              x: '100%',
              opacity:0,
              transition: {
                duration: 0.7,
              },
            }}
            key={uniqueId('inner_menu' + 257290)}
          >
            <motion.i
              key="close-icon"
              initial="hidden"
              animate="visible"
              variants={variants}
              className="icon close-icon"
              onClick={() =>  setIsSideMenuOpen(false)}
            />
            <Link href="/our-beers">
              <motion.a
                initial="hidden"
                animate="visible"
                variants={variants}
                key="our-beers"
                onClick={() => handleSpinnerCloseMenu("/our-beers")}
              >
                <h2 className="">{side_menu && side_menu.our_beers}</h2>
              </motion.a>
            </Link>

            <motion.ul className="d-flex flex-column border-right ul-1">
              {side_menu &&
                side_menu.beers.map((item, index) => (
                  <motion.li
                    className="nav-item px-3"
                    initial="hidden"
                    animate="visible"
                    variants={variants}
                    key={item.title}
                  >
                    <Link href={`/${item.slug}`}>
                      <a className="nav-link" variants={item}  onClick={() => handleSpinnerCloseMenu(`/${item.slug}`)}>
                        {item.title}
                      </a>
                    </Link>
                  </motion.li>
                ))}
            </motion.ul>

            <Link href="/our-story">
              <motion.a
                initial="hidden"
                animate="visible"
                variants={variants}
                key="our-story"
                onClick={() => handleSpinnerCloseMenu(`/our-story`)}
              >
                <h2  >{side_menu && side_menu.our_story}</h2>
              </motion.a>
            </Link>

            <Link href="/contact">
              <motion.a
                initial="hidden"
                animate="visible"
                variants={variants}
                key="contact"
                onClick={() => handleSpinnerCloseMenu(`/contact`)}
              >
                <h2>{side_menu && side_menu.contact}</h2>
              </motion.a>
            </Link>

            <motion.ul className="d-flex flex-column px-0 ul-2">
              {side_menu &&
                side_menu.ul_1.map((item, index) => (
                  <motion.li
                    className="nav-item"
                    initial="hidden"
                    animate="visible"
                    variants={variants}
                    key={item.slug}
                  >
                    <Link href={`/${item.slug}`}>
                      <a className="nav-link"  onClick={() => handleSpinnerCloseMenu(`/${item.slug}`)}>{item.title}</a>
                    </Link>
                  </motion.li>
                ))}
            </motion.ul>

            <motion.div
              className="side-menu-footer"
              initial="hidden"
              animate="visible"
              key="side-menu-footer"
              variants={{
                hidden: {
                  opacity: 0,
                },
                visible: {
                  opacity: 1,
                  transition: { duration: 0.8 },
                },
              }}
            >
              <motion.div
                className="d-flex justify-content-center"
                initial="hidden"
                animate={{
                  opacity: 1,
                  transition: { delay: 0.7, staggerChildren: 0.3 },
                }}
                key="socialIcons234235"
                exit="hidden"
                variants={{
                  hidden: {
                    opacity: 0,
                  },
                  visible: {
                    opacity: 1,
                    delay: 1,
                  },
                }}
              >
                <GlobalFlags />
                <Social_blue_icons />
              </motion.div>

              <motion.div
                className="d-flex flex-column align-items-center justify-content-center mt-2"
                initial="hidden"
                animate={{
                  opacity: 1,
                  transition: { delay: 0.75, staggerChildren: 0.3 },
                }}
                key="side-menu-footer234235"
                exit="hidden"
                variants={{
                  hidden: {
                    opacity: 0,
                  },
                  visible: {
                    opacity: 1,
                    delay: 1,
                  },
                }}
              >
                <small>
                Site by 
                  <Link href="mailto:ilann@winfourteen.fr">
                    <a>
                      <small className="text-underline"> Win Fourteen Agency</small>
                    </a>
                  </Link>
                </small>
                <small>  and Designed by JD Créa</small>

                <small>
                  Copyright &#169; 2021 |{' '}
                  <Link href="https:onebeer.fr">
                    <a>
                      <small>ONE BEER</small>
                    </a>
                  </Link>
                </small>
              </motion.div>
            </motion.div>
          </motion.div>
        </>
      )}
    </AnimatePresence>
  );
});

export default Side_Menu;
