import React from 'react';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { Social_blue_icons } from '../../components/Social_icons';
import { useWindowSize } from '../../hooks/UseWindowSize';
import GlobalFlags from '../../components/GlobalFlags';
import { AnimatePresence, motion } from 'framer-motion';

const Side_Menu = ({ setIsSidemenuOpen, isSideMenuOpen }) => {
  const windowSize = useWindowSize();
  const side_menu = useSelector(
    state => state.global_locale.selected_lang.side_menu
  );
  const container = {
    hidden: { opacity: 0, x: 0 },
    show: {
      opacity: 1,
      x: '100%',
      transition: {
        staggerChildren: 0.2,
      },
    },
  };
  const variants = {
    hidden: { x: 0, opacity: 0 },
    visible: {
      x: 10,
      opacity: 1,
      transition: { duration: 0.5 },
    },
  };

  const item = {
    hidden: { x: 0, opacity: 0 },
    show: { x: 10, opacity: 1, transition: { duration: 0.5 } },
  };
  return (
    <AnimatePresence>
      {isSideMenuOpen && (
        <div
          className="side-menu d-flex"
          initial={{ width: 0 }}
            animate={{ width: 300,  transition: {
              staggerChildren: 0.2,
            },  }}
            exit={{ width: 0 }}
        >
          {/* <div className="blurred" /> */}
        </div>
      )}
      {isSideMenuOpen && (
        <div
          className={`inner_menu d-flex flex-column text-right px-5`}
          // style={{
          //   right: isSideMenuOpen ? '0px' : '-780px',
          //   transition: 'all .5s linear',
          // }}
          initial={{ width: 0 }}
          animate={{ width: 300 }}
          exit={{ width: 0 }}
        >
          <motion.i
           initial='hidden'
           animate='visible'
            variants={variants}
            className="icon close-icon"
            onClick={() => setIsSidemenuOpen(!isSideMenuOpen)}
          />
          <Link href="/our-beers">
            <motion.a   initial='hidden'
           animate='visible'
            variants={variants}>
              <h2 className="">{side_menu && side_menu.our_beers}</h2>
            </motion.a>
          </Link>

          <motion.ul className="d-flex flex-column border-right">
            {side_menu &&
              side_menu.beers.map((item, index) => (
                <motion.li key={index * 43564} className="nav-item px-3"   initial='hidden'
                animate='visible'
                 variants={variants}>
                  <Link href={`/${item.slug}`}>
                    <a className="nav-link" variants={item}>
                      {item.title}
                    </a>
                  </Link>
                </motion.li>
              ))}
          </motion.ul>

          <Link href="/our-story">
            <motion.a  initial='hidden'
           animate='visible'
            variants={variants}>
              <h2>{side_menu && side_menu.our_story}</h2>
            </motion.a>
          </Link>

          <Link href="/contact">
            <motion.a  initial='hidden'
           animate='visible'
            variants={variants}>
              <h2>{side_menu && side_menu.contact}</h2>
            </motion.a>
          </Link>

          <motion.ul className="d-flex flex-column px-0">
            {side_menu &&
              side_menu.ul_1.map((item, index) => (
                <motion.li 
                  key={index * 44}
                  className="nav-item"
                  initial='hidden'
                  animate='visible'
                   variants={variants}
                >
                  <Link href={`/${item.slug}`}>
                    <a className="nav-link">{item.title}</a>
                  </Link>
                </motion.li>
              ))}
          </motion.ul>

          <div className="side-menu-footer"   initial='hidden'
           animate='visible'
            variants={variants}>
            <div className="d-flex justify-content-center">
              <GlobalFlags />
              <Social_blue_icons />
            </div>

            <div className="d-flex justify-content-center mt-2"   initial='hidden'
           animate='visible'
            variants={variants}>
              <small>
                Copyright &#169; 2021 |{' '}
                <Link href="https://onebeer.fr">
                  <a>
                    <small>ONE BEER</small>
                  </a>
                </Link>
              </small>
            </div>
          </div>
        </div>
      )}
    </AnimatePresence>
  );
};

export default Side_Menu;


const variants = {
    hidden: {   y: 50,
      opacity: 0,
      transition: {
        y: { stiffness: 1000 }
      } },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        y: { stiffness: 1000, velocity: -100 }
      }
    },
  };
