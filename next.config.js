const withImages = require('next-images');
const withVideos = require('next-videos');
module.exports = withVideos(withImages(
    {
        images: {
          domains: ['cdn.bizniskatalog.mk','vesnik.com', 'localhost:3000'],
        },
      }
));
