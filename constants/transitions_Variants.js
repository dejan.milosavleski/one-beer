const dr02 = 0.2;
const dr04 = 0.4;
const dr07 = 0.7;
const dr09 = 0.9;
const dr1 = 1;
const dr13 = 1.3;

const op0 = 0;
const op1 = 1;

const sc01 =  0.1 ;
const sc02 = 0.2;
const sc04 = 0.4;

const x50 = "50%";
const xmin50 = '-50%';
const x100 = "100%";
const xmin100 = '-100%';
const xzero = 0;
const x20_20_0 =[-20,20, 0];

const easeIO = 'easeInOut';
const easeO = 'easeOut';

const tr07InOut = { duration: dr07,  ease: easeIO };
const tr13InOut = { duration: dr13,  ease: easeIO };

export const blogVariants = {
  enter: { transition: {staggerChildren: sc01} },
  exit: { transition:{ staggerChildren: sc01}  },
};

export const postPreviewVariants = {
  initial: { x: x100, opacity: op0 },
  enter: { x: xzero, opacity: op1 ,transition:tr07InOut },
  exit: { x: xmin100, opacity: op0, transition: tr07InOut }
};

export const contOpSc2 = {
  hidden: { opacity: op0 },
  show: {
    opacity: op1,
    transition: {
      staggerChildren: sc02,
    },
  },
}
export const contSc04 = {
  show: { transition: {staggerChildren: sc04} },
  hidden: { transition:{ staggerChildren: sc04}  },
}
export const contOpSc4 = {
  hidden: { opacity: op0 },
  show: {
    opacity: op1,
    transition: {
      staggerChildren: sc04,
    },
  },
}
export const contOpTr13 = {
  hidden: { opacity: op0 },
  show: {
    opacity: op1,
    transition: tr13InOut,
  },
}
export const childOp = {
  hidden: { opacity: op0 },
  show: { opacity: op1 },
}
export const childOpTD02 = {
  hidden: { opacity: op0 },
  show: { opacity: op1, transition: { duration: dr02 }},
}
export const childOpTD04 = {
  hidden: { opacity: op0 },
  show: { opacity: op1, transition: { duration: dr04 }},
}
export const childOpTD07EIO = {
  initial: {opacity: op0},
  exit: { opacity: op0 },
  enter: { opacity: op1, transition: { duration: dr07, ease: easeIO }},
}
export const childOpTD09EO = {
  initial: { x: x50, opacity: op0 },
  enter: { x:xzero, opacity: op1, transition: { duration: dr13, ease: easeO } },
  exit: { x:xmin50,opacity: op0, transition: { duration: dr13, ease: easeO }},
}


export const containeropacity = {
  hidden: { opacity: op0 },
  show: {
    opacity: op1,
    transition: {
      staggerChildren: sc02,
    },
  },
};
export const childrenOpacity = {
  hidden: { opacity: op0 },
  show: { opacity: op1 },
};

// move on x element
export const xMotionOpX20TD1E = {
  hidden: { opacity: op0 },
  show: { opacity: op1, x: x20_20_0, transition: { duration: dr1 }, ease: easeO },
}