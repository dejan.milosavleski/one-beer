const cn = {
  lang: 'cn',
  arrow_down_text:"滾動",
  blanche: {
    section_1: {
      text: (
        <>
          一款细腻、
          <br />
          清淡和优雅的啤酒
        </>
      ),
      excerpt: '比利时啤酒厂成立于1859年',
    },
    section_2: {
      header: 'Blanche',
      text: '一款精緻、微妙和優雅的啤酒，帶有柔和的香菜和橙子味，口感特別平衡。 令人愉悅的新鮮感！',
    },
    section_3: {
      ul: [
        '100%手工酿造',
        '未经过滤的味道',
        '香菜和橙子的甜美气息',
        '口感丰满, 酒体浑厚',
        '美丽的白色泡沫',
      ],
      header: '爱上它的5个理由',
    },
    section_4: {
      span_color: 'blanche_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: '规格',
          excerpt: (
            <>
              <b>瓶装： </b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: '保质期',
          excerpt: (
            <>
              <b>瓶装： </b> 14 个月
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: '酒精度',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: '饮用温度',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: '配料',
          excerpt:
            '水，大麥麥芽，小麥，燕麥，啤酒花，酵母，櫻桃濃縮汁 櫻桃味，香菜，橙皮，甜味劑：三氯蔗糖。',
        },
      ],
    },
    section_5: {
      excerpt: <> 訂閱我們的時事通訊 店鋪開業第一時間通知</>,
      link: '使者',
      placeholder: '選民電子郵件',
      visit_page: '後退',
      download: '产品表'
    },
  },
  //////////// BLONDE/////////////
  blonde: {
    section_1: {
      text: (
        <>
          一款色调金黄的清爽原
          <br />
          味啤酒
          <br />
        </>
      ),
      excerpt: '比利时啤酒厂成立于1859年',
    },
    section_2: {
      header: 'Blonde',
      text: '它的金色長袍反映在它淡淡的麥芽和啤酒花味道上。終極啤酒！ 甘甜的苦味，提升飲用的愉悅感！',
    },
    section_3: {
      ul: [
        '100%手工酿造 ',
        '淡淡麦芽和啤酒花味道',
        '令人愉悦的苦味',
        '美丽的白色泡沫',
        '淡黄色的酒体',
      ],
      header: '爱上它的5个理由',
    },
    section_4: {
      span_color: 'blonde_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: '规格',
          excerpt: (
            <>
              <b> 瓶装：</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: '保质期',
          excerpt: (
            <>
              <b> 瓶装：</b> 14 个月
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: '酒精度',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: '饮用温度',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: '配料',
          excerpt: '水，大麥麥芽，啤酒花，酵母。',
        },
      ],
    },
    section_5: {
      excerpt: <> 訂閱我們的時事通訊 店鋪開業第一時間通知</>,
      link: '使者',
      placeholder: '選民電子郵件',
      visit_page: '後退',
      download: '产品表'
    },
  },
  /////////// CERISE ////////////
  cerise: {
    section_1: {
      text: <>一款添加樱桃汁的原味啤酒</>,
      excerpt: '比利时啤酒厂成立于1859年',
    },
    section_2: {
      header: 'Cerise',
      text: '它的長袍毫無疑問地揭示了：它確實是真正的有機櫻桃汁，與這款 100% 精釀啤酒相得益彰！ 在這種鮮美多汁的紅色水果的味道背後，有一種令人驚訝的酸甜觸感，一種讓我們回到童年的味道。',
    },
    section_3: {
      ul: [
        '100%手工酿造',
        '樱桃的馥郁香气',
        '100%天然果香',
        '清新鲜美的酸甜味',
        '令人迷醉的红色',
      ],
      header: '爱上它的5个理由',
    },
    section_4: {
      span_color: 'cerise_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: '规格',
          excerpt: (
            <>
              <b> 瓶装：</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: '保质期',
          excerpt: (
            <>
              <b> 瓶装：</b> 18 个月
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: '酒精度',
          excerpt: '4.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: '饮用温度',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: '配料',
          excerpt:
            '水，大麥麥芽，小麥，燕麥，啤酒花，酵母，櫻桃濃縮汁 櫻桃味，香菜，橙皮，甜味劑：三氯蔗糖。',
        },
      ],
    },
    section_5: {
      excerpt: <> 訂閱我們的時事通訊 店鋪開業第一時間通知</>,
      link: '使者',
      placeholder: '選民電子郵件',
      visit_page: '後退',
      download: '产品表'
    },
  },
  //////////// CITRON //////////////////////////
  citron: {
    section_1: {
      text: <>一款添加柠檬汁的原味啤酒</>,
      excerpt: '比利时啤酒厂成立于1859年',
    },
    section_2: {
      header: 'Citron',
      text: '一種低酒精度的啤酒，帶有淡淡的檸檬味。 清爽又喜慶，其酸酸甜甜的口感絕對驚艷！',
    },
    section_3: {
      ul: [
        '100%手工酿造',
        '柠檬汁的馥郁香气',
        '100%天然果香',
        '未经过滤的味道',
        '色泽明媚，酒体浑厚',
      ],
      header: '爱上它的5个理由',
    },
    section_4: {
      span_color: 'citron_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: '规格',
          excerpt: (
            <>
              <b>瓶装：</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: '保质期',
          excerpt: (
            <>
              <b>瓶装：</b> 18 个月
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: '酒精度',
          excerpt: '2.3 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: '饮用温度',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: '配料',
          excerpt:
            '水，大麥麥芽，小麥，燕麥，啤酒花， 酵母，濃縮檸檬汁，檸檬提取物，橙汁提取物，橙汁濃縮物，香菜，橙皮，甜味劑 : 三氯蔗糖。',
        },
      ],
    },
    section_5: {
      excerpt: <> 訂閱我們的時事通訊 店鋪開業第一時間通知</>,
      link: '使者',
      placeholder: '選民電子郵件',
      visit_page: '後退',
      download: '产品表'
    },
  },
  ////////////////PECHE////////////////////
  peche: {
    section_1: {
      text: <>一款添加桃汁的原味啤酒</>,
      excerpt: '比利时啤酒厂成立于1859年',
    },
    section_2: {
      header: 'Pêche',
      text: '它的長袍讓你想起了成熟的桃子肉？ 難怪，我們添加了最好的水果！ 令人愉悅的新鮮果味，讓您沉醉在其甜美的香味中。',
    },
    section_3: {
      ul: [
        '100%手工酿造',
        '桃汁的馥郁香气',
        '100%天然水果香甜味',
        '呈桃子的深黄色',
        '橙黄色的泡沫',
      ],
      header: '爱上它的5个理由',
    },
    section_4: {
      span_color: 'peche_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: '规格',
          excerpt: (
            <>
              <b>瓶装：</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: '保质期',
          excerpt: (
            <>
              <b>瓶装：</b> 18 个月
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: '酒精度',
          excerpt: '4.2 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: '饮用温度',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: '配料',
          excerpt:
            '水，大麥麥芽，小麥，燕麥，啤酒花，酵母，濃縮桃汁，桃子味，芒果味 , 香菜，橙皮，甜味劑：三氯蔗糖。',
        },
      ],
    },
    section_5: {
      excerpt: <> 訂閱我們的時事通訊 店鋪開業第一時間通知</>,
      link: '使者',
      placeholder: '選民電子郵件',
      visit_page: '後退',
      download: '产品表'
    },
  },
  ////////// ROSE /////////////
  rose: {
    section_1: {
      text: <>一款添加树莓汁的原味啤酒</>,
      excerpt: '比利时啤酒厂成立于1859年',
    },
    section_2: {
      header: 'Rose',
      text: '在柔和的粉紅色長袍的誘惑下，探索白啤酒微妙的苦味與覆盆子的甜味的結合，帶來清新的果味體驗！',
    },
    section_3: {
      ul: [
        '100%手工酿造',
        '树莓汁的馥郁香气 ',
        '100%天然果香',
        '色泽多彩，酒体浑厚',
        '甜白啤酒',
      ],
      header: '爱上它的5个理由',
    },
    section_4: {
      span_color: 'rose_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: '规格',
          excerpt: (
            <>
              <b> 瓶装：</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: '保质期',
          excerpt: (
            <>
              <b> 瓶装：</b> 18 个月
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: '酒精度',
          excerpt: '3.5 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: '饮用温度',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: '配料',
          excerpt:
            '水，大麥麥芽，小麥，燕麥，啤酒花，酵母，覆盆子濃縮汁，覆盆子香氣，香菜，橙皮，甜味劑：三氯蔗糖。',
        },
      ],
    },
    section_5: {
      excerpt: <> 訂閱我們的時事通訊 店鋪開業第一時間通知</>,
      link: '使者',
      placeholder: '選民電子郵件',
      visit_page: '後退',
      download: '产品表'
    },
  },
};

export default cn;
