const de ={
    lang: "de",
    arrow_down_text:'scrollen',
    blanche: {
        section_1: {
          text: (
            <>
              Das feine, subtile,
              <br />
              leichte & elegante Bier
            </>
          ),
          excerpt: <>Die belgische Brauerei wurde 1859 <br />
          gegründet </>,
        },
        section_2: {
          header: 'Blanche',
          text: 'Ein feines, dezentes und elegantes Bier, eine sanfte Note von Koriander und Orange für einen besonders ausgewogenen Geschmack. Eine herrliche Frische!',
        },
        section_3: {
          ul: [
            'Seine 100% handwerkliche Herstellung',
            'Sein ungefilterter Geschmack',
            'Seine süße Note von Koriander und Orange',
            'Ihre Kraft, die sie verwirrt',
            'Sein schöner weißer Schaum',
          ],
          header: 'gute Gründe, es zu lieben',
        },
        section_4: {
          span_color: 'blanche_span_color',
          ul: [
            {
              icon: 'bottle-icon',
              title: 'FORMAT',
              excerpt: (
                <>
                  <b>Flasche: </b> 0,33L / 0,20L
                </>
              ),
            },
            {
              icon: 'watch-icon',
              title: 'DAUER DER AUFBEWAHRUNG',
              excerpt: (
                <>
                  <b>Flasche: </b> 14 Monate
                </>
              ),
            },
            {
              icon: 'glass-icon',
              title: 'ALKOHOLGEHALT',
              excerpt: '5.0 % VOL.',
            },
            {
              icon: 'termometar-icon',
              title: 'SERVICE',
              excerpt: '3-4 °C',
            },
            {
              icon: 'ingredients-icon',
              title: 'ZUTATEN',
              excerpt:
                'Wasser, Gerstenmalz, Weizen, Hafer, Hopfen, Hefe, Koriander, Orangenschale, Süßstoff: Sucralose.',
            },
          ],
        },
        section_5: {
          excerpt: (
            <>
              {' '}
         Melden Sie sich an, um informiert zu werden, wenn unser E-Shop öffnet
            </>
          ),
          link: 'SENDEN',
          placeholder: "Ihre E-Mail",
           visit_page: 'Zurück',
           download: 'Produktblatt'
         
        },
      },

      /////////////////BLONDE ///////////
      blonde: {
        section_1: {
          text: (
            <>
              Das natürliche,
              <br />
              erfrischende Bier
              <br />
              mit goldgelben Tönen
            </>
          ),
          excerpt: <>Die belgische Brauerei wurde 1859 <br />
          gegründet </>,
        },
        section_2: {
          header: 'Blonde',
          text: 'Sein goldenes Gewand spiegelt sich in seinem leichten Geschmack nach Malz und Hopfen wider. DAS ultimative Bier! Eine süße Bitterkeit, die den Genuss des Trinkens steigert!',
        },
        section_3: {
          ul: [
            'Seine 100% handwerkliche Herstellung',
            'Sein leichter Geschmack nach Malz und Hopfen',
            'Seine angenehme Bitterkeit',
            'ein schöner weißer Schaum',
            'Sein strohgelbes Kleid',
          ],
          header: 'gute Gründe, es zu lieben',
        },
        section_4: {
          span_color: 'blonde_span_color',
          ul: [
            {
              icon: 'bottle-icon',
              title: 'FORMAT',
              excerpt: (
                <>
                  <b>Flasche:</b> 0,33L / 0,20L
                </>
              ),
            },
            {
              icon: 'watch-icon',
              title: 'DAUER DER AUFBEWAHRUNG',
              excerpt: (
                <>
                  <b>Flasche:</b> 14 Monate
                </>
              ),
            },
            {
              icon: 'glass-icon',
              title: 'ALKOHOLGEHALT',
              excerpt: '5.0 % VOL.',
            },
            {
              icon: 'termometar-icon',
              title: 'service',
              excerpt: '3-4 °C',
            },
            {
              icon: 'ingredients-icon',
              title: 'ZUTATEN',
              excerpt: 'Wasser, Gerstenmalz, Hopfen, Hefe.',
            },
          ],
        },
        section_5: {
          excerpt: (
            <>
              {' '}
         Melden Sie sich an, um informiert zu werden, wenn unser E-Shop öffnet
            </>
          ),
          link: 'SENDEN',
          placeholder: "Ihre E-Mail",
           visit_page: 'Zurück',
           download: 'Produktblatt'
        },
      },
      ///////// CERISE ////////////////////
      cerise: {
        section_1: {
          text: (
            <>
             Natürliches Bier

              <br />
              mit Kirschsaft
            </>
          ),
          excerpt: <>Die belgische Brauerei wurde 1859 <br />
          gegründet </>,
        },
        section_2: {
          header: 'Cerise',
          text: 'Sein Gewand verrät keinen Zweifel: Es ist tatsächlich echter Bio-Kirschsaft, der harmonisch zu diesem 100% Craft Beer passt! Hinter dem Geschmack dieser köstlich saftigen roten Frucht verbirgt sich eine überraschend süß-säuerliche Note, ein Geschmack, der uns in die Kindheit zurückversetzt.',
        },
        section_3: {
          ul: [
            'Seine 100% handwerkliche Herstellung',
            'Sein zarter Griebesaft',
            'Seine 100% natürliche fruchtige Note',
            'Seine überraschend frische süß-saure Note',
            'Sein rotes und bezauberndes Kleid',
          ],
          header: 'gute Gründe, es zu lieben',
        },
        section_4: {
            span_color: "cerise_span_color",
          ul: [
            {
              icon: 'bottle-icon',
              title: 'FORMAT',
              excerpt: (
                <>
                  <b>Flasche:</b> 0,33L / 0,20L
                </>
              ),
            },
            {
              icon: 'watch-icon',
              title: 'DAUER DER AUFBEWAHRUNG',
              excerpt: (
                <>
                  <b>Flasche:</b> 18 Monate
                </>
              ),
            },
            {
              icon: 'glass-icon',
              title: 'ALKOHOLGEHALT',
              excerpt: '4.0 % VOL.',
            },
            {
              icon: 'termometar-icon',
              title: 'service',
              excerpt: '3-4 °C',
            },
            {
              icon: 'ingredients-icon',
              title: 'ZUTATEN',
              excerpt:
                'Wasser, Gerstenmalz, Weizen, Hafer, Hopfen, Kirschsaftkonzentrat, Kirschgeschmack, Koriander, Orangenschale, Süßstoff: Sucralose.',
            },
          ],
        },
        section_5: {
          excerpt: (
            <>
              {' '}
         Melden Sie sich an, um informiert zu werden, wenn unser E-Shop öffnet
            </>
          ),
          link: 'SENDEN',
          placeholder: "Ihre E-Mail",
           visit_page: 'Zurück',
           download: 'Produktblatt'
        },
      },
      //////////// CITRON //////////////////////////
      citron: {
        section_1: {
          text: (
            <>
             Natürliches Bier

              <br />
              mit Zitronensaft
            </>
          ),
          excerpt: <>Die belgische Brauerei wurde 1859 <br />
          gegründet </>,
        },
        section_2: {
          header: 'Citron',
          text: 'Ein alkoholarmes Bier mit feinem Zitronengeschmack. Erfrischend und festlich ist sein süß-saurer Geschmack absolut fantastisch!',
        },
        section_3: {
          ul: [
            'Seine 100% handwerkliche Herstellung',
            'Sein zarter Zitronensaft',
            'Seine 100% natürliche fruchtige Note',
            'Sein ungefilterter Geschmack',
            'Sein bewölktes und sonniges Kleid',
          ],
          header: 'gute Gründe, es zu lieben',
        },
        section_4: {
            span_color: "citron_span_color",
          ul: [
            {
              icon: 'bottle-icon',
              title: 'FORMAT',
              excerpt: (
                <>
                  <b>Flasche:</b> 0,33L / 0,20L
                </>
              ),
            },
            {
              icon: 'watch-icon',
              title: 'DAUER DER AUFBEWAHRUNG',
              excerpt: (
                <>
                  <b>Flasche:</b> 18 Monate
                </>
              ),
            },
            {
              icon: 'glass-icon',
              title: 'ALKOHOLGEHALT',
              excerpt: '2.3 % VOL.',
            },
            {
              icon: 'termometar-icon',
              title: 'service',
              excerpt: '3-4 °C',
            },
            {
              icon: 'ingredients-icon',
              title: 'ZUTATEN',
              excerpt:
                'Wasser, Gerstenmalz, Weizen, Hafer, Hopfen, Hefe, Zitronensaftkonzentrat, Zitronenextrakt, Orangenextrakt, Orangensaftkonzentrat, Koriander, Orangenschale, Süßstoff: Sucralose.',
            },
          ],
        },
        section_5: {
          excerpt: (
            <>
              {' '}
         Melden Sie sich an, um informiert zu werden, wenn unser E-Shop öffnet
            </>
          ),
          link: 'SENDEN',
          placeholder: "Ihre E-Mail",
           visit_page: 'Zurück',
           download: 'Produktblatt'
        },
      },
      ////////////////PECHE////////////////////
      peche: {
        section_1: {
          text: (
            <>
            Natürliches Bier

              <br />
              mit Pfirsichsaft
            </>
          ),
          excerpt: <>Die belgische Brauerei wurde 1859 <br />
          gegründet </>,
        },
        section_2: {
          header: 'Pêche',
          text: 'Sein Gewand erinnert Sie an das Fleisch eines reifen Pfirsichs? Kein Wunder, wir haben das Beste aus den Früchten hinzugefügt! Herrlich frisch und fruchtig, lassen Sie sich von der Süße seines Duftes mitreißen.',
        },
        section_3: {
          ul: [
            'Seine 100% handwerkliche Herstellung',
            'Seine 100% handwerkliche Herstellung',
            'Seine fruchtige und süße Note 100% natürlich',
            'Seine dunkelgelbe Pfirsichfarbe',
            'Sein orange-gelber Schaum',
          ],
          header: 'gute Gründe, es zu lieben',
        },
        section_4: {
            span_color: "peche_span_color",
          ul: [
            {
              icon: 'bottle-icon',
              title: 'FORMAT',
              excerpt: (
                <>
                  <b>Flasche: </b> 0,33L / 0,20L
                </>
              ),
            },
            {
              icon: 'watch-icon',
              title: 'DAUER DER AUFBEWAHRUNG',
              excerpt: (
                <>
                  <b>Flasche: </b> 18 mois
                </>
              ),
            },
            {
              icon: 'glass-icon',
              title: 'ALKOHOLGEHALT',
              excerpt: '4.2 % VOL.',
            },
            {
              icon: 'termometar-icon',
              title: 'service',
              excerpt: '3-4 °C',
            },
            {
              icon: 'ingredients-icon',
              title: 'ZUTATEN',
              excerpt:
                'Wasser, Gerstenmalz, Weizen, Hafer, Hopfen, Hefe, Pfirsichsaftkonzentrat, Pfirsicharoma, Mangoaroma, Koriander, Orangenschale, Süßstoff: Sucralose.',
            },
          ],
        },
        section_5: {
          excerpt: (
            <>
              {' '}
         Melden Sie sich an, um informiert zu werden, wenn unser E-Shop öffnet
            </>
          ),
          link: 'SENDEN',
          placeholder: "Ihre E-Mail",
           visit_page: 'Zurück',
           download: 'Produktblatt'
        },
      },
      ////////// ROSE /////////////
      rose: {
        section_1: {
          text: (
            <>
             Natürliches Bier

              <br />
              mit Himbeersaft
            </>
          ),
          excerpt: <>Die belgische Brauerei wurde 1859 <br />
          gegründet </>,
        },
        section_2: {
          header: 'Rose',
          text: 'Verführt von seinem zartrosa Gewand, entdecken Sie die Verbindung der zarten Bitterkeit eines Weißbiers mit der süßen Note der Himbeere für ein erfrischendes und fruchtiges Erlebnis!',
        },
        section_3: {
          ul: [
            'Seine 100% handwerkliche Herstellung',
            'Sein zarter Himbeersaft',
            'Seine 100% natürliche fruchtige Note',
            'Sein wolkiges und buntes Kleid',
            'Seine süße Weißbierseite',
          ],
          header: 'gute Gründe, es zu lieben',
        },
        section_4: {
            span_color: "rose_span_color",
          ul: [
            {
              icon: 'bottle-icon',
              title: 'FORMAT',
              excerpt: (
                <>
                  <b>Flasche:</b> 0,33L / 0,20L
                </>
              ),
            },
            {
              icon: 'watch-icon',
              title: 'ALKOHOLGEHALT',
              excerpt: (
                <>
                  <b>Flasche:</b> 18 Monate
                </>
              ),
            },
            {
              icon: 'glass-icon',
              title: 'ALKOHOLGEHALT',
              excerpt: '3.5 % VOL.',
            },
            {
              icon: 'termometar-icon',
              title: 'service',
              excerpt: '3-4 °C',
            },
            {
              icon: 'ingredients-icon',
              title: 'ZUTATNE',
              excerpt:
                'Wasser, Gerstenmalz, Weizen, Hafer, Hopfen, Hefe, Himbeersaftkonzentrat, Himbeeraroma, Koriander, Orangenschale,Süßstoff: Sucralose.',
            },
          ],
        },
        section_5: {
          excerpt: (
            <>
              {' '}
         Melden Sie sich an, um informiert zu werden, wenn unser E-Shop öffnet
            </>
          ),
          link: 'SENDEN',
          placeholder: "Ihre E-Mail",
           visit_page: 'Zurück',
           download: 'Produktblatt'
        },
      },
}

export default de;