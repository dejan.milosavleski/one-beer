const ES = {
  lang: 'es',
  arrow_down_text:'hacer desfilar',
  blanche: {
    section_1: {
      text: (
        <>
          La cerveza fina, sutil,
          <br />
          ligera y elegante
        </>
      ),
      excerpt: 'Cervecería belga fundada en 1859',
    },
    section_2: {
      header: 'Blanche',
      text: 'Una cerveza fina, sutil y elegante, una suave nota de cilantro y naranja para un sabor particularmente equilibrado. ¡Una deliciosa frescura!',
    },
    section_3: {
      ul: [
        'Su fabricación 100% artesanal',
        'Su sabor sin filtrar',
        'Su nota dulce de cilantro y naranja',
        'Su fuerza la nubla',
        'Su hermosa espuma blanca',
      ],
      header: 'buenas razones para amarlo',
    },
    section_4: {
      span_color: 'blanche_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMATO',
          excerpt: (
            <>
              <b>Botella:</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'VIDA ÚTIL',
          excerpt: (
            <>
              <b>Botella:</b> 14 meses
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'CONTENIDO DE ALCOHOL',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'SERVICIO',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTES',
          excerpt:
            'agua, malta de cebada,trigo, avena, lúpulo, levadura, cilantro, piel de naranja, edulcorante: sucralosa',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>Regístrese para recibir información cuando abra nuestra tienda online</>
      ),
      link: 'enviar a',
      placeholder: "Su email",
       visit_page: 'Espalda',
       download : "Ficha de producto"
     
    },
  },
  //////////// BLONDE/////////////
  blonde: {
    section_1: {
      text: (
        <>
          La cerveza natural,
          <br />
          refrescante, con tonos
          <br />
          amarillos dorados
        </>
      ),
      excerpt: 'Cervecería belga fundada en 1859',
    },
    section_2: {
      header: 'Blonde',
      text: 'Su capa dorada se refleja en su ligero sabor a malta y lúpulo.¡La cerveza definitiva! ¡Un dulce amargor que potencia el placer de beberlo!',
    },
    section_3: {
      ul: [
        'Su fabricación 100% artesanal',
        'Su ligero sabor a malta y lúpulo.',
        'Su agradable amargura',
        'Su hermosa espuma blanca',
        'Su vestido amarillo paja',
      ],
      header: 'buenas razones para amarlo',
    },
    section_4: {
      span_color: 'blonde_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMATO',
          excerpt: (
            <>
              <b> Botella:</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'VIDA ÚTIL',
          excerpt: (
            <>
              <b> Botella:</b> 14 meses
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'CONTENIDO DE ALCOHOL',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'SERVICIO',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTES',
          excerpt: 'agua, malta de cebada, lúpulo, levadura.',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>Regístrese para recibir información cuando abra nuestra tienda online</>
      ),
      link: 'enviar a',
      placeholder: "Su email",
       visit_page: 'Espalda',
       download : "Ficha de producto"
    },
  },
  /////////// CERISE ////////////
  cerise: {
    section_1: {
      text: (
        <>
          Cerveza natural
          <br />
          con jugo de cereza
        </>
      ),
      excerpt: 'Cervecería belga fundada en 1859',
    },
    section_2: {
      header: 'Cerise',
      text: 'Su manto no deja lugar a dudas: ¡es un auténtico zumo de cereza ecológico el que combina armoniosamente con esta cerveza 100% artesanal! Detrás del sabor de esta fruta roja deliciosamente jugosa, hay un toque sorprendentemente agridulce, un sabor que nos remonta a la infancia.',
    },
    section_3: {
      ul: [
        'Su fabricación 100% artesanal',
        'Su delicado jugo de guindas',
        'Su nota afrutada y dulce 100% natural',
        'Su sorprendente y fresco toque agridulce',
        'Su vestido rojo y hechizante',
      ],
      header: 'buenas razones para amarlo',
    },
    section_4: {
      span_color: 'cerise_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMATO',
          excerpt: (
            <>
              <b> Botella:</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'VIDA ÚTIL',
          excerpt: (
            <>
              <b> Botella:</b> 18 meses
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'CONTENIDO DE ALCOHOL',
          excerpt: '4.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'SERVICIO',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTES',
          excerpt:
            'agua, malta de cebada, trigo, avena, lúpulo, levadura, concentrado de jugo de cereza, sabor a cereza, cilantro, piel de naranja, edulcorante: sucralosa.',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>Regístrese para recibir información cuando abra nuestra tienda online</>
      ),
      link: 'enviar a',
      placeholder: "Su email",
       visit_page: 'Espalda',
       download : "Ficha de producto"
    },
  },
  //////////// CITRON //////////////////////////
  citron: {
    section_1: {
      text: (
        <>
          Cerveza natural
          <br />
          con jugo de limón
        </>
      ),
      excerpt: 'Cervecería belga fundada en 1859',
    },
    section_2: {
      header: 'Citron',
      text: 'Una cerveza de bajo contenido alcohólico con un delicado sabor a limón. Refrescante y festivo, ¡su sabor agridulce es absolutamente increíble!',
    },
    section_3: {
      ul: [
        'Su fabricación 100% artesanal',
        'Su delicado jugo de limón',
        'Su nota afrutada 100% natural',
        'Su sabor sin filtrar',
        'Su vestido nublado y soleado',
      ],
      header: 'buenas razones para amarlo',
    },
    section_4: {
      span_color: 'citron_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMATO',
          excerpt: (
            <>
              <b>Botella:</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'VIDA ÚTIL',
          excerpt: (
            <>
              <b>Botella:</b> 18 meses
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'CONTENIDO DE ALCOHOL',
          excerpt: '2.3 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'SERVICIO',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTES',
          excerpt:
            'agua, malta de cebada, trigo, avena, lúpulo, levadura, concentrado de jugo de limón, extracto de limón, extracto de naranja, concentrado de jugo de naranja, cilantro, piel de naranja, edulcorante: sucralosa.',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>Regístrese para recibir información cuando abra nuestra tienda online</>
      ),
      link: 'enviar a',
      placeholder: "Su email",
       visit_page: 'Espalda',
       download : "Ficha de producto"
    },
  },
  ////////////////PECHE////////////////////
  peche: {
    section_1: {
      text: (
        <>
          Cerveza natural
          <br />
          con jugo de melocotón
        </>
      ),
      excerpt: 'Cervecería belga fundada en 1859',
    },
    section_2: {
      header: 'Pêche',
      text: '¿Su manto te recuerda a la carne de un melocotón maduro? No es de extrañar, ¡agregamos lo mejor de la fruta! Deliciosamente fresco y afrutado, déjate llevar por la dulzura de su aroma.',
    },
    section_3: {
      ul: [
        'Su fabricación 100% artesanal',
        'Su delicado jugo de melocotón',
        'Su nota afrutada y dulce 100% natural',
        'Su vestido de color amarillo oscuro de melocotón',
        'Su espuma naranja-amarilla',
      ],
      header: 'buenas razones para amarlo',
    },
    section_4: {
      span_color: 'peche_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMATO',
          excerpt: (
            <>
              <b> Botella:</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'VIDA ÚTIL',
          excerpt: (
            <>
              <b> Botella:</b> 18 meses
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'CONTENIDO DE ALCOHOL',
          excerpt: '4.2 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'SERVICIO',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTES',
          excerpt:
            'agua, malta de cebada, trigo, avena, lúpulo, levadura, jugo de durazno concentrado, aroma de melocotón, aroma de mango, cilantro, piel de naranja, edulcorante: sucralosa',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>Regístrese para recibir información cuando abra nuestra tienda online</>
      ),
      link: 'enviar a',
      placeholder: "Su email",
       visit_page: 'Espalda',
       download : "Ficha de producto"
    },
  },
  ////////// ROSE /////////////
  rose: {
    section_1: {
      text: (
        <>
          Cerveza natural
          <br />
          con jugo de frambuesa
        </>
      ),
      excerpt: 'Cervecería belga fundada en 1859',
    },
    section_2: {
      header: 'Rose',
      text: 'Seducido por su suave capa rosa, descubre la alianza del delicado amargor de una cerveza blanca con la dulce nota de frambuesa para una experiencia refrescante y afrutada.'
    },
    section_3: {
      ul: [
        'Su fabricación 100% artesanal',
        'Su delicado jugo de frambuesa',
        'Su nota afrutada 100% natural',
        'Su nota afrutada 100% natural',
        'Su lado de cerveza blanca dulce',
      ],
      header: 'buenas razones para amarlo',
    },
    section_4: {
      span_color: 'rose_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMATO',
          excerpt: (
            <>
              <b> Botella:</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'VIDA ÚTIL',
          excerpt: (
            <>
              <b> Botella:</b> 18 meses
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'CONTENIDO DE ALCOHOL',
          excerpt: '3.5 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'SERVICIO',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTES',
          excerpt:
            'agua, malta de cebada, trigo, avena, lúpulo, levadura, concentrado de jugo de frambuesa, aroma de frambuesa, cilantro, piel de naranja, edulcorante: sucralosa',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>Regístrese para recibir información cuando abra nuestra tienda online</>
      ),
      link: 'enviar a',
      placeholder: "Su email",
       visit_page: 'Espalda',
       download : "Ficha de producto"
    },
  },
};

export default ES;
