const NL = {
    lang: "nl",
    arrow_down_text:"rol",
    blanche: {
      section_1: {
        text: (
          <>
         Fijn, subtiel,

            <br />
            licht en elegant bier
          </>
        ),
       

        excerpt: <> Belgische brouwerij opgericht <br />in 1859</>,
      },
      section_2: {
        header: 'Blanche',
        text: 'Een fijn, subtiel en elegant bier, een zachte toon van koriander en sinaasappel voor een bijzonder evenwichtige smaak. Een heerlijke frisheid!',
      },
      section_3: {
        ul: [
          'Zijn 100% ambachtelijk brouwproces',
          'Zijn ongefilterde smaak',
          'Zijn zoete smaak van koriander en sinaasappel',
          'Zijn krachtige troebelheid',
          'Zijn mooie witte schuimkraag',
        ],
        header: 'goede redenen om dit bier te smaken',
      },
      section_4: {
        span_color: 'blanche_span_color',
        ul: [
          {
            icon: 'bottle-icon',
            title: 'FORMAAT',
            excerpt: (
              <>
                <b>Fles:</b> 0,33L / 0,20L
              </>
            ),
          },
          {
            icon: 'watch-icon',
            title: 'HOUDBAARHEIDSDUUR',
            excerpt: (
              <>
                <b>Fles:</b> 14 maand
              </>
            ),
          },
          {
            icon: 'glass-icon',
            //casha
    title: 'ALCOHOLGEHALTE',
            excerpt: '5.0 % VOL.',
          },
          {
            icon: 'termometar-icon',
            title: 'SERVICE',
            excerpt: '3-4 °C',
          },
          {
            icon: 'ingredients-icon',
            title: 'INGREDIËNTEN',
            excerpt:
              'water, gerstemout, tarwe, haver, hop, gist, koriander, sinaasappelschil, zoetstof: sucralose.',
          },
        ],
      },
      section_5: {
        excerpt: (
          <>
            {' '}
        Inscrivez vous pour être informé dès l’ouverture de notre e-shop
          </>
        ),
        link: 'ENVOYER',
        placeholder: "Uw e-mail",
        
         visit_page: 'Terug',
         download: "productblad"
       
      },
    },
    //////////// BLONDE/////////////
    blonde: {
      section_1: {
        text: (
          <>
           Natuurlijk, verfrissend

            <br />
            bier met goudgele toets
      
          </>
        ),
        excerpt: <> Belgische brouwerij opgericht <br />in 1859</>,
      },
      section_2: {
        header: 'Blonde',
        text: 'Zijn gouden mantel wordt weerspiegeld in zijn lichte smaak van mout en hop. HET ultieme bier! Een zoete bitterheid die het drinkgenot vergroot!',
      },
      section_3: {
        ul: [
            'Zijn 100% ambachtelijk brouwproces',
            'Zijn lichte mouten hopsmaak',
            'Zijn aangename bitterheid',
            'Zijn mooie witte schuimkraag',
            'Zijn strogele kleur',
        ],
        header: 'goede redenen om dit bier te smaken',
      },
      section_4: {
        span_color: 'blonde_span_color',
        ul: [
          {
            icon: 'bottle-icon',
            title: 'FORMAAT',
            excerpt: (
              <>
                <b> Fles:</b> 0,33L / 0,20L
              </>
            ),
          },
          {
            icon: 'watch-icon',
            title: 'HOUDBAARHEIDSDUUR',
            excerpt: (
              <>
                <b> Fles:</b> 14 maand
              </>
            ),
          },
          {
            icon: 'glass-icon',
            title: 'ALCOHOLGEHALTE',
            excerpt: '5.0 % VOL.',
          },
          {
            icon: 'termometar-icon',
            title: 'SERVICE',
            excerpt: '3-4 °C',
          },
          {
            icon: 'ingredients-icon',
            title: 'INGREDIËNTEN',
            excerpt: 'water, gerstemout, hop, gist.',
          },
        ],
      },
      section_5: {
        excerpt: (
          <>
            {' '}
        Inscrivez vous pour être informé dès l’ouverture de notre e-shop
          </>
        ),
        link: 'ENVOYER',
        placeholder: "Uw e-mail",
         visit_page: 'Terug',
         download: "productblad"
      },
    },
    /////////// CERISE ////////////
    cerise: {
      section_1: {
        text: (
          <>
           Natuurlijk bier

            <br />
            met kersensap
          </>
        ),
        excerpt: <> Belgische brouwerij opgericht <br />in 1859</>,
      },
      section_2: {
        header: 'Cerise',
        text: 'Zijn mantel verraadt geen twijfel: het is inderdaad echt biologisch kersensap dat harmonieus past bij dit 100% ambachtelijke bier! Achter de smaak van deze heerlijk sappige rode vrucht zit een verrassend zoetzure toets, een smaak die ons terugbrengt naar de kindertijd.',
      },
      section_3: {
        ul: [
            'Zijn 100% ambachtelijk brouwproces',
            'Zijn overheerlijk kersensap',
            'Zijn 100% natuurlijk fruitige smaak',
            'Zijn verrassend frisse zoetzure toets',
            'Zijn betoverend rode kleur',
        ],
        header: 'goede redenen om dit bier te smaken',
      },
      section_4: {
          span_color: "cerise_span_color",
        ul: [
          {
            icon: 'bottle-icon',
            title: 'FORMAAT',
            excerpt: (
              <>
                <b> Fles:</b> 0,33L / 0,20L
              </>
            ),
          },
          {
            icon: 'watch-icon',
            title: 'HOUDBAARHEIDSDUUR',
            excerpt: (
              <>
                <b> Fles:</b> 18 maand
              </>
            ),
          },
          {
            icon: 'glass-icon',
            title: 'ALCOHOLGEHALTE',
            excerpt: '4.0 % VOL.',
          },
          {
            icon: 'termometar-icon',
            title: 'SERVICE',
            excerpt: '3-4 °C',
          },
          {
            icon: 'ingredients-icon',
            title: 'INGREDIËNTEN',
            excerpt:
              'water, gerstemout, tarwe, haver, hop, gist, kriekensapconcentraat, kriekenaroma, koriander, sinaasappelschil, zoetstof: sucralose.',
          },
        ],
      },
      section_5: {
        excerpt: (
          <>
            {' '}
        Inscrivez vous pour être informé dès l’ouverture de notre e-shop
          </>
        ),
        link: 'ENVOYER',
        placeholder: "Uw e-mail",
         visit_page: 'Terug',
         download: "productblad"
      },
    },
    //////////// CITRON //////////////////////////
    citron: {
      section_1: {
        text: (
          <>
          Natuurlijk bier

            <br />
            met citroensap
          </>
        ),
        excerpt: <> Belgische brouwerij opgericht <br />in 1859</>,
      },
      section_2: {
        header: 'Citron',
        text: 'Een laag alcoholisch bier met een delicate citroensmaak. Verfrissend en feestelijk, de zoetzure smaak is absoluut geweldig!',
      },
      section_3: {
        ul: [
            'Zijn 100% ambachtelijk brouwproces',
          'Zijn overheerlijk citroensap',
          'Zijn 100% natuurlijk fruitige smaak',
          'Zijn ongefilterde smaak',
          'Zijn troebele, zonnige kleur',
        ],
        header: 'goede redenen om dit bier te smaken',
      },
      section_4: {
          span_color: "citron_span_color",
        ul: [
          {
            icon: 'bottle-icon',
            title: 'FORMAAT',
            excerpt: (
              <>
                <b>Fles:</b> 0,33L / 0,20L
              </>
            ),
          },
          {
            icon: 'watch-icon',
            title: 'HOUDBAARHEIDSDUUR',
            excerpt: (
              <>
                <b>Fles:</b> 18 maand
              </>
            ),
          },
          {
            icon: 'glass-icon',
            title: 'ALCOHOLGEHALTE',
            excerpt: '2.3 % VOL.',
          },
          {
            icon: 'termometar-icon',
            title: 'SERVICE',
            excerpt: '3-4 °C',
          },
          {
            icon: 'ingredients-icon',
            title: 'INGREDIËNTEN',
            excerpt:
              'water, gerstemout, tarwe, haver, hop, gist, citroensapconcentraat, citroenextract, sinaasappelextract, sinaasappelsap-concentraat, koriander, sinaasappelschil, zoetstof: sucralose.',
          },
        ],
      },
      section_5: {
        excerpt: (
          <>
            {' '}
        Inscrivez vous pour être informé dès l’ouverture de notre e-shop
          </>
        ),
        link: 'ENVOYER',
        placeholder: "Uw e-mail",
         visit_page: 'Terug',
         download: "productblad"
      },
    },
    ////////////////PECHE////////////////////
    peche: {
      section_1: {
        text: (
          <>
          Natuurlijk bier

            <br />
            met perziksap
          </>
        ),
        excerpt: <> Belgische brouwerij opgericht <br />in 1859</>,
      },
      section_2: {
        header: 'Pêche',
        text: 'Zijn mantel doet je denken aan het vruchtvlees van een rijpe perzik? Geen wonder, we hebben het allerbeste van het fruit toegevoegd! Heerlijk fris en fruitig, laat je meeslepen door de zoetheid van zijn geur.',
      },
      section_3: {
        ul: [
            'Zijn 100% ambachtelijk brouwproces',
          'Zijn overheerlijk perziksap',
          'Zijn 100% natuurlijk fruitige en zoete smaak',
          'Zijn diepgele perzikkleur',
          'Zijn oranjegele schuimkraag',
        ],
        header: 'goede redenen om dit bier te smaken',
      },
      section_4: {
          span_color: "peche_span_color",
        ul: [
          {
            icon: 'bottle-icon',
            title: 'FORMAAT',
            excerpt: (
              <>
                <b>Fles:</b> 0,33L / 0,20L
              </>
            ),
          },
          {
            icon: 'watch-icon',
            title: 'HOUDBAARHEIDSDUUR',
            excerpt: (
              <>
                <b>Fles:</b> 18 maand
              </>
            ),
          },
          {
            icon: 'glass-icon',
            title: 'ALCOHOLGEHALTE',
            excerpt: '4.2 % VOL.',
          },
          {
            icon: 'termometar-icon',
            title: 'SERVICE',
            excerpt: '3-4 °C',
          },
          {
            icon: 'ingredients-icon',
            title: 'INGREDIËNTEN',
            excerpt:
              'water, gerstemout, tarwe, haver, hop, gist, perziksapconcentraat, perzikaroma, mango-aroma, koriander, sinaasappelschil, zoetstof: sucralose.',
          },
        ],
      },
      section_5: {
        excerpt: (
          <>
            {' '}
        Inscrivez vous pour être informé dès l’ouverture de notre e-shop
          </>
        ),
        link: 'ENVOYER',
        placeholder: "Uw e-mail",
         visit_page: 'Terug',
         download: "productblad"
      },
    },
    ////////// ROSE /////////////
    rose: {
      section_1: {
        text: (
          <>
       Natuurlijk bier

            <br />
            met frambozensap
          </>
        ),
        excerpt: <> Belgische brouwerij opgericht <br />in 1859</>,
      },
      section_2: {
        header: 'Rose',
        text: 'Verleid door zijn zachtroze mantel, ontdek de alliantie van de delicate bitterheid van een witbier met de zoete noot van framboos voor een verfrissende en fruitige ervaring!',
      },
      section_3: {
        ul: [
            'Zijn 100% ambachtelijk brouwproces',
          'Zijn overheerlijk frambozensap',
          'Zijn 100% natuurlijk fruitige smaak',
          'Zijn troebele, rijke kleur',
          'Zijn zoete toets van witbier',
        ],
        header: 'goede redenen om dit bier te smaken',
      },
      section_4: {
          span_color: "rose_span_color",
        ul: [
          {
            icon: 'bottle-icon',
            title: 'FORMAAT',
            excerpt: (
              <>
                <b>Fles:</b> 0,33L / 0,20L
              </>
            ),
          },
          {
            icon: 'watch-icon',
            title: 'HOUDBAARHEIDSDUUR',
            excerpt: (
              <>
                <b>Fles:</b> 18 maand
              </>
            ),
          },
          {
            icon: 'glass-icon',
            title: 'ALCOHOLGEHALTE',
            excerpt: '3.5 % VOL.',
          },
          {
            icon: 'termometar-icon',
            title: 'SERVICE',
            excerpt: '3-4 °C',
          },
          {
            icon: 'ingredients-icon',
            title: 'INGREDIËNTEN',
            excerpt:
              'water, gerstemout, tarwe, haver, hop, gist, frambozensapconcentraat, frambozenaroma, koriander, sinaasappelschil, zoetstof: sucralose.',
          },
        ],
      },
      section_5: {
        excerpt: (
          <>
            {' '}
        Inscrivez vous pour être informé dès l’ouverture de notre e-shop
          </>
        ),
        link: 'ENVOYER',
        placeholder: "Uw e-mail",
         visit_page: 'Terug',
         download: "productblad"
      },
    },
  };
  
  export default NL;
  