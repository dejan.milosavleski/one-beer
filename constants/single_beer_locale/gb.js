const gb = {
  arrow_down_text: 'scroll',
  lang: 'gb',
  blanche: {
    section_1: {
      text: (
        <>
          The light beer,
          <br />
          subtle & elegant
        </>
      ),
      excerpt: 'Belgian brewery founded in 1859',
    },
    section_2: {
      header: 'Blanche',
      text: 'A light-bodied, subtle and elegant beer with a sweet blend of coriander and orange for a perfect balanced taste. Refreshing bliss',
    },
    section_3: {
      ul: [
        '100% artisanal fabrication',
        'Unfiltered real taste',
        'The sweet hint of coriander & orange',
        'The strong cloudy colour',
        'Perfect white froth',
      ],
      header: 'reasons to love it',
    },
    section_4: {
      span_color: 'blanche_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bottle:</b> 0,33L / 0,20 L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'CONSERVATION PERIOD',
          excerpt: (
            <>
              <b>Bottled:</b>14 months
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'alcohol',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'serve',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTS',
          excerpt:
            'water, barley malt, wheat, oats, hops, yeast, coriander, orange peel, sweetener: sucralose.',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
          subscribe to our newsletter to be the first informed of the opening of
          the SHOP
        </>
      ),
      link: 'send',
      placeholder: 'Your email',
      visit_page: 'Back',
      download: 'Product Sheet'
    },
  },
  ///////////BLONDE //////////////
  blonde: {
    section_1: {
      text: (
        <>
          The natural beer,
          <br />
          refreshing, with golden
          <br />
          yellow tones
        </>
      ),
      excerpt: 'Belgian brewery founded in 1859',
    },
    section_2: {
      header: 'blonde',
      text: 'Its malt and hops, which gives this beer its golden colour and light taste. THE ideal beer! A hint of bitterness that wets the appetite!',
    },
    section_3: {
      ul: [
        '100% artisanal fabrication',
        'Light malt & hops taste',
        'The touch of bitter',
        'Perfect white froth',
        'The straw yellow colour',
      ],
      header: 'reasons to love it',
    },
    section_4: {
      span_color: 'blonde_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bottle:</b> 0,33L / 0,20 L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'CONSERVATION PERIOD',
          excerpt: (
            <>
              <b>Bottled:</b>14 months
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'alcohol',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'serve',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTS',
          excerpt: 'water, barley malt, hops, yeast.',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
          subscribe to our newsletter to be the first informed of the opening of
          the SHOP
        </>
      ),
      link: 'send',
      placeholder: 'Your email',
      visit_page: 'Back',
      download: 'Product Sheet'
    },
  },
  ///////////CERISE //////////////
  cerise: {
    section_1: {
      text: (
        <>
          The natural beer,
          <br />
          with a cherry base
        </>
      ),
      excerpt: 'Belgian brewery founded in 1859',
    },
    section_2: {
      header: 'cerise',
      text: 'Its colour says it all: real natural cherry juice is the perfect partner for this 100% craft beer! Combined with this small flavour-packed red fruit lurks a surprisingly sweet-and-sour taste, bringing back childhood memories',
    },
    section_3: {
      ul: [
        '100% artisanal fabrication',
        'Made with delicate cherry juice',
        'The 100% natural fruity flavour',
        'The surprising sweet & sour touch of freshness',
        'The enveloping red colour',
      ],
      header: 'reasons to love it',
    },
    section_4: {
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bottle:</b> 0,33L / 0,20 L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'CONSERVATION PERIOD',
          excerpt: (
            <>
              <b>Bottled:</b>18 months
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'alcohol',
          excerpt: '4.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'serve',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTS',
          excerpt:
            'water, barley malt, wheat, oats, hops, yeast, cherry juice concentrate, cherry flavor, coriander, orange peel, sweetener: sucralose',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
          subscribe to our newsletter to be the first informed of the opening of
          the SHOP
        </>
      ),
      link: 'send',
      placeholder: 'Your email',
      visit_page: 'Back',
      download: 'Product Sheet'
    },
  },
  ///////////CITRON //////////////
  citron: {
    section_1: {
      text: (
        <>
          The natural beer,
          <br />
          with a hint of lemon
        </>
      ),
      excerpt: 'Belgian brewery founded in 1859',
    },
    section_2: {
      header: 'citron',
      text: 'A beer with a very low alcohol content and just a hint of lemon. Refreshing and pleasant, its mild bitter fragrance will make you want more!',
    },
    section_3: {
      ul: [
        '100% artisanal fabrication',
        'Made with delicate lemon juice',
        'The 100% natural fruity flavour',
        'The unfiltered taste',
        'The cloudy bright colour',
      ],
      header: 'reasons to love it',
    },
    section_4: {
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bottle:</b> 0,33L / 0,20 L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'CONSERVATION PERIOD',
          excerpt: (
            <>
              <b>Bottled:</b>18 months
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'alcohol',
          excerpt: '2.3 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'serve',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTS',
          excerpt:
            'water, barley malt, wheat, oats, hops, yeast, lemon juice concentrate, lemon extract, orange extract, orange juice concentrate, coriander, orange peel, sweetener: sucralose',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
          subscribe to our newsletter to be the first informed of the opening of
          the SHOP
        </>
      ),
      link: 'send',
      placeholder: 'Your email',
      visit_page: 'Back',
      download: 'Product Sheet'
    },
  },
  ///////////PECHE //////////////
  peche: {
    section_1: {
      text: (
        <>
          The natural beer,
          <br />
          with a peach base
        </>
      ),
      excerpt: 'Belgian brewery founded in 1859',
    },
    section_2: {
      header: 'Pêche',
      text: 'Does its colour remind you of a ripe peach? That’s because it contains the best of the fruit! Pleasantly fresh and fruity, allow yourself to be seduced by the sweetness of its fragrance',
    },
    section_3: {
      ul: [
        '100% artisanal fabrication',
        'Made with delicate peche juice',
        'The 100% natural fruity flavour',
        'The dark yellow colour reminiscent of a peach',
        'The light yellow foam',
      ],
      header: 'reasons to love it',
    },
    section_4: {
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bottle:</b> 0,33L / 0,20 L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'CONSERVATION PERIOD',
          excerpt: (
            <>
              <b>Bottled:</b>18 months
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'alcohol',
          excerpt: '4.2 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'serve',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTS',
          excerpt:
            'water, barley malt, wheat, oats, hops, yeast, peach juice concentrate, peach, aroma, mango, aroma, coriander, orange peel, sweetener: sucralose',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
          subscribe to our newsletter to be the first informed of the opening of
          the SHOP
        </>
      ),
      link: 'send',
      placeholder: 'Your email',
      visit_page: 'Back',
      download: 'Product Sheet'
    },
  },
  ///////////ROSE //////////////
  rose: {
    section_1: {
      text: (
        <>
          The natural beer,
          <br />
          with a raspbery base
        </>
      ),
      excerpt: 'Belgian brewery founded in 1859',
    },
    section_2: {
      header: 'Rosé',
      text: 'Charmed by its soft rose hue, discover the subtle combination of the slight bitterness of a white beer with the sweet note of raspberries for a refreshing and fruity experience!',
    },
    section_3: {
      ul: [
        '100% artisanal fabrication',
        'Made with delicate raspberry juice',
        'The 100% natural fruity flavour',
        'The cloudy, rich colour',
        'The sweet white beer taste',
      ],
      header: 'reasons to love it',
    },
    section_4: {
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bottle:</b> 0,33L / 0,20 L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'CONSERVATION PERIOD',
          excerpt: (
            <>
              <b>Bottled:</b>18 months
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'alcohol',
          excerpt: '3.5 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'serve',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGREDIENTS',
          excerpt:
            'water, barley malt, wheat, oats, hops, yeast, raspberry juice concentrate, raspberry aroma, coriander, orange peel, sweetener: sucralose',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
          subscribe to our newsletter to be the first informed of the opening of
          the SHOP
        </>
      ),
      link: 'send',
      placeholder: 'Your email',
      visit_page: 'Back',
      download: 'Product Sheet'
    },
  },
};

export default gb;
