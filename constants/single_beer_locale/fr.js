const fr = {
  lang: "fr",
  arrow_down_text:"faire défiler",
  blanche: {
    section_1: {
      text: (
        <>
          La bière fine, subtile,
          <br />
          légère & élégante
        </>
      ),
      excerpt: 'Brasserie Belge fondée en 1859',
    },
    section_2: {
      header: 'Blanche',
      text: 'Une bière fine, subtile et élégante, une douce note de coriandre et d’orange pour un goût particulièrement équilibré. Un bonheur de fraîcheur !',
    },
    section_3: {
      ul: [
        'Sa fabrication 100% artisanale',
        'Son goût non filtré',
        'Sa douce note de coriander et d’orange',
        'Sa force qui la rend trouble',
        'Sa belle mousse blanche',
      ],
      header: 'Bonnes raisons de l’aimer',
    },
    section_4: {
      span_color: 'blanche_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bouteille :</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'DURÉE',
          excerpt: (
            <>
              <b>Bouteille :</b> 14 mois
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'TENEUR EN ALCOOL',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'service',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGRÉDIENTS',
          excerpt:
            'Eau, malt d’orge, froment, avoine,houblon, levure, coriandre, écorce d’orange, edulcorant (sucralose).',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
      Inscrivez vous pour être informé dès l’ouverture de notre e-shop
        </>
      ),
      link: 'ENVOYER',
      placeholder: "Votre E-mail",

      visit_page: 'Retour',
      download: 'Fiche produit'
      
    },
  },
  //////////// BLONDE/////////////
  blonde: {
    section_1: {
      text: (
        <>
          La bière naturelle,
          <br />
          rafraîchissante,
          <br />
          aux tons jaune dorés
        </>
      ),
      excerpt: 'Brasserie Belge fondée en 1859',
    },
    section_2: {
      header: 'Blonde',
      text: 'Sa robe dorée se reflète dans son goût léger de malt et de houblon.LA bière par excellence ! Une douce amertume qui met en appétit !',
    },
    section_3: {
      ul: [
        'Une bière 100% artisanale',
        'Son léger goût de malt et de houblon',
        'Son agréable amertume',
        'Sa belle mousse blanche',
        'Sa robe jaune paille',
      ],
      header: 'Bonnes raisons de l’aimer',
    },
    section_4: {
      span_color: 'blonde_span_color',
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bouteille :</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'DURÉE DE CONSERVATION',
          excerpt: (
            <>
              <b>Bouteille :</b> 14 mois
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'TENEUR EN ALCOOL',
          excerpt: '5.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'service',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGRÉDIENTS',
          excerpt: 'eau, malt d’orge, houblon, levure',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
      Inscrivez vous pour être informé dès l’ouverture de notre e-shop
        </>
      ),
      link: 'ENVOYER',
      placeholder: "Votre E-mail",
      visit_page: 'Retour',
      download: 'Fiche produit'
    },
  },
  /////////// CERISE ////////////
  cerise: {
    section_1: {
      text: (
        <>
          La bière naturelle
          <br />
          au jus de cerise
        </>
      ),
      excerpt: 'Brasserie Belge fondée en 1859',
    },
    section_2: {
      header: 'Cerise',
      text: 'Sa robe n’offre aucun doute : c’est bien du véritable jus de cerise bio qui se marie avec harmonie à cette bière 100 % artisanale ! Derrière la saveur de ce petit fruit rouge et juteux, se cache une touche aigre-douce étonnante, une saveur qui confine à l’enfance.',
    },
    section_3: {
      ul: [
        'Une bière 100% artisanale',
        'Son délicat jus de griottes',
        'Sa note fruitée 100% naturelle',
        'Sa touche aigre-douce étonnamment fraîche',
        'Sa robe rouge et envoûtante',
      ],
      header: 'Bonnes raisons de l’aimer',
    },
    section_4: {
        span_color: "cerise_span_color",
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bouteille :</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'DURÉE DE CONSERVATION',
          excerpt: (
            <>
              <b>Bouteille :</b> 18 mois
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'TENEUR EN ALCOOL',
          excerpt: '4.0 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'service',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGRÉDIENTS',
          excerpt:
            'eau, malt d’orge, froment, avoine, houblon, levure, concentré de jus de cerises, arôme de cerises, coriandre, écorce d’orange, edulcorant : sucralose.',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
      Inscrivez vous pour être informé dès l’ouverture de notre e-shop
        </>
      ),
      link: 'ENVOYER',
      placeholder: "Votre E-mail",
      visit_page: 'Retour',
      download: 'Fiche produit'
    },
  },
  //////////// CITRON //////////////////////////
  citron: {
    section_1: {
      text: (
        <>
          La bière naturelle
          <br />
          au jus de citron
        </>
      ),
      excerpt: 'Brasserie Belge fondée en 1859',
    },
    section_2: {
      header: 'Citron',
      text: 'Une bière faiblement alcoolisée, au subtil goût de citron. Rafraîchissante et festive, son doux parfum acidulé a comme un goût d’encore !',
    },
    section_3: {
      ul: [
        'Sa fabrication 100% artisanale',
        'Son délicat jus de citron',
        'Sa note fruitée 100% naturelle',
        'Son goût non filtré',
        'Sa robe trouble et ensoleillée',
      ],
      header: 'Bonnes raisons de l’aimer',
    },
    section_4: {
        span_color: "citron_span_color",
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bouteille :</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'DURÉE DE CONSERVATION',
          excerpt: (
            <>
              <b>Bouteille :</b> 18 mois
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'TENEUR EN ALCOOL',
          excerpt: '2.3 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'service',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGRÉDIENTS',
          excerpt:
            'eau, malt d’orge, froment, avoine, houblon, levure, concentré de jus de citron, extrait de citron, extrait d’orange, concentré de jus d’orange, coriandre, écorce d’orange, edulcorant (sucralose).',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
      Inscrivez vous pour être informé dès l’ouverture de notre e-shop
        </>
      ),
      link: 'ENVOYER',
      placeholder: "Votre E-mail",
      visit_page: 'Retour',
      download: 'Fiche produit'
    },
  },
  ////////////////PECHE////////////////////
  peche: {
    section_1: {
      text: (
        <>
          La bière naturelle
          <br />
          au jus de pêche
        </>
      ),
      excerpt: 'Brasserie Belge fondée en 1859',
    },
    section_2: {
      header: 'Pêche',
      text: 'Sa robe vous fait penser à la chair d’une pêche bien mûre ? C’est normal, c’est le meilleur du fruit que l’on a ajouté ! Agréablement fraîche et fruitée, laissez-vous porter par la douceur de son parfum.',
    },
    section_3: {
      ul: [
        'Une bière 100% artisanale',
        'Son délicat jus de pêche',
        'Sa note fruitée et sucrée 100% naturelle',
        'Sa robe à la couleur jaune foncé de pêche',
        'Sa mousse jaune orangé',
      ],
      header: 'Bonnes raisons de l’aimer',
    },
    section_4: {
        span_color: "peche_span_color",
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bouteille :</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'DURÉE DE CONSERVATION',
          excerpt: (
            <>
              <b>Bouteille :</b> 18 mois
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'TENEUR EN ALCOOL',
          excerpt: '4.2 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'service',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGRÉDIENTS',
          excerpt:
            'eau, malt d’orge, froment, avoine, houblon, levure, concentré de jus de pêche, arôme de pêche, arôme de mangue, coriandre, écorce d’orange, edulcorant : sucralose',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
      Inscrivez vous pour être informé dès l’ouverture de notre e-shop
        </>
      ),
      link: 'ENVOYER',
      placeholder: "Votre E-mail",
      visit_page: 'Retour',
      download: 'Fiche produit'
    },
  },
  ////////// ROSE /////////////
  rose: {
    section_1: {
      text: (
        <>
          La bière naturelle
          <br />
          au jus de framboise
        </>
      ),
      excerpt: 'Brasserie Belge fondée en 1859',
    },
    section_2: {
      header: 'Rose',
      text: 'Envoûté par sa douce robe rose, découvrez l’alliance de la discrète amertume d’une bière blanche avec la note sucrée de la framboise pour une dégustation rafraîchissante et fruitée !',
    },
    section_3: {
      ul: [
        'Sa fabrication 100% artisanale',
        'Son délicat jus de framboise',
        'Sa note fruitée 100% naturelle',
        'Sa robe trouble et colorée',
        'Son côté bière blanche sucrée',
      ],
      header: 'Bonnes raisons de l’aimer',
    },
    section_4: {
        span_color: "rose_span_color",
      ul: [
        {
          icon: 'bottle-icon',
          title: 'FORMAT',
          excerpt: (
            <>
              <b>Bouteille :</b> 0,33L / 0,20L
            </>
          ),
        },
        {
          icon: 'watch-icon',
          title: 'DURÉE DE CONSERVATION',
          excerpt: (
            <>
              <b>Bouteille :</b> 18 mois
            </>
          ),
        },
        {
          icon: 'glass-icon',
          title: 'TENEUR EN ALCOOL',
          excerpt: '3.5 % VOL.',
        },
        {
          icon: 'termometar-icon',
          title: 'service',
          excerpt: '3-4 °C',
        },
        {
          icon: 'ingredients-icon',
          title: 'INGRÉDIENTS',
          excerpt:
            'eau, malt d’orge, froment, avoine, houblon, levure, concentré de jus de framboise, arôme de framboise, coriandre, écorce d’orange, edulcorant : sucralose.',
        },
      ],
    },
    section_5: {
      excerpt: (
        <>
          {' '}
      Inscrivez vous pour être informé dès l’ouverture de notre e-shop
        </>
      ),
      link: 'ENVOYER',
      placeholder: "Votre E-mail",
      visit_page: 'Retour',
      download: 'Fiche produit'
    },
  },
};

export default fr;
