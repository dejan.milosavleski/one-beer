export const Boire_Responsamble = [
  {
    title: ['Boire responsable'],
    text: [
      'La marque One Beer® a le souci du bien-être de ses consommateurs. Ce n’est pas parce que nous sommes commerçants de bière que nous ne nous devons pas de vous sensibiliser sur les méfaits de l’alcool. Nous sommes conscients du fait que si nous abandonnons nos consommateurs à eux-mêmes et qu’ils meurent, cela va entacher la confiance absolue que nous voulons installer entre vous et nous. C’est pourquoi nous tenons ferme à une politique de consommation responsable de la bière. Il est important de savoir que l’alcool est très dangereux pour la santé. Il est responsable de la mort de plusieurs personnes dans le monde aujourd’hui. C’est pourquoi nous vous invitons à consommer avec modération. Même si nous vous proposons des bières avec un faible taux d’alcool, cela ne veut pas dire que vous devez vous plonger sans toutefois réfléchir. '

    ],
  },
  {
    title: [
     'One Beer® opte pour une consommation modérée',
    ],
    text: [
      `Plusieurs recherches sont en accord sur le fait que les personnes qui consomment l’alcool avec modération vivent beaucoup plus longtemps. Selon des résultats d’une étude menée par le Docteur Williams de l’université du Pays de Galles, la bière permet une lubrification de la circulation sanguine. Ce qui vous aide à éviter la formation des caillots de sang dans l’organisme. `,
      `C’est pourquoi il faut en boire de temps en temps avec modération. D’un autre côté, la bière est la boisson alcoolisée la plus nutritive sur le plan de la valeur alimentaire. Lorsque vous en consommez régulièrement, vous évitez certaines maladies et vous donnez à votre système immunitaire de pouvoir se défendre. Notre objectif n’est donc pas de vous déconseiller d’en consommer parfois.`,
    ],
  },
  {
    title: [
      'One Beer® sensibilise sur l’abus d’alcool'
    ],
    text: [
      `Certes, nous vendons la bière non raffinée, mais cela ne veut pas dire qu’une consommation à l’excès vous met à l’abri des problèmes de santé. Des milliers de morts à cause de l’abus d’alcool sont enregistrés par an à travers le monde entier. C’est pourquoi il est essentiel de limiter votre consommation.`,
      `Si nous nous sentons obligés de le signaler, ceci participe à la loyauté que nous souhaitons avoir envers vous. Comme nous vous l’avons dit, nous tenons à la bonne santé de tous nos consommateurs. C’est pourquoi nous ne pouvons cacher une vérité pareille. Vous devez le savoir pour parvenir à boire de manière responsable. `,
      `Dit de cette manière, nous sommes certains de ne pas être à l’origine des problèmes de santé que vous allez avoir par la suite. Certes, nos bières sont saines et sans danger, mais nous vous invitons à une consommation modérée.`,
    ],
  },
  {
    title: ['Un excès d’alcool peut créer des problèmes irréversibles'],
    text: [
      `L’alcool est bénéfique pour le corps, mais un abus peut créer des problèmes incroyables. Si nous prenons par exemple le cas des jeunes et des personnes âgées. Il est important de savoir que l’alcool peut avoir des effets psychoactifs sur le cerveau. En plus de cela, il peut aussi conduire à des pertes de mémoire. `,

    ],
  }
];
