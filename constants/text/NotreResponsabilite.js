export const NotreResponsabilite = [
  {
    title: ['Mentions légales'],
    text: [
  
    ],
  },
    {
    title: ['Article 1 : Identification'],
    text: [
    `Le Site institutionnel ONE BEER (le « Site ») accessible via l’adresse www.onebeer.fr appartient à RDJ GROUP, Société par Actions Simplifiée au capital de 3 502 000 Euros, dont le siège social est 36 rue Anatole France 92300 Levallois-Perret immatriculée au Registre du Commerce et des sociétés de Nanterre sous le numéro 888 296 274 (ci-après «la Société »).`,
`Le numéro de TVA intracommunautaire de la Société est le suivant: FR74888296274`,
`La Société est joignable a l’adresse suivante : contact@onebeer.fr`
    ],
  },
  {
    title: ['Article 2. Conditions d’utilisation du Site'],
    text: [
    `Les présentes conditions d’utilisation et la charte de protection des données ont pour objet d’assurer l’utilisation du Site, dans le respect des droits de toute personne, incluant le respect de la vie privée et de la propriété intellectuelle.`,
`Le Site est un service de communication et de boutique en ligne pour donner accès aux internautes majeurs à un espace d’informations et d’achat relatif aux activités et a la marque ONEBEER.`,
`A ce titre, et considérant que le Site est soumis notamment aux dispositions de la Loi Evin du 10 janvier 1991, son accès est exclusivement destiné aux seules personnes majeures.`,
`L’accès au Site et son utilisation sont soumis aux conditions stipulées ci-après. Par l’accession au Site et sa navigation, l’Internaute accepte sans réserve les présentes conditions d’utilisation et s’engage à les respecter.`,
`Si l’internaute n’accepte pas ces conditions, il est prié de ne pas accéder au Site et de ne pas l’utiliser.`,
`Les présentes mentions légales peuvent être modifiées à tout moment. Toute modification sera publiée sur le Site et tout utilisateur accédant au Site après leur publication sera présumé avoir accepté ces modifications.`

    ],
  },
  {
    title: ['Article 3. Propriété Intellectuelle'],
    text: [
    `Le contenu du Site y compris, mais sans limitation, tout logo, marque, photo, illustration, image, texte sur le Site, est protégé en vertu des droits de propriété intellectuelle, ou de toute autre disposition légale applicable en l’espèce.`,
`La Société est titulaire de tous les droits de propriété relatifs au contenu du Site, à l’exclusion de contenus expressément attribués à d’autres.`,
`La reproduction, la distribution, l’adaptation de ses logos, marques et autres contenus sur le Site est interdite sans autorisation préalable de la Société titulaire des droits, que ce soit à des fins commerciales ou non. La Société se réserve la possibilité de poursuivre toute personne contrevenant à ces dispositions.`,
`La Société respecte les droits de propriété intellectuelle de tiers, et toutes les dispositions légales en vigueur pour la protection des droits et intérêts des tiers et prend l’ensemble des mesures à hauteur de ses moyens en conséquence.`,

    ],
  },
  {
    title: ['Article 4. Hyperliens'],
    text: [
    `Le Site peut contenir des liens vers des sites web tiers.`,
`Le fait de cliquer sur l’un de ces liens dirigera automatiquement l’utilisateur du Site vers ces sites qui sont soumis à des conditions d’utilisation qui leur sont propres, et non aux présentes.`,
`Il appartient à chaque internaute de prendre connaissance des engagements de chaque site web vers lequel il a été redirigé avant de procéder à sa navigation, afin de vous assurer de n’utiliser que des sites dont le contenu et les règles d’utilisation ont été acceptées.`,
`La Société n’a pas la maîtrise de ces autres sites tiers, et ne peut en aucun cas en surveiller le contenu ou les conditions d’utilisation, notamment en matière de respect de la vie privée. La Société ne pourra en aucun cas être tenu responsable pour tout dommage subi sur un site vers lequel un de ses internautes aurait été redirigé.`,
`Tout lien vers le Site présent depuis un site Tiers est prohibé en l’absence d’autorisation préalable de La Société qui se réserve le droit de poursuivre tout contrevenant. La Société décline toute responsabilité concernant tout lien présent sur un site externe et renvoyant vers le Site. En outre, La Société ne pourra en aucun cas être présumé associé à un contenu présent au sein de ce site tiers par la simple existence de cet hyperlien.`,

    ],
  },
  {
    title: ['Article 5. Responsabilité – Absence de garanties'],
    text: [
    `Le Site est mis à jour régulièrement, afin de permettre l’accès et l’utilisation du Site dans les meilleures conditions par tout utilisateur majeur.`,
`Cependant l’utilisateur a dans ce cadre pleinement conscience que les contenus présents sur le Site sont donnés à titre indicatif.`,
`La Société ne pourra être tenu responsable de toute conséquence directe et indirecte de la reproduction et l’utilisation du contenu du Site par l’Internaute, sans préjudice de l’application des dispositions relatives à la propriété intellectuelle.`,
`En outre, la Société se réserve le droit de porter modification sur le Site à tout moment, et ne peut donc garantir l’accessibilité du Site à tout moment, sans interruption, en temps utile, en toute sécurité et sans erreur, ni l’absence d’erreurs et bogues.`,
`La Société ne peut être tenu responsable de tout désagrément et/ou dommage causé à un internaute par les actions de tiers qui, par un moyen technique déloyal, a utilisé les informations diffusées sur le Site. La Société assure toutefois mettre tous les moyens à sa disposition pour lutter contre toute action de ce type, étant limitée par les contraintes techniques inhérentes à Internet.`,
`La Société ne peut être tenu responsable d’un quelconque dommage subi du fait notamment de la qualité du réseau Internet et/ou des configurations techniques.`,
`Les présentes conditions d’utilisation sont soumises à la loi Française, et tous litiges concernant notamment leur application et leur interprétation seront présentés devant le tribunal de commerce de Nanterre.`,

    ],
  },
    {
    title: ['Article 6. Politique de Confidentialité – Protection des Données personnelles'],
    text: [
`La Société a mis en place une Politique de confidentialité relative au respect de la vie privée des internautes accédant au Site accessible ici : onebeer.fr/poliquedeconfidentialite`,
    ],
  },
];













