export const notre_misions_fr = [
  {
    title: ['À propos de nous'],
    text: ['Le secteur de l’agro-alimentaire fait partie de l’ADN de notre famille depuis plusieurs générations. Nous avons acquis au fil des années un savoir-faire dans les boissons alcoolisées et désaltérantes non alcoolisées, qui accompagnent le quotidien de chacun. Les vins de Bourgogne, du fait des racines de notre famille, sont une vraie passion. Mais la bière est devenue progressivement notre spécificité. La bière, ou plutôt les bières, puisque notre soif de découverte nous a menés vers toutes les bières du monde ! Ce secteur a connu dernièrement une évolution constante vers la qualité, le respect de l’environnement et la recherche du plaisir gustatif : autant de valeurs qui nous sont chères.']
  },
  {
    title: [''],
    text: [
      'D’importateurs distributeurs, nous avons naturellement basculé vers la production, mus par la volonté de créer une bière qui nous ressemble. Désaltérante et surtout d’une qualité de plus en plus élevée, d’une part grâce à sa technique de brassage, mais surtout à la noblesse du houblon et des fruits qui la composent. Une bière haut de gamme et toute en légèreté, avec un taux d’alcool modéré lui permettant d’être consommée à tout moment de la journée. Un produit noble, tout en restant convivial. Une bière dans l’air du temps. Fabriquée dans le respect de l’environnement, réalisée à partir d’un houblon haut de gamme parce qu’on ne doit pas choisir entre la finesse du goût et l’avenir de la planète ! Une bière peu alcoolisée parce que sa dégustation doit s’intégrer dans un mode de vie sain. Et enfin une bière accessible, parce que le plaisir ne doit pas être réservé à une élite !'
      ],
  },
  {
    title: [''],
    text: ['C’est ainsi qu’est née ONE BEER. Une bière haut de gamme, qui ne fait pas tourner la tête mais qui fait voyager ! Une bière qui se décline aussi en plusieurs versions aromatisées, à base de fruits naturels, non filtrée. Sa bouteille bleue deviendra vite synonyme de convivialité… tout en préservant la planète puisqu’elle est totalement respectueuse de l’environnement !'],
  },
  {
    title: [''],
    text: ['ONE BEER, un nom unique pour une gamme entière. Une bière blonde et une bière blanche, cette dernière existant en plusieurs versions fruitées. Elle séduira les amateurs de bières attachés à la tradition et saura conquérir de nouveaux adeptes. ONE BEER, une bière pour chaque occasion avec un dénominateur commun : le plaisir, sans la culpabilité !'],
  },

];
export const notre_misions_gb = [
  {
    title: ['About Us'],
    text: [`The food industry has been part of our family's DNA for generations. Over the years, we have acquired expertise in alcoholic and non-alcoholic thirst-quenching drinks, which accompany everyone's daily life. Burgundy wines, because of the roots of our family, are a real passion. But beer has gradually become our specificity. Beer, or rather beers, since our thirst for discovery has led us to all beers in the world! This sector has recently experienced a constant evolution towards quality, respect for the environment and the search for taste pleasure: all values that are dear to us.`]
  },
  {
    title: [''],
    text: [
      `From importer distributors, we naturally switched to production, driven by the desire to create a beer that resembles us. Thirst-quenching and above all of an increasingly high quality, on the one hand thanks to its brewing technique, but above all to the nobility of the hops and the fruits that compose it. A premium, light beer with a moderate alcohol content that allows it to be consumed at any time of the day. A noble product, while remaining user-friendly. A beer in tune with the times. Made with respect for the environment, made from top-of-the-range hops because you don't have to choose between fine taste and the future of the planet! A low-alcohol beer because its tasting must be part of a healthy lifestyle. And finally an accessible beer, because the pleasure should not be reserved for an elite!`
      ],
  },
  {
    title: [''],
    text: [`This is how ONE BEER was born. A top-of-the-range beer, which does not turn your head but which makes you travel! A beer that is also available in several flavored versions, made from natural fruits, unfiltered. Its blue bottle will quickly become synonymous with conviviality ... while preserving the planet since it is totally respectful of the environment !`],
  },
  {
    title: [''],
    text: [`ONE BEER, a unique name for an entire range. A lager and a white beer, the latter available in several fruity versions. It will appeal to beer lovers attached to tradition and will win over new followers. ONE BEER, a beer for every occasion with a common denominator: pleasure, without the guilt!`],
  },

];
