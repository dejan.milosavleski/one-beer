export const politique_de_confidentialité_TEXT = [
    {
      title: [
        'Politique de confidentialité',
      ],
      text: [
`ONE BEER une marque de RDJ GROUP Société par Actions Simplifiée au capital de 3 502 000 euros dont le siège social est situé 36 rue Anatole France 92300 Levallois-Perret / immatriculée au RCS de Nanterre sous le N°888 296 274 édite le site Internet www.onebeer.fr (le « Site »).`,
`ONE BEER est attaché au respect de la vie privée des Internautes accédant au Site. La présente Politique de confidentialité a pour objet de permettre à Internaute de prendre connaissance des modalités de collectes et de traitements des données à caractère personnel le concernant tout au long de sa navigation sur le Site et de ses droits en matière de respect de sa vie privée et de protection de ses données à caractère personnel.`,
`Dans le cadre des traitements de données à caractère mise en œuvre sur le Site, ONE BEER se conforme à la Loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés (loi « Informatique et Libertés ») et au Règlement Général sur la Protection des 2016/679 du 27 avril 2016 (« RGPD ») (ensemble la « Règlementation sur la protection des données »)`,
`Dans le cadre de sa politique de respect de la vie privée des personnes, ONE BEER a désigné un Délégué à la Protection des Données que vous pouvez contacter pour toute demande relative à la protection des données à caractère personnel à l’adresse suivante : info@onebeer.fr`,
      ],
    },
    {
      title: ['ARTICLE 1. POUR QUELLES RAISONS POUVONS-NOUS TRAITER VOS',
      'DONNÉES DANS LE CADRE DE VOTRE NAVIGATION SUR LE SITE ?'],
      text: [
      `Au cours de votre navigation sur le Site nous sommes susceptibles de collecter des données vous concernant. Aucune des données ne sera collectée à votre insu. Nous ne collectons vos données que pour les finalités déterminées, explicites et légitimes suivantes :`,
      `- Répondre à vos demandes d’informations de toute nature,`,
      `- Administrer, évaluer et gérer votre dossier de candidature en réponse à nos offres d’emploi qui sont proposées sur notre site ;`,
      `- Améliorer et analyser nos services, mieux comprendre vos besoins et vos intérêts, et personnaliser le cas échéant nos contenus et nos offres.`,
      `- Réaliser la maintenance et optimisation du site en vue de vérifier/améliorer la qualité de service, la disponibilité et la performance du service, solutionner les éventuels problèmes ou anomalies de fonctionnement, et sécuriser le site contre la fraude.`,
      ],
    },
    {
      title: ['ARTICLE 2. AVEC QUI PARTAGEONS-NOUS VOS DONNÉES ?'],
      text: [
        `Nous sommes amenés à partager les données vous concernant avec des tiers sous- traitants pour nous aider à exécuter et fournir les services mentionnés ci-dessus (tels que notamment hébergeur, éditeur). Nous nous assurons que nos sous-traitants présentes les garanties suffisantes quant aux mesures techniques et organisationnelles mises en œuvre de manière à ce que l’usage des données soit effectué conformément à la Règlementation sur la protection des données.`,
      ],
    },

     {
      title: ['ARTICLE 3. OÙ SONT LOCALISÉES VOS DONNÉES ?'],
      text: [
        `Vos données collectées et traitées dans le cadre des services fournis sur le Site et conformément à la présente Politique. Nous conservons vos données pendant une durée n’excédant pas celle nécessaire à la réalisation des finalités décrites précédemment, dans la mesure nécessairement raisonnable afin de respecter une exigence légale en vigueur, ou pendant toute période utile au regard d’un délai de prescription applicable ou à l’établissement de la preuve d’un droit.`
      ],
    },

     {
      title: ['ARTICLE 4. DURÉE DE CONSERVATION'],
      text: [
        `Nous sommes amenés à partager les données vous concernant avec des tiers sous- traitants pour nous aider à exécuter et fournir les services mentionnés ci-dessus (tels que notamment hébergeur, éditeur). Nous nous assurons que nos sous-traitants présentes les garanties suffisantes quant aux mesures techniques et organisationnelles mises en œuvre de manière à ce que l’usage des données soit effectué conformément à la Règlementation sur la protection des données.`,
      ],
    },

         {
      title: ['ARTICLE 5. COMMENT SÉCURISONS-VOUS VOS DONNÉES ?'],
      text: [
        `ONE BEER s’engage à prendre toutes les mesures techniques et organisationnelles nécessaires afin de garantir un niveau de sécurité, d’intégrité et de confidentialité adapté au risque compte tenu notamment de l’état des connaissances, des coûts de mise en œuvre et de la nature, de la portée, du contexte et des finalités du traitement pour protéger vos données contre le vol, la destruction, l’altération, la perte accidentelle, la diffusion ou l’accès non autorisé.`],
    },

         {
      title: ['ARTICLE 6. UTILISATION DE COOKIES'],
      text: [
       `Nous utilisons des cookies ou des technologies similaires pour collecter des données sur notre Site afin de nous fournir des renseignements et nous souvenir de vous quand vous vous connectez sur notre Site, mieux comprendre vos intérêts, faciliter l’accès à nos services, assurer l’exécution de nos services, les analyser et nous permettre de proposer des publicités et des offres personnalisées.`,
        `Pour en savoir plus, prenez connaissance de notre politique en matière de cookie accessible sur ce Site à l’adresse suivante : info@onebeer.fr`
       ],
    },

         {
      title: ['ARTICLE 7. VOS DROITS'],
      text: [
      `Vous disposez de droits sur vos données à caractère personnel. Conformément à la réglementation en matière de protection des données à caractère personnel et après avoir justifié de votre identité, vous bénéficiez, dans les conditions prévues aux articles 15 à 22 du RGPD :`,

      `• d’un droit d’accès : l’internaute peut obtenir communication des données le concernant;`,
      `• d’un droit de rectification : en cas d’inexactitude de ces informations, l’internaute peut exiger qu’elles soient rectifiées ou complétées;`,
      `• d’un droit d’effacement : l’internaute peut exiger que ses données soient effacées, équivalent pour tout inscrit à un espace privé, à la fermeture de son compte.`,
      `• d’un droit d’opposition : l’internaute peut s’opposer au traitement de ses données à des fins de prospection commerciale ;`,
      `• d’un droit de limitation : l’internaute peut obtenir la limitation du traitement de ses données ;`,
      `• d’un droit de portabilité : l’internaute peut demander de recevoir ses données personnelles qu’il a fournies dans un format structuré, couramment utilisé et lisible par machine, dans certaines circonstances, ou demander qu’elles soient transmises à un autre responsable de traitement si cela est techniquement possible`,

      `Vous pouvez contactez nos Services afin d’exercer vos droits à l’adresse électronique suivante : info@onebeer.fr en joignant à votre demande une copie d’un titre d’identité.`,

      `Notez que vous pouvez également rectifier ou supprimer des données directement sur votre compte client sous réserves que nous ayons besoin de conserver certaines données pour satisfaire à des exigences légales et/ou pour la gestion de la relation commerciale, de vos commandes ou de vos factures.`,

      `Si vous avez donné votre consentement pour le traitement de vos données personnelles, vous pouvez retirer votre consentement à tout moment, sans que cela ne porte atteinte à la licéité du traitement avant le retrait.`

      ],
    },

         {
      title: ['ARTICLE 8. LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTÉS'],
      text: [
      
`ONE BEER vous rappelle que vous pouvez contacter la CNIL directement sur le site internet https://www.cnil.fr/fr/agir ou par courrier à l’adresse suivante : Commission`,

`Nationale de l’Informatique et des Libertés, 3 Place de Fontenoy – TSA 80715, 75334 PARIS CEDEX 07`
      ],
    },

  ];
  





