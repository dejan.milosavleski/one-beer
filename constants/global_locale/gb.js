const gb = {
  lang: 'gb',
  menu: {
    menu_titles: [
      {
        title: 'our beers',
        slug: 'our-beers',
      },
      {
        title: 'our story',
        slug: 'our-story',
      },
      {
        title: 'CONTACT',
        slug: 'contact',
      },
      {
        title: 'shop',
        slug: 'shop',
      },
    ],
  },
  side_menu: {
    our_beers: 'our beers',
    our_story: 'our story',
    contact: 'CONTACT',
    beers: [
      {
        title: 'blonde',
        slug: 'blonde',
      },
      {
        title: 'blanche',
        slug: 'blanche',
      },
      {
        title: 'cerise',
        slug: 'cerise',
      },
      {
        title: 'citron',
        slug: 'citron',
      },
      {
        title: 'PêCHE',
        slug: 'peche',
      },
      {
        title: 'ROSé',
        slug: 'rose',
      },
    ],
    ul_1: [
      {
        slug: 'shop',
        title: 'Buy now',
      },
      {
        slug: 'politique',
        title: 'Privacy policy',
      },
      {
        slug: 'termes-et-conditions',
        title: 'Legal Notice',
      },
    ],
  },
  auth_18: {
    title_1: 'To enter the site you must be of legal age.',
    title_2: `What's your birthday ?`,
    datepicker_placeholder: 'select your birthday',
    form_fields: {
      day: 'DD',
      month: 'MM',
      year: 'YYYY',
    },
    validation: {
      required: 'Required',
      number: 'Must be a number',
      email: 'Invalid email address',
      too_young: `You do not meet the minimum age requirement!`,
      correct_date_num: 'enter the correct date',
      correct_month_num: 'enter the correct month',
      correct_year_num: `enter the correct year`,
    },
    modal_message: `You do not meet the minimum age requirement!`,
    button: 'validate',
  },

  home_page: {
    button: 'Find out more about them here',
    beers: [
      {
        title_1: 'One beer',
        title_2: '6 flavors',
        title_3: 'An endless story',
        small_text:
          'All dressed in blue, she takes care of the planet with her 100% recyclable bottle.<br/>Discover a 100% natural beer, light but full of character. <br/>Low in alcohol content, light-bodied, elegant with a subtle mix of sweet and sour.<br/>Unfiltered, refreshing and thirst quenching, it comes in 6 flavours for you to indulge.',
      },
      {
        title_1: 'One beer',
        title_2: '6 flavors',
        title_3: 'An endless story',
        small_text:
          'All dressed in blue, she takes care of the planet with her 100% recyclable bottle.<br/>Discover a 100% natural beer, light but full of character. <br/>Low in alcohol content, light-bodied, elegant with a subtle mix of sweet and sour.<br/>Unfiltered, refreshing and thirst quenching, it comes in 6 flavours for you to indulge.',
      },
      {
        title_1: 'One beer',
        title_2: '6 flavors',
        title_3: 'An endless story',
        small_text:
          'All dressed in blue, she takes care of the planet with her 100% recyclable bottle.<br/>Discover a 100% natural beer, light but full of character. <br/>Low in alcohol content, light-bodied, elegant with a subtle mix of sweet and sour.<br/>Unfiltered, refreshing and thirst quenching, it comes in 6 flavours for you to indulge.',
      },
      {
        title_1: 'One beer',
        title_2: '6 flavors',
        title_3: 'An endless story',
        small_text:
          'All dressed in blue, she takes care of the planet with her 100% recyclable bottle.<br/>Discover a 100% natural beer, light but full of character. <br/>Low in alcohol content, light-bodied, elegant with a subtle mix of sweet and sour.<br/>Unfiltered, refreshing and thirst quenching, it comes in 6 flavours for you to indulge.',
      },
      {
        title_1: 'One beer',
        title_2: '6 flavors',
        title_3: 'An endless story',
        small_text:
          'All dressed in blue, she takes care of the planet with her 100% recyclable bottle.<br/>Discover a 100% natural beer, light but full of character. <br/>Low in alcohol content, light-bodied, elegant with a subtle mix of sweet and sour.<br/>Unfiltered, refreshing and thirst quenching, it comes in 6 flavours for you to indulge.',
      },
      {
        title_1: 'One beer',
        title_2: '6 flavors',
        title_3: 'An endless story',
        small_text:
          'All dressed in blue, she takes care of the planet with her 100% recyclable bottle.<br/>Discover a 100% natural beer, light but full of character. <br/>Low in alcohol content, light-bodied, elegant with a subtle mix of sweet and sour.<br/>Unfiltered, refreshing and thirst quenching, it comes in 6 flavours for you to indulge.',
      },
    ],
  },
  our_beers: {
    main_title: 'LIVING A LIFE OUT OF THE ORDINARY ?',
    second_title: `That's great, because our beer is anything but !`,
  },
  our_story: {
    main_title: 'About Us',
    content: [
      {
        title: [''],
        text: [
          `The food industry has been part of our family's DNA for several generations. Over the years,
          we have acquired particular expertise in alcoholic and non-alcoholic thirst-quenching
          beverages that are part of everyone's daily life. Our family roots mean that we are
          passionate about Burgundy wines, but beer has gradually become our speciality. Beer, or
          rather beers, since our thirst for discovery has led us all around the world in search of new
          beers! In recent times, the beer industry has started to focus more and more on quality,
          good environmental practices and the sheer tasting pleasure of a good beer: all values that
          are very close to our heart.`,
        ],
      },
      {
        title: [''],
        text: [
          `From being importers and distributors, we naturally switched to production, driven by the
          desire to create a beer that reflects our personality.The one beer brewery was created in 1856, 6 generations succeeded to work and improve the beers brewed on site,
          always with original copper cauldrons, a guarantee of quality and history. We like it thirst-quenching and above all
          of increasingly high quality, thanks to its brewing technique on the one hand, but above all
          thanks to the nobility of the finest hops and fruits that are at the core of the beer. A top-of-
          the-range and light beer with a moderate alcohol content that can be enjoyed at any time of
          the day. A noble product, but still genial, a beer in tune with the times. Made with the
          utmost respect for the environment, using top quality hops, because you should not have to
          choose between a fine taste and the future of the planet! A low-alcohol beer because it
          should fit in with a healthy lifestyle. And finally, a beer that is accessible, because pleasure
          should not be exclusive!`,
        ],
      },
      {
        title: [''],
        text: [
          `This is how ONE BEER was born. A top-of-the-range beer that might not turn heads, but that
          takes you on a journey! A beer that also exists in several different flavours, based on natural
          fruit and unfiltered. Its blue bottle will quickly become synonymous with conviviality... while
          at the same time preserving the planet, as it is totally environmentally friendly!`,
        ],
      },
      {
        title: [''],
        text: [
          `ONE BEER, a unique name for a whole range. A lager and a white beer, the latter available in
          several different fruit flavours. It will appeal to beer lovers who are value tradition but is also
          certain to make some new fans. ONE BEER, a beer for every occasion with a common
          denominator: pleasure, without guilt!`,
        ],
      },
    ],
  },
  contact: {
    main_title: 'Contact us',
    excerpt: 'ONE BEER is a trademark of RDJ Group registered in Europe and other countries.',
    distributers:[
      {
        text_1: 'Official Distributor',
        text_2: 'International Business Service',
        email: 'www.ibs-wholesale.com',
        color: '#EBD864'
      },
      {
        text_1: 'Distributor Belgium',
        text_2: 'SRX Event Company',
        email: ' xavier@srxlux.hk',
        color: '#CA6182'
      },
      {
        text_1: 'Distributor Cameroon / Gabon / Ivory Coast:',
        text_2: 'Company Empiretrad-Group',
        email: ' empiretradgroup.sarl@gmail.com',
        color: '#ECA03A'
      },
      {
        text_1: 'Distributor HONG KONG / CHINA',
        text_2:  <>
        Sun Po Wing Trading LTD ,<br />
        Unit C3, 18 Floor, TML Tower,
        <br />
        No.3 Hoi Shing RD, Tsuen Wan,
        <br />
        N.T. Hong Kong{' '}
      </>,
        email: 'yvonnelam@sunpowing.hk',
        color: '#EEBF52'
      }
    ],

    form: {
      name: 'Last name and first name',
      email: 'E-mail',
      message: 'Message',
      button: 'Send',
    },
  },
  shop: {},
  cookie_agreement: {
    text: 'By clicking "Accept All Cookies", you agree to the storing of cookies on oyur device to enhance site navigation,<br/> analyze site usage, and assist in our marketing efforts.',
    button_text: `Accept All Cookies`,
  },
  politique_de_confidentialité: {
    main_title: 'Privacy Policy',
    content: [
      {
        title: [''],
        text: [
          `ONE BEER a trademark of RDJ GROUP Simplified Joint Stock Company with a capital of 3,502,000 euros whose registered office is located at 36 rue Anatole France 92300 Levallois-Perret / registered with the RCS of Nanterre under number 888 296 274 publishes the website www .onebeer.fr (the "Site").`,
          `ONE BEER is committed to respecting the privacy of Internet users accessing the Site. The purpose of this Privacy Policy is to enable Internet users to be aware of the methods of collecting and processing personal data concerning them throughout their browsing on the Site and of their rights in terms of respect for their privacy. and protection of his personal data.`,
          `Within the framework of the processing of data of a nature implemented on the Site, ONE BEER complies with Law n ° 78-17 of January 6, 1978 relating to data processing, files and freedoms ("Data Protection Act" ) and the General Protection Regulation 2016/679 of April 27, 2016 ("RGPD") (together the "Data Protection Regulation")`,
          <>
            As part of its policy of respect for the privacy of individuals, ONE
            BEER has appointed a Data Protection Officer who you can contact for
            any request relating to the protection of personal data at the
            following address:{' '}
            <a href="mailto:info@onebeer.fr">info@onebeer.fr</a>
          </>,
        ],
      },
      {
        title: [
          'ARTICLE 1. FOR WHAT REASONS CAN WE TREAT YOUR',
          'DATA AS PART OF YOUR NAVIGATION ON THE SITE?',
        ],
        text: [
          `During your browsing on the Site, we may collect data about you. None of the data will be collected without your knowledge. We only collect your data for the following specific, explicit and legitimate purposes:`,
          `- Respond to your requests for information of any kind,`,
          `- Administer, evaluate and manage your application file in response to our job offers that are offered on our site`,
          `- Improve and analyze our services, better understand your needs and interests, and customize our content and our offers, if necessary.`,
          `- Carry out the maintenance and optimization of the site in order to check / improve the quality of service, the availability and the performance of the service, solve any problems or operating anomalies, and secure the site against fraud.`,
        ],
      },
      {
        title: ['ARTICLE 2. WITH WHOM DO WE SHARE YOUR DATA?'],
        text: [
          `We share your data with third-party subcontractors to help us perform and provide the services mentioned above (such as, in particular, host, publisher). We ensure that our subcontractors present sufficient guarantees as to the technical and organizational measures implemented so that the use of data is carried out in accordance with the Data Protection Regulations.`,
        ],
      },

      {
        title: ['ARTICLE 3. WHERE ARE YOUR DATA LOCATED?'],
        text: [
          `Your data collected and processed as part of the services provided on the Site and in accordance with this Policy. We keep your data for a period not exceeding that necessary for the achievement of the purposes described above, to the extent necessarily reasonable in order to comply with a legal requirement in force, or for any useful period with regard to an applicable limitation period or establishing proof of a right.`,
        ],
      },

      {
        title: ['ARTICLE 4. DURATION OF CONSERVATION'],
        text: [
          `We share your data with third-party subcontractors to help us perform and provide the services mentioned above (such as, in particular, host, publisher). We ensure that our subcontractors present sufficient guarantees as to the technical and organizational measures implemented so that the use of data is carried out in accordance with the Data Protection Regulations.`,
        ],
      },

      {
        title: ['ARTICLE 5. HOW DO YOU SECURE YOUR DATA?'],
        text: [
          `ONE BEER undertakes to take all the technical and organizational measures necessary to guarantee a level of security, integrity and confidentiality adapted to the risk, taking into account in particular the state of knowledge, the costs of implementation and the nature, scope, context and purposes of processing to protect your data against theft, destruction, alteration, accidental loss, dissemination or unauthorized access.`,
        ],
      },

      {
        title: ['ARTICLE 6. USE OF COOKIES'],
        text: [
          `We use cookies or similar technologies to collect data on our Site in order to provide us with information and remember you when you log into our Site, to better understand your interests, to facilitate access to our services, to ensure the performance of our services, analyze them and enable us to provide personalized advertisements and offers.`,
          <>
            For more information, read our cookie policy available on this Site
            at the following address:{' '}
            <a href="mailto:info@onebeer.fr">info@onebeer.fr</a>
          </>,
        ],
      },

      {
        title: ['ARTICLE 7. YOUR RIGHTS'],
        text: [
          `You have rights over your personal data. In accordance with the regulations on the protection of personal data and after proving your identity, you benefit, under the conditions provided for in Articles 15 to 22 of the GDPR:`,

          `• a right of access: the Internet user can obtain communication of data concerning him;`,
          `• a right of rectification: in the event of this information being inaccurate, the internet user may demand that it be rectified or supplemented;`,
          `• a right of erasure: the user can demand that his data be erased, equivalent for any registered in a private area, to the closure of his account.`,
          `• a right of opposition: the user can object to the processing of his data for the purposes of commercial prospecting;`,
          `• a right of limitation: the user can obtain the limitation of the processing of his data;`,
          `• a right of portability: the Internet user can request to receive his personal data that he has provided in a structured, commonly used and machine-readable format, in certain circumstances, or request that they be transmitted to another person in charge of treatment if technically possible`,

          <>
            You can contact our Services to exercise your rights at the
            following email address:{' '}
            <a href="mailto:info@onebeer.fr">info@onebeer.fr</a> by attaching a
            copy of an identity document to your request.
          </>,

          `Note that you can also rectify or delete data directly on your customer account provided that we need to keep certain data to meet legal requirements and / or for the management of the commercial relationship, your orders or your invoices.`,

          `If you have given your consent for the processing of your personal data, you can withdraw your consent at any time, without this affecting the lawfulness of the processing before the withdrawal.`,
        ],
      },

      {
        title: ['ARTICLE 8. THE NATIONAL COMPUTER AND FREEDOM COMMISSION'],
        text: [
          `ONE BEER reminds you that you can contact the CNIL directly on the website https://www.cnil.fr/fr/agir or by mail to the following address: Commission`,

          `National Information Technology and Liberties, 3 Place de Fontenoy - TSA 80715, 75334 PARIS CEDEX 07`,
        ],
      },
    ],
  },
  mentions_légales: {
    main_title: 'Legal Notice',
    content: [
      {
        title: ['Article 1: Identification'],
        text: [
          `The ONE BEER institutional Site (the "Site") accessible via the address www.onebeer.fr belongs to RDJ GROUP, a Simplified Joint Stock Company with a capital of 3,502,000 Euros, whose registered office is 36 rue Anatole France 92300 Levallois- Perret registered in the Nanterre Trade and Companies Register under number 888 296 274 (hereinafter “the Company”).`,
          `The Company's intra-community VAT number is as follows: FR74888296274`,
          `The Company can be contacted at the following address: contact@onebeer.fr`,
        ],
      },
      {
        title: ['Article 2. Conditions of use of the Site'],
        text: [
          `The purpose of these conditions of use and the data protection charter is to ensure the use of the Site, with respect for the rights of all persons, including respect for privacy and intellectual property.`,
          `The Site is a communication and online store service to give major Internet users access to an information and purchasing area relating to the activities and the ONEBEER brand .`,
          `As such, and considering that the Site is subject in particular to the provisions of the Evin Law of January 10, 1991, its access is exclusively intended for adults only.`,
          `Access to the Site and its use are subject to the conditions set out below. By accessing the Site and browsing, the Internet user unreservedly accepts these conditions of use and undertakes to comply with them.`,
          `If the user does not accept these conditions, he is asked not to access the Site and not to use it.`,
          `These legal notices can be modified at any time. Any modification will be published on the Site and any user accessing the Site after their publication will be presumed to have accepted these modifications.`,
        ],
      },
      {
        title: ['Article 3. Intellectual property'],
        text: [
          `The content of the Site including, but not limited to, any logo, brand, photo, illustration, image, text on the Site, is protected under intellectual property rights, or any other legal provision applicable in this case. `,
          `The Company is the holder of all property rights relating to the content of the Site, to the exclusion of content expressly attributed to others .`,
          `The reproduction, distribution, adaptation of its logos, brands and other content on the Site is prohibited without the prior authorization of the Company holding the rights, whether for commercial purposes or not. The Company reserves the right to prosecute any person who contravenes these provisions.`,
          `The Company respects the intellectual property rights of third parties, and all legal provisions in force for the protection of the rights and interests of third parties and takes all measures within its means accordingly.`,
        ],
      },
      {
        title: ['Article 4. Hyperlinks'],
        text: [
          `The Site may contain links to third party websites.`,
          ` Clicking on one of these links will automatically direct the user of the Site to those sites which are subject to their own terms of use, and not hereof.`,
          `It is up to each Internet user to take note of the commitments of each website to which he has been redirected before proceeding to his navigation, in order to ensure that you only use sites whose content and rules of use have been accepted.`,
          `The Company has no control over these other third-party sites, and can under no circumstances monitor their content or the conditions of use, particularly with regard to respect for privacy. The Company cannot under any circumstances be held responsible for any damage suffered on a site to which one of its Internet users has been redirected.`,
          `Any link to the Site from a Third Party site is prohibited in the absence of prior authorization from The Company, which reserves the right to prosecute any offender. The Company declines any responsibility concerning any link present on an external site and referring to the Site. In addition, the Company cannot under any circumstances be presumed to be associated with any content on this third-party site by the mere existence of this hyperlink.`,
        ],
      },
      {
        title: ['Article 5. Liability - Absence of guarantees'],
        text: [
          `The Site is updated regularly, in order to allow access and use of the Site under the best conditions by any major user.`,
          `However, the user is fully aware in this context that the content on the Site is given for information only.`,
          `The Company cannot be held responsible for any direct or indirect consequence of the reproduction and use of the content of the Site by the Internet user, without prejudice to the application of the provisions relating to intellectual property .`,
          `In addition, the Company reserves the right to modify the Site at any time, and therefore cannot guarantee the accessibility of the Site at all times, without interruption, in good time, in complete safety and without error, nor the 'no errors and bugs.`,
          `The Company cannot be held responsible for any inconvenience and / or damage caused to an Internet user by the actions of third parties who, through unfair technical means, used the information disseminated on the Site. However, the Company ensures that it puts all the means at its disposal to fight against any action of this type, being limited by the technical constraints inherent in the Internet.`,
          `The Company cannot be held responsible for any damage suffered due in particular to the quality of the Internet network and / or technical configurations .`,
          `These conditions of use are subject to French law, and all disputes concerning in particular their application and interpretation will be presented to the commercial court of Nanterre.`,
        ],
      },
      {
        title: [
          'Article 6. Confidentiality Policy - Protection of Personal Data',
        ],
        text: [
          `The Company has implemented a confidentiality policy relating to respect for the privacy of Internet users accessing the Site accessible here: onebeer.fr/poliquedeconfidentialite`,
        ],
      },
    ],
  },
  footer: {
    link_1: 'Privacy policy',
    link_2: 'Legal Notice',
    link_3:
      'Alcohol abuse is dangerous for your health. To consume with moderation',
  },
};

export default gb;
