const fr = {
  lang: 'fr',
  menu: {
    menu_titles: [
      {
        title: 'Nos bières',
        slug: 'our-beers',
      },
      {
        title: 'notre histoire',
        slug: 'our-story',
      },
      {
        title: 'CONTACT',
        slug: 'contact',
      },
      {
        title: 'shop',
        slug: 'shop',
      },
    ],
  },
  side_menu: {
    our_beers: 'Nos bières',
    our_story: 'notre histoire',
    contact: 'CONTACT',
    beers: [
      {
        title: 'blonde',
        slug: 'blonde',
      },
      {
        title: 'blanche',
        slug: 'blanche',
      },
      {
        title: 'cerise',
        slug: 'cerise',
      },
      {
        title: 'citron',
        slug: 'citron',
      },
      {
        title: 'PêCHE',
        slug: 'peche',
      },
      {
        title: 'ROSé',
        slug: 'rose',
      },
    ],
    ul_1: [
      {
        slug: 'shop',
        title: 'Achetez Maintenant',
      },
      {
        slug: 'politique',
        title: 'Politique de Confidentialité',
      },
      {
        slug: 'termes-et-conditions',
        title: 'Mentions légales',
      },
    ],
  },
  auth_18: {
    title_1: 'Pour rentrer sur le site vous devez être majeur.',
    title_2: `Quelle est votre date de naissance ?`,
    form_fields: {
      day: 'JJ',
      month: 'MM',
      year: 'AAAA',
    },
    modal_message: `Vous ne remplissez pas l'âge minimum requis!`,
    button: 'valider',
  },
  home_page: {
    button: 'DÉCOUVREZ-LES VITE ICI !',
    beers: [
      {
        title_1: 'Une bière',
        title_2: '6 saveurs',
        title_3: 'Une histoire sans fin',
        small_text:
          'Toute de bleu vêtue, elle veille à préserver la planète<br/>avec sa bouteille entièrement recyclable.<br/>Découvrez une bière 100 % naturelle, légère et de caractère.<br/>À faible teneur en alcool, elle sait se montrer aussi bien fine, subtile et élégante, que sucrée ou acidulée.<br/>Non filtrée, fraîche et désaltérante, elle se décline sous 6 saveurs pour vous régaler !',
      },
      {
        title_1: 'Une bière',
        title_2: '6 saveurs',
        title_3: 'Une histoire sans fin',
        small_text:
          'Toute de bleu vêtue, elle veille à préserver la planète<br/>avec sa bouteille entièrement recyclable.<br/>Découvrez une bière 100 % naturelle, légère et de caractère.<br/>À faible teneur en alcool, elle sait se montrer aussi bien fine, subtile et élégante, que sucrée ou acidulée.<br/>Non filtrée, fraîche et désaltérante, elle se décline sous 6 saveurs pour vous régaler !',
      },
      {
        title_1: 'Une bière',
        title_2: '6 saveurs',
        title_3: 'Une histoire sans fin',
        small_text:
          'Toute de bleu vêtue, elle veille à préserver la planète<br/>avec sa bouteille entièrement recyclable.<br/>Découvrez une bière 100 % naturelle, légère et de caractère.<br/>À faible teneur en alcool, elle sait se montrer aussi bien fine, subtile et élégante, que sucrée ou acidulée.<br/>Non filtrée, fraîche et désaltérante, elle se décline sous 6 saveurs pour vous régaler !',
      },
      {
        title_1: 'Une bière',
        title_2: '6 saveurs',
        title_3: 'Une histoire sans fin',
        small_text:
          'Toute de bleu vêtue, elle veille à préserver la planète<br/>avec sa bouteille entièrement recyclable.<br/>Découvrez une bière 100 % naturelle, légère et de caractère.<br/>À faible teneur en alcool, elle sait se montrer aussi bien fine, subtile et élégante, que sucrée ou acidulée.<br/>Non filtrée, fraîche et désaltérante, elle se décline sous 6 saveurs pour vous régaler !',
      },
      {
        title_1: 'Une bière',
        title_2: '6 saveurs',
        title_3: 'Une histoire sans fin',
        small_text:
          'Toute de bleu vêtue, elle veille à préserver la planète<br/>avec sa bouteille entièrement recyclable.<br/>Découvrez une bière 100 % naturelle, légère et de caractère.<br/>À faible teneur en alcool, elle sait se montrer aussi bien fine, subtile et élégante, que sucrée ou acidulée.<br/>Non filtrée, fraîche et désaltérante, elle se décline sous 6 saveurs pour vous régaler !',
      },
      {
        title_1: 'Une bière',
        title_2: '6 saveurs',
        title_3: 'Une histoire sans fin',
        small_text:
          'Toute de bleu vêtue, elle veille à préserver la planète<br/>avec sa bouteille entièrement recyclable.<br/>Découvrez une bière 100 % naturelle, légère et de caractère.<br/>À faible teneur en alcool, elle sait se montrer aussi bien fine, subtile et élégante, que sucrée ou acidulée.<br/>Non filtrée, fraîche et désaltérante, elle se décline sous 6 saveurs pour vous régaler !',
      },
    ],
  },
  our_beers: {
    main_title: 'VOTRE VIE N’EST PAS LINÉAIRE ?',
    second_title: 'Nos bières non plus !',
  },
  our_story: {
    main_title: 'À propos de nous',
    content: [
      {
        title: [''],
        text: [
          'Le secteur de l’agro-alimentaire fait partie de l’ADN de notre famille depuis plusieurs générations. Nous avons acquis au fil des années un savoir-faire dans les boissons alcoolisées et désaltérantes non alcoolisées, qui accompagnent le quotidien de chacun. Les vins de Bourgogne, du fait des racines de notre famille, sont une vraie passion. Mais la bière est devenue progressivement notre spécificité. La bière, ou plutôt les bières, puisque notre soif de découverte nous a menés vers toutes les bières du monde ! Ce secteur a connu dernièrement une évolution constante vers la qualité, le respect de l’environnement et la recherche du plaisir gustatif : autant de valeurs qui nous sont chères.',
        ],
      },
      {
        title: [''],
        text: [
          'D’importateurs distributeurs, nous avons naturellement basculé vers la production, mus par la volonté de créer une bière qui nous ressemble. La brasserie de ONE BEER est créée en 1856, 6 générations de succèdent pour travailler et améliorer les bières brassées sur place, toujours avec des chaudrons en cuivre d’origine gage de qualité et d’histoire. Elle est désaltérante et surtout d’une qualité de plus en plus élevée, d’une part grâce à sa technique de brassage, mais surtout à la noblesse du houblon et des fruits qui la composent. Une bière haut de gamme et toute en légèreté, avec un taux d’alcool modéré lui permettant d’être consommée à tout moment de la journée. Un produit noble, tout en restant convivial. Une bière dans l’air du temps. Fabriquée dans le respect de l’environnement, réalisée à partir d’un houblon haut de gamme parce qu’on ne doit pas choisir entre la finesse du goût et l’avenir de la planète ! Une bière peu alcoolisée parce que sa dégustation doit s’intégrer dans un mode de vie sain. Et enfin une bière accessible, parce que le plaisir ne doit pas être réservé à une élite !',
        ],
      },
      {
        title: [''],
        text: [
          'C’est ainsi qu’est née ONE BEER. Une bière haut de gamme, qui ne fait pas tourner la tête mais qui fait voyager ! Une bière qui se décline aussi en plusieurs versions aromatisées, à base de fruits naturels, non filtrée. Sa bouteille bleue deviendra vite synonyme de convivialité… tout en préservant la planète puisqu’elle est totalement respectueuse de l’environnement !',
        ],
      },
      {
        title: [''],
        text: [
          'ONE BEER, un nom unique pour une gamme entière. Une bière blonde et une bière blanche, cette dernière existant en plusieurs versions fruitées. Elle séduira les amateurs de bières attachés à la tradition et saura conquérir de nouveaux adeptes. ONE BEER, une bière pour chaque occasion avec un dénominateur commun : le plaisir, sans la culpabilité !',
        ],
      },
    ],
  },
  contact: {
    main_title: 'Nous contacter',
    excerpt:
      'ONE BEER est une marque de RDJ Group déposée en Europe et dans d’autres pays.',
    distributers: [
      {
        text_1: 'Distributeur officiel',
        text_2: 'International Business Service',
        email: 'www.ibs-wholesale.com',
        color: '#EBD864',
      },
      {
        text_1: 'Distributeur Belgique',
        text_2: 'Société SRX Event',
        email: 'xavier@srxlux.hk',
        color: '#CA6182',
      },
      {
        text_1: 'Distributeur Cameroun / Gabon / Côte d’Ivoire:',
        text_2: 'Société Empiretrad-Group',
        email: 'empiretradgroup.sarl@gmail.com',
        color: '#ECA03A',
      },
      {
        text_1: 'Distributeur HONG KONG / CHINE',
        text_2: 'Sun Po Wing LTD',
        email: 'yvonnelam@sunpowing.hk',
        color: '#EEBF52',
      },
    ],
    form: {
      name: 'Nom et Prenom',
      email: 'E-mail',
      message: 'Message',
      button: 'Envoyer',
    },
  },
  shop: {},
  politique_de_confidentialité: {
    main_title: 'Politique de Confidentialité',
    content: [
      {
        title: [''],
        text: [
          `ONE BEER est une marque de RDJ GROUP Société par Actions Simplifiée au capital de
  3 502 000 euros dont le siège social est situé 36 rue Anatole France 92300 Levallois-Perret
  immatriculée au RCS de Nanterre sous le N°888 296 274 édite le site
  Internet www.onebeer.fr (le « Site »).`,
          `ONE BEER est attaché au respect de la vie privée des Internautes accédant au Site. La
  présente Politique de confidentialité a pour objet de permettre à Internaute de prendre
  connaissance des modalités de collectes et de traitements des données à caractère
  personnel le concernant tout au long de sa navigation sur le Site et de ses droits en
  matière de respect de sa vie privée et de protection de ses données à caractère personnel.`,
          `Dans le cadre des traitements de données à caractère mise en œuvre sur le Site, ONE
  BEER se conforme à la Loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux
  fichiers et aux libertés (loi « Informatique et Libertés ») et au Règlement Général sur la
  Protection des 2016/679 du 27 avril 2016 (« RGPD ») (ensemble la « Règlementation sur la
  protection des données »)`,
          <>
            Dans le cadre de sa politique de respect de la vie privée des
            personnes, ONE BEER a désigné un Délégué à la Protection des Données
            que vous pouvez contacter pour toute demande relative à la
            protection des données à caractère personnel à l’adresse suivante :{' '}
            <a href="mailto:info@onebeer.fr">info@onebeer.fr</a>
          </>,
        ],
      },
      {
        title: [
          '1/ TRAITEMENT DE VOS DONNÉES DANS LE CADRE DE VOTRE NAVIGATION SUR LE SITE',
        ],
        text: [
          `Au cours de votre navigation sur le Site nous sommes susceptibles de collecter des
          données vous concernant. Aucune des données ne sera collectée à votre insu. Nous ne
          collectons vos données que pour les finalités déterminées, explicites et légitimes
          suivantes :`,
          `• Répondre à vos demandes d’informations de toute nature,`,
          `• Améliorer et analyser nos services, mieux comprendre vos besoins et vos intérêts,<br/>
          et personnaliser le cas échéant nos contenus et nos offres.`,
          `• Réaliser la maintenance et optimisation du site en vue de vérifier/améliorer la<br/>
          qualité de service, la disponibilité et la performance du service, solutionner les<br/>
          éventuels problèmes ou anomalies de fonctionnement, et sécuriser le site contre la<br/>
          fraude.`,
        ],
      },
      {
        title: ['2/ AVEC QUI PARTAGEONS-NOUS VOS DONNÉES ?'],
        text: [
          `Nous sommes amenés à partager les données vous concernant avec des tiers soustraitants pour nous aider à exécuter et fournir les services mentionnés ci-dessus (tels que
            notamment hébergeur, éditeur). Nous nous assurons que nos sous-traitants présentent
            les garanties suffisantes quant aux mesures techniques et organisationnelles mises en
            œuvre de manière à ce que l’usage des données soit effectué conformément à la
            Règlementation sur la protection des données.`,
        ],
      },

      {
        title: ['3/ OÙ SONT LOCALISÉES VOS DONNÉES ?'],
        text: [
          `Les données peuvent être localisées dans un pays de l’Union européenne mais aussi en
          dehors de l’Union Européenne. Dans ce dernier cas, nous nous assurons que les
          données se situent dans un pays hors UE assure un niveau de protection adéquat
          concernant le traitement des données en conformité avec la Règlementation sur la
          protection des données.`,
          `A défaut, nous prenons des garanties appropriées de manière à ce que le niveau de
          protection garanti par la Règlementation sur la protection des données.`,
        ],
      },

      {
        title: ['4/ DURÉE DE CONSERVATION '],
        text: [
          `Vos données sont collectées et traitées dans le cadre des services fournis sur le Site et
          conformément à la présente Politique. Nous conservons vos données pendant une durée
          n’excédant pas celle nécessaire à la réalisation des finalités décrites précédemment, dans
          la mesure nécessairement raisonnable afin de respecter une exigence légale en vigueur,
          ou pendant toute période utile au regard d’un délai de prescription applicable ou à
          l’établissement de la preuve d’un droit. `,
        ],
      },

      {
        title: ['A5/ COMMENT SÉCURISONS-VOUS VOS DONNÉES ?'],
        text: [
          `ONE BEER s’engage à prendre toutes les mesures techniques et organisationnelles
          nécessaires afin de garantir un niveau de sécurité, d’intégrité et de confidentialité adapté
          au risque compte tenu notamment de l’état des connaissances, des coûts de mise en 
          œuvre et de la nature, de la portée, du contexte et des finalités du traitement pour
          protéger vos données contre le vol, la destruction, l’altération, la perte accidentelle, la
          diffusion ou l’accès non autorisé.`,
        ],
      },

      {
        title: ['6/ COOKIES '],
        text: [
          <>
            Nous utilisons des cookies ou des technologies similaires pour
            collecter des données sur notre Site afin de nous fournir des
            renseignements et nous souvenir de vous quand vous vous connectez
            sur notre Site, mieux comprendre vos intérêts, faciliter l’accès à
            nos services, assurer l’exécution de nos services, les analyser et
            nous permettre de proposer des publicités et des offres
            personnalisées.`, `Pour en savoir plus, prenez connaissance de notre
            politique en matière de cookie accessible sur ce Site à l’adresse
            suivante : <a href="mailto:info@onebeer.fr">info@onebeer.fr</a>
          </>,
        ],
      },

      {
        title: ['7/ VOS DROITS '],
        text: [
          `Vous disposez de droits sur vos données à caractère personnel. Conformément à la
          réglementation en matière de protection des données à caractère personnel et après
          avoir justifié de votre identité, vous bénéficiez, dans les conditions prévues aux articles 15
          à 22 du RGPD :`,

          `d’un droit d’accès : l’internaute peut obtenir communication des données le
          concernant; `,
          `• d’un droit de rectification : en cas d’inexactitude de ces informations, l’internaute
          peut exiger qu’elles soient rectifiées ou complétées;`,
          `• d’un droit d’effacement : l’internaute peut exiger que ses données soient effacées,
          équivalent pour tout inscrit à un espace privé, à la fermeture de son compte.`,
          `• d’un droit d’opposition : l’internaute peut s’opposer au traitement de ses données
          à des fins de prospection commerciale ; `,
          `• d’un droit de limitation : l’internaute peut obtenir la limitation du traitement de<br/> 
          ses données ;`,
          `•d’un droit de portabilité : l’internaute peut demander de recevoir ses données
          personnelles qu’il a fournies dans un format structuré, couramment utilisé et
          lisible par machine, dans certaines circonstances, ou demander qu’elles soient
          transmises à un autre responsable de traitement si cela est techniquement
          possible `,

          <>
            Vous pouvez contactez nos Services afin d’exercer vos droits à
            l’adresse électronique suivante :{' '}
            <a href="mailto:info@onebeer.fr">info@onebeer.fr</a> en joignant à
            votre demande une copie d’un titre d’identité.{' '}
          </>,

          `Notez que vous pouvez également rectifier ou supprimer des données directement sur
          votre compte client sous réserves que nous ayons besoin de conserver certaines données
          pour satisfaire à des exigences légales et/ou pour la gestion de la relation commerciale, de
          vos commandes ou de vos factures. `,

          `Si vous avez donné votre consentement pour le traitement de vos données personnelles,
          vous pouvez retirer votre consentement à tout moment, sans que cela ne porte atteinte à
          la licéité du traitement avant le retrait. `,
        ],
      },

      {
        title: ['8/ LA COMMISSION NATIONALE DE L’INFORMATIQUE ET DES LIBERTÉS'],
        text: [
          <>
            ONE BEER vous rappelle que vous pouvez contacter la CNIL directement
            sur le site internet{' '}
            <a href="https://www.cnil.fr/fr/agir" target="_blank">
              {' '}
              https://www.cnil.fr/fr/agir
            </a>{' '}
            ou par courrier à l’adresse suivante : Commission Nationale de
            l’Informatique et des Libertés, 3 Place de Fontenoy – TSA 80715,
            75334 PARIS CEDEX 07
          </>,
        ],
      },
    ],
  },
  cookie_agreement: {
    text: `En cliquant sur "Accepter tous les cookies", vous acceptez le stockage de cookies sur votre appareil pour améliorer la navigation,<br/>sur le site, analyser l'utilisation du site et nous aider dans nos efforts de marketing.`,
    button_text: `Accepter tous les cookies`,
  },
  mentions_légales: {
    main_title: 'Mentions légales',
    content: [
      {
        title: ['1/ IDENTITE'],
        text: [
          <>
            Le Site internet ONE BEER, (le «Site») est accessible via l’adresse{' '}
            <a href="/">www.onebeer.fr</a> et appartient à RDJ GROUP, SAS au
            capital de 3 502 000 Euros, dont le siège social est 36 rue Anatole
            France 92300 Levallois-Perret, immatriculée au Registre du Commerce
            et des sociétés de Nanterre sous le numéro 888 296 274 («la
            Société»).
          </>,
          `Numéro de TVA intracommunautaire : FR74888296274`,
          <>
            Pour toute correspondance, veuillez utiliser l’adresse suivante :{' '}
            <a href="mailto:contact@onebeer.fr">contact@onebeer.fr</a>
          </>,
        ],
      },
      {
        title: ['2/ CONDITIONS D’UTILISATION DU SITE'],
        text: [
          `Ces conditions d’utilisation et la charte de protection des données ont pour objet d’assurer l’utilisation du Site, dans le respect des droits de chacun , de la vie privée et de la propriété intellectuelle.`,
          <>
            Le Site internet <a href="/">www.onebeer.fr</a> est un service de
            communication y incluant une boutique en ligne avec accès aux
            internautes majeurs. Le site propose un espace d’informations et
            d’achat relatif aux activités et produits de la marque ONE BEER.
          </>,
          `Le Site est soumis aux dispositions de la Loi Evin du 10 janvier 1991, son accès est exclusivement destiné aux seules personnes majeures.`,
          <>
            L’accès au Site et son utilisation sont soumis aux conditions
            stipulées ci-après. En accédant et naviguant sur le site{' '}
            <a href="/">www.onebeer.fr</a> l’internaute accepte sans réserve les
            présentes conditions d’utilisation et s’engage à les respecter.
          </>,
          `Si l’internaute n’accepte pas ces conditions, il est prié de quitter le site et de ce fait, de ne pas l’utiliser. Les présentes mentions légales peuvent être modifiées à tout moment. Toute modification sera publiée sur le Site et tout utilisateur accédant au Site après leur publication sera présumé avoir accepté ces modifications.
          `,
        ],
      },
      {
        title: ['3/ PROPRIÉTÉ INTELLECTUELLE'],
        text: [
          `Tout contenu du Site, y compris logos , marque, photos, illustrations, images, textes sur le Site, est protégé en vertu des droits de propriété intellectuelle, ou de toute autre disposition légale applicable en l’espèce. La Société RDJ GROUP est seule titulaire de tous les droits de propriété relatifs au contenu du Site.`,
          `La distribution, la reproduction, l’adaptation de ses logos, marques et tous les autres contenus sur le Site est interdite sans autorisation préalable de la Société titulaire des droits, pour des fins commerciales ou non. La Société RDJ GROUP se réserve la possibilité de poursuivre toute personne ne respectant pas ces dispositions. La Société respecte les droits de propriété intellectuelle , la protection des droits et intérêts des tiers et toutes les dispositions légales en vigueur.`,
        ],
      },
      {
        title: ['4/ LIENS WEB'],
        text: [
          `Le Site peut contenir des liens vers des sites web tiers ou liens de téléchargement . Accéder à un de ces liens dirigera automatiquement l’utilisateur du Site vers ces sites qui sont soumis à des conditions d’utilisation qui leur sont propres. Il appartient à chacun de prendre connaissance des engagements de chaque site web vers lequel il a été redirigé avant la navigation.`,
          `La Société RDJ GROUP ne gère pas ces autres sites tiers, et ne peut en aucun cas en surveiller le contenu ou les conditions d’utilisation, notamment en matière de respect de la vie privée. La Société RDJ GROUP ne pourra en aucun cas être tenue responsable pour tout dommage subi sur un site vers lequel un de ses internautes aurait été redirigé.`,
          <>
            Tout lien vers le Site <a href="/">www.onebeer.fr</a> présent depuis
            un autre site est interdit en l’absence d’autorisation préalable de
            la Société RDJ GROUP qui se réserve le droit de poursuivre tout
            contrevenant.
          </>,
          <>
            La Société RDJ GROUP décline toute responsabilité si un ou plusieurs
            liens présent sur un site externe renvoi vers le site{' '}
            <a href="/">www.onebeer.fr </a>{' '}
          </>,
          `  En outre, La Société RDJ GROUP ne pourra être présumé associé à un
          contenu présent au sein de sites tiers par la simple existence de
          ces liens.`,
        ],
      },
      {
        title: ['5/ RESPONSABILITÉ'],
        text: [
          <>
            Le Site <a href="/">www.onebeer.fr</a> est mis à jour
            ponctuellement, afin de permettre un accès et une navigation sur le
            site web dans les meilleures conditions par tout utilisateur majeur.
          </>,
          `La Société RDJ GROUP ne pourra être tenue responsable de toute conséquence directe et indirecte de la reproduction et l’utilisation du contenu du Site par l’Internaute, sans préjudice de l’application des dispositions relatives à la propriété intellectuelle.
          `,
          <>
            La Société RDJ GROUP peut modifier le Site à tout moment. La Société
            RDJ GROUP ne peut être tenue responsable de tout désagrément ou
            dommage causé à un internaute, par des actions de personnes qui, par
            tous moyens techniques , ont utilisé les informations diffusées sur
            le Site <a href="/">www.onebeer.fr</a>
          </>,
          `La Société RDJ GROUP assure mettre en oeuvre tous les moyens à sa disposition pour contrer toute action de ce genre , les seules limitations sont les contraintes techniques inhérentes à Internet. De plus , La Société RDJ GROUP ne pourra être tenue responsable des dommages subis du fait de la qualité du réseau Internet dans les zones de connexion.`,
          `Ces conditions d’utilisation du site www.onebeer.fr sont soumises à la loi Française. Tout éventuel litige sera t amené devant le tribunal de commerce de Nanterre.`,
        ],
      },
      {
        title: ['6/PROTECTION DES DONNÉES PERSONNELLES'],
        text: [
          <>
            Une Politique de confidentialité relative au respect de la vie
            privée est consultable sur le site www.onebeer.fr , à l’adresse
            suivante:{' '}
            <a href="/politique">www.onebeer.fr/poliquedeconfidentialite</a>
          </>,
        ],
      },
    ],
  },
  footer: {
    link_1: 'Politique de Confidentialité',
    link_2: 'Mentions légales',
    link_3:
      'L’abus d’alcool est dangereux pour santé. À consommer avec modération',
  },
};

export default fr;
