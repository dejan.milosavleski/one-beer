import { useEffect } from 'react';
import Layout from '../components/Layout';
import fruit from '../styles/assets/images/FRUITS/FRUIT_PECHE.png';
import logo from '../styles/assets/images/logos/onebeer-peche.svg';
import logo_black from '../styles/assets/images/Logos-Labels/onebeer-logo-black.svg';
import logo_white from '../styles/assets/images/Logos-Labels/onebeer-logo-white.svg';
import bottles from '../styles/assets/images/single-beer-with-fruits/peche.png';
import Single_beer from '../containers/single_beer/SingleBeer';
import puces_1 from '../styles/assets/images/puces_1/PUCE_PECHE-new.png';
import puces_2 from '../styles/assets/images/puces_1/PUCE_PECHE-new-1.png';
// import ilustration from '../styles/assets/images/ilustracii_flip/peche.png';
import ilustration from '../styles/assets/images/ILLUSTRATION/peche.gif';
import text_pic from '../styles/assets/images/section_5/Essayez-lemaintenant.png';
import { useSelector, useDispatch } from 'react-redux';
import { change_lang } from '../store/single_beer_locale/action';
import Cookies from 'js-cookie';
import { useWindowSize } from '../hooks/UseWindowSize';
const Peche = () => {
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const lang = Cookies.get('lang');
  const windowSize = useWindowSize();
  const dispatch = useDispatch();

  useEffect(() => {
    if (lang) {
      dispatch(change_lang(lang));
    }
  }, []);

  return (
    <Layout bg_color="bg-peche" menu_text="#fff" menu_icons="blue" single_beer_logo={logo_white} single_wrapper="content-peche">
        <Single_beer
         section_1={locale && locale.blanche && locale.peche.section_1}
         section_2={locale && locale.blanche && locale.peche.section_2}
         section_3={locale && locale.blanche && locale.peche.section_3}
         section_4={locale && locale.blanche && locale.peche.section_4}
         section_5={locale && locale.blanche && locale.peche.section_5}
        border_color="#EBA03B"
        logo={logo}
        puces_1={puces_1}
        puces_2={puces_2}
        fruit={fruit}
        beer_name="beerName_peche-black-icon"
        bottles={bottles}
        ilustration={ilustration}
        text_pic={text_pic}
        menu_text={ windowSize.width <=768 ? "#000" : "#fff"}
      />
    </Layout>
  );bla
}

export default Peche;