import { useEffect } from 'react';
import Layout from '../components/Layout';
import logo from '../styles/assets/images/logos/onebeer-cerise.svg';
import fruit from '../styles/assets/images/FRUITS/FRUIT_CERISE.png';
import bottles from '../styles/assets/images/single-beer-with-fruits/cerise.png';
import Single_beer from '../containers/single_beer/SingleBeer';
import puces_1 from '../styles/assets/images/puces_1/PUCE_CERISE-new.png';
import puces_2 from '../styles/assets/images/puces_1/PUCE_CERISE-new-1.png';
// import ilustration from '../styles/assets/images/ilustracii_flip/cerise.png';
import ilustration from '../styles/assets/images/ILLUSTRATION/cerise.gif';
import text_pic from '../styles/assets/images/section_5/Essayez-lemaintenant.png';
import { useSelector, useDispatch } from 'react-redux';
import { change_lang } from '../store/single_beer_locale/action';
import Cookies from 'js-cookie';
import { useWindowSize } from '../hooks/UseWindowSize';
const Cerise = () => {
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const lang = Cookies.get('lang');
  const windowSize = useWindowSize();
  const dispatch = useDispatch();
  useEffect(() => {
    if (lang) {
      dispatch(change_lang(lang));
    }
  }, []);
 
  return (
    <Layout bg_color="bg-cerise" menu_text="" menu_icons="blue" single_wrapper="content-cerise">
      <Single_beer
        section_1={locale && locale.blanche && locale.cerise.section_1}
        section_2={locale && locale.blanche && locale.cerise.section_2}
        section_3={locale && locale.blanche && locale.cerise.section_3}
        section_4={locale && locale.blanche && locale.cerise.section_4}
        section_5={locale && locale.blanche && locale.cerise.section_5}
        border_color="#C42038"
        logo={logo}
        puces_1={puces_1}
        puces_2={puces_2}
        fruit={fruit}
        beer_name="beerName_cerise-black-icon"
        bottles={bottles}
        ilustration={ilustration}
        text_pic={text_pic}
        menu_text={ windowSize.width <=768 ? "#000" : "#fff"}
      />
    </Layout>
  );
};

export default Cerise;
