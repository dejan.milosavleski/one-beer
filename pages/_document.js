import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
       
          <Main />
          <NextScript />
          
          <canvas id="projector">Your browser does not support the Canvas element.</canvas>
          <script src="https://code.createjs.com/easeljs-0.7.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
            <script src="https://cdpn.io/cp/internal/boomboom/pen.js?key=pen.js-edfcfdf0-0f3f-a7b8-1f23-aca79fb5412f" ></script>
          <script type="text/javascript" src="/parallaxEffect.js"></script>
        </body>
      </Html>
    )
  }
}

export default MyDocument