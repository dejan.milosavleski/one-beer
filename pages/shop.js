import { useWindowSize } from '../hooks/UseWindowSize';
import Link from 'next/link';
import { useRouter } from 'next/router';
const Shop = () => {
  const windowSize = useWindowSize();
  const router = useRouter();
  const styles = {
    fontFamily: 'Amanda',
    fontSize: windowSize.width <= 576 ? '3rem' : '8rem',
    color: 'white',
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: '9'
  };
  return (
    <div className="dark-blue shop d-flex justify-content-center aling-items-center">
   
        <button onClick={() => router.back()}>
          <i className="mr-3 icon arrow-transparent-left-icon"></i> back
        </button>
    
      <h2 style={styles}>Coming Soon...</h2>
    </div>
  );
};

export default Shop;
