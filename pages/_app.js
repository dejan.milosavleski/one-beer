import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/assets/scss/main.scss';
import { wrapper } from '../store/store';
import { AnimatePresence } from 'framer-motion';

function MyApp({ Component, pageProps, router }) {


  return (
    <AnimatePresence exitBeforeEnter>
      <Component {...pageProps} key={router.route} />
    </AnimatePresence>
  );
}

export default wrapper.withRedux(MyApp);
