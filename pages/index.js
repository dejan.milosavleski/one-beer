import { useEffect } from 'react'
import Auth_18 from '../containers/auth_18/Auth_18';
import Layout from '../components/Layout';
const Index = () => {

  return (
    <Layout bg_color="auth-display" single_wrapper="content-auth">
      <Auth_18 />
    </Layout>
  );
};

export default Index;
