import { useEffect } from 'react';
import Layout from '../components/Layout';
import logo from '../styles/assets/images/logos/onebeer-citron.svg';
import logo_black from '../styles/assets/images/logos/onebeer-logo-black.svg';
import fruit from '../styles/assets/images/FRUITS/FRUIT_CITRON.png';
import bottles from '../styles/assets/images/single-beer-with-fruits/citron.png';
import Single_beer from '../containers/single_beer/SingleBeer';
import puces_1 from '../styles/assets/images/puces_1/PUCE_CITRON-new.png';
import puces_2 from '../styles/assets/images/puces_1/PUCE_CITRON-new-1.png';
// import ilustration from '../styles/assets/images/ilustracii_flip/citron.png';
import ilustration from '../styles/assets/images/ILLUSTRATION/citron.gif';
import text_pic from '../styles/assets/images/section_5/Essayez-lemaintenant.png';
import { useSelector, useDispatch } from 'react-redux';
import { change_lang } from '../store/single_beer_locale/action';
import Cookies from 'js-cookie';
import { useWindowSize } from '../hooks/UseWindowSize';
const Citron = () => {
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const lang = Cookies.get('lang');
  const windowSize = useWindowSize();
  const dispatch = useDispatch();
  useEffect(() => {
    if (lang) {
      dispatch(change_lang(lang));
    }
  }, []);
  
  return (
    <Layout bg_color="bg-citron" menu_text="#000" menu_icons="blue" single_beer_logo={logo_black} single_wrapper="content-citron">
      <Single_beer
       section_1={locale && locale.blanche && locale.citron.section_1}
       section_2={locale && locale.blanche && locale.citron.section_2}
       section_3={locale && locale.blanche && locale.citron.section_3}
       section_4={locale && locale.blanche && locale.citron.section_4}
       section_5={locale && locale.blanche && locale.citron.section_5}
        border_color="#EEDA64"
        logo={logo}
        puces_1={puces_1}
        puces_2={puces_2}
        fruit={fruit}
        beer_name="beerName_citron-black-icon"
        bottles={bottles}
        ilustration={ilustration}
        text_pic={text_pic}
        menu_text={ windowSize.width && windowSize.width <=768 ? "#000" : "#fff"}
      />
    </Layout>
  );
};

export default Citron;
