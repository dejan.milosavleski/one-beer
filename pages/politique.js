import logo from '../styles/assets/images/ONE_BEER_LOGO.png';
import Layout from '../components/Layout';
import OurStory from '../containers/our_story/OurStory';
import { useSelector } from 'react-redux';
const Politique_de_confidentialité = () => {
  const politique_de_confidentialité = useSelector(
    state => state.global_locale.selected_lang.politique_de_confidentialité
  );
  return (
    <Layout bg_color="dark-blue" logo={logo} single_wrapper="content-politique">
      <OurStory
        content={politique_de_confidentialité && politique_de_confidentialité.content}
        content_className="politique"
        main_title={politique_de_confidentialité && politique_de_confidentialité.main_title}
      />
    </Layout>

  );
};

export default Politique_de_confidentialité;
