import { useEffect } from 'react';
import { useSelector  } from 'react-redux';
import OurStoryPage from '../containers/our_story/OurStory';
import logo from '../styles/assets/images/ONE_BEER_LOGO.png';
import Layout from '../components/Layout';
import { useRouter } from 'next/router';

const Our_Story = () => {
  const router = useRouter();
  const our_story = useSelector(state => state.global_locale.selected_lang.our_story)
  useEffect(() => {
    const data = localStorage.getItem('date');
    if (data === null) {
      router.push('/');
    }
  }, []);

  return (
    <Layout bg_color="dark-blue" logo={logo} single_wrapper="content-ourstory">
      <OurStoryPage content={our_story && our_story.content} main_title={our_story && our_story.main_title} />
    </Layout>
  );
};

export default Our_Story;
