const nodemailer = require('nodemailer');
const { google } = require('googleapis');
import Cookies from 'js-cookie';


const lang = Cookies.get('lang')
export default async (req, res) => {
  const { email } = req.body;

  const CLIENT_ID =`190847509724-0gbvf3vddmpugfc4ob5kr9sp3nq3r9al.apps.googleusercontent.com`;
  const CLIENT_SECRET = `TqNJdV-iSiTA-7t8D2vavVh8`;
  const REDIRECT_URI = `https://developers.google.com/oauthplayground`
  const REFRESH_TOKEN = `1//04CYvuEPIVLllCgYIARAAGAQSNgF-L9IrONqcXyr0cOfA4GKw0thijR_5teHIxzJ_NmBgCsqZ8XjitlKfhKrPC3ziNrHwszDoww`

  const oAuth2Client = new google.auth.OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI
  );
  oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

  const sendMail = async () => {
   
      const accesToken = await oAuth2Client.getAccessToken();
      const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          type: 'oAuth2',
          user: 'ivan.ivanov@altius.mk ',
          clientId: CLIENT_ID,
          clientSecret: CLIENT_SECRET,
          refreshToken: REFRESH_TOKEN,
          accesToken: accesToken,
        },
      });

      const mailOptions = {
        from: `${email}📧<${email}>`,
        to: 'ivanveles2003@yahoo.com, info@onebeer.fr',
        subject:  lang && lang === "fr" ? 'Onebeer Formulaire E-mail' : `Onebeer Email-Form`,
        text: lang && lang === "fr" ? "contacter le courrier  "  + email: 'contact mail   ' + email,
      };

      const result = await transport.sendMail(mailOptions);
      return result;
  };
  try {
    await sendMail();
    res.json({ message: lang && lang === "fr" ? 'Le courriel a été envoyé' : `Email has been sent` });
  } catch (error) {
    console.log(error.message);
  }
};
