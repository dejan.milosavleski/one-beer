import { useEffect } from 'react';
import Layout from '../components/Layout';
import fruit from '../styles/assets/images/FRUITS/ILLU_BLONDE.png';
import logo from '../styles/assets/images/logos/onebeer-blonde.svg';
import logo_black from '../styles/assets/images/logos/onebeer-logo-black.svg';
import logo_white from '../styles/assets/images/Logos-Labels/onebeer-logo-white.svg';
import bottles from '../styles/assets/images/single-beer-with-fruits/blonde.png';
import Single_beer from '../containers/single_beer/SingleBeer';
import puces_1 from '../styles/assets/images/puces_1/PUCE_BLONDE-new.png';
import puces_2 from '../styles/assets/images/puces_1/PUCE_BLONDE-new-1.png';
// import ilustration from '../styles/assets/images/ilustracii_flip/blonde.png';
import ilustration from '../styles/assets/images/ILLUSTRATION/blonde.gif';
import text_pic from '../styles/assets/images/section_5/Essayez-lemaintenant.png';
import { useSelector, useDispatch } from 'react-redux';
import { change_lang } from '../store/single_beer_locale/action';
import Cookies from 'js-cookie';
import { useWindowSize } from '../hooks/UseWindowSize';
const Blonde = () => {
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const lang = Cookies.get('lang');
  const dispatch = useDispatch();

  useEffect(() => {
    if (lang) {
      dispatch(change_lang(lang));
    }
  }, []);
    const windowSize = useWindowSize();
  return (
    <Layout bg_color="bg-blonde" menu_text="#fff" menu_icons="blue" single_beer_logo={logo_black} single_wrapper="content-blonde">
      <Single_beer
        section_1={locale && locale.blanche && locale.blonde.section_1}
        section_2={locale && locale.blanche && locale.blonde.section_2}
        section_3={locale && locale.blanche && locale.blonde.section_3}
        section_4={locale && locale.blanche && locale.blonde.section_4}
        section_5={locale && locale.blanche && locale.blonde.section_5}
        border_color="#C3A245"
        logo={logo}
        puces_1={puces_1}
        puces_2={puces_2}
        fruit={fruit}
        beer_name="beerName_blonde-black-icon"
        bottles={bottles}
        ilustration={ilustration}
        text_pic={text_pic}
        menu_text={ windowSize.width <=768 ? "#000" : "#fff"}
      />
    </Layout>
  );
};

export default Blonde;
