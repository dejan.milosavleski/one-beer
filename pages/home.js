import { useEffect, useState } from 'react';
import Layout from '../components/Layout';
import HomePage from '../containers/home/Home';
import logo from '../styles/assets/images/logos/onebeer-logo-white.svg';
import VideoPlayer from '../components/VideoPlayer';
import Video_file_2 from '../public/One Beer Citron 2-No_Logo_11Sec.mp4';
import { useWindowSize } from '../hooks/UseWindowSize';
const Home = () => {
  const [isVideoFinished, setIsVideoFinished] = useState(false);
  const [isVideoStarted, setIsVideoStarted] = useState(false);
  const [hideVideo, setHideVideo] = useState(false);
  const [showLayout, setShowLayout] = useState(false);
  const windowSize = useWindowSize();

  // hide video 2 second before is finished
  useEffect(() => {
    if (isVideoFinished) {
      const timer = setTimeout(() => {
        setHideVideo(true);
      }, 1800);
      return () => clearTimeout(timer);
    }
  }, [isVideoFinished]);

  // show video first
  useEffect(() => {
    if (windowSize.width && windowSize.width <= 576 && isVideoStarted) {
      const timer = setTimeout(() => {
        setIsVideoFinished(true);
        setIsVideoStarted(false);
      }, 8500);
      return () => clearTimeout(timer);
    }
  }, [windowSize, isVideoStarted]);

  // render layout after video
  useEffect(() => {
    const timer = setTimeout(() => {
      setShowLayout(true);
    }, 3000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <Layout
          bg_color="dark-blue"
          logo={logo}
          menu_text="#fff"
          single_wrapper="content-home"
        >
          <HomePage menu_text_color="#fff" />
        </Layout>
    // <>
    //   {showLayout && (
    //     <Layout
    //       bg_color="dark-blue"
    //       logo={logo}
    //       menu_text="#fff"
    //       single_wrapper="content-home"
    //     >
    //       <HomePage menu_text_color="#fff" />
    //     </Layout>
    //   )}

    //   <div
    //     className={`video-player  ${isVideoFinished && 'fade-out'}`}
    //     style={{ display: hideVideo && 'none' }}
    //   >
    //     <VideoPlayer
    //       url={Video_file_2}
    //       setIsVideoFinished={setIsVideoFinished}
    //       setIsVideoStarted={setIsVideoStarted}
    //     />
    //   </div>
    // </>
  );
};
export default Home;
