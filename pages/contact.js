import { useEffect } from 'react';
import Layout from '../components/Layout';
import Contact from '../containers/contact/Contact';
import logo from '../styles/assets/images/ONE_BEER_LOGO.png';
import { useRouter } from 'next/router';
const Home = () => {
  const router = useRouter();
  useEffect(() => {
    const data = localStorage.getItem('date');
    if (data === null) {
      router.push('/');
    }
}, []);

  return (
    <Layout  bg_color="dark-blue" logo={logo} single_wrapper="content-contact">
      <Contact/>
    </Layout>
  );
}
export default Home;