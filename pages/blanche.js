import { useEffect, useState } from 'react';
import Layout from '../components/Layout';
import logo from '../styles/assets/images/logos/onebeer-blanche.svg';
import logo_black from '../styles/assets/images/logos/onebeer-logo-black.svg';
import fruit from '../styles/assets/images/FRUITS/ECORCE_ORANGE_CORIANDRE_a.png';
import bottles from '../styles/assets/images/single-beer-with-fruits/blanche.png';
import Single_beer from '../containers/single_beer/SingleBeer';
import puces_1 from '../styles/assets/images/puces_1/PUCE_BLANCHE-new.png';
import puces_2 from '../styles/assets/images/puces_1/PUCE_BLANCHE-new-1.png';
// import ilustration from '../styles/assets/images/ilustracii_flip/blanche.png';
import ilustration from '../styles/assets/images/ILLUSTRATION/blanche.gif';
import text_pic from '../styles/assets/images/section_5/Essayez-lemaintenant.png';
import { useSelector, useDispatch } from 'react-redux';
import { change_lang } from '../store/single_beer_locale/action';
import Cookies from 'js-cookie';

const Blanche_Beer = () => {
  const locale = useSelector(state => state.single_beer_locale.selected_lang);
  const lang = Cookies.get('lang');
  const dispatch = useDispatch();
  useEffect(() => {
    if (lang) {
      dispatch(change_lang(lang));
    }
  }, [lang]);

  return (
    <Layout
      bg_color="bg-blanche"
      menu_text="#000"
      menu_icons="blue"
      single_beer_logo={logo_black}
      single_wrapper="content-blanche"
    >
      <Single_beer
        section_1={locale && locale.blanche && locale.blanche.section_1}
        section_2={locale && locale.blanche && locale.blanche.section_2}
        section_3={locale && locale.blanche && locale.blanche.section_3}
        section_4={locale && locale.blanche && locale.blanche.section_4}
        section_5={locale && locale.blanche && locale.blanche.section_5}
        border_color="#AFDDF8"
        logo={logo}
        puces_1={puces_1}
        puces_2={puces_2}
        fruit={fruit}
        beer_name="beerName_blanche_black-icon"
        bottles={bottles}
        ilustration={ilustration}
        text_pic={text_pic}
        menu_text="#000"
      />
    </Layout>
  );
};

export default Blanche_Beer;
