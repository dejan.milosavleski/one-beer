import OurStoryPage from '../containers/our_story/OurStory';
import logo from '../styles/assets/images/ONE_BEER_LOGO.png';
import Layout from '../components/Layout';
import { NotreResponsabilite } from '../constants/text/NotreResponsabilite';
import { useSelector } from 'react-redux';
const Termes_et_conditions = () => {
  const mentions_légales = useSelector(
    state => state.global_locale.selected_lang.mentions_légales
  );
  return (
    <Layout bg_color="dark-blue" logo={logo} single_wrapper="content-terms-et-conditions">
      <OurStoryPage content={mentions_légales && mentions_légales.content} main_title={mentions_légales && mentions_légales.main_title}
      content_className="mentions-legales"
      />
    </Layout>
  );
};

export default Termes_et_conditions;
