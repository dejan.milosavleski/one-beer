import OurStoryPage from '../containers/our_story/OurStory';
import logo from '../styles/assets/images/ONE_BEER_LOGO.png';
import Layout from '../components/Layout';
import { Boire_Responsamble } from '../constants/text/BoireResponsamble';
const Boire_responsable = () => {
  return (
    <Layout bg_color="dark-blue" logo={logo} single_wrapper="content-responsabilite-sociale">
      <OurStoryPage content={Boire_Responsamble} />
    </Layout>
  );
};

export default Boire_responsable;