import { useEffect } from 'react';
import Layout from '../components/Layout';
import OurBeersPage from '../containers/our_beers/Our_Beers';
import logo from '../styles/assets/images/ONE_BEER_LOGO.png';
import { useRouter } from 'next/router';
const Our_beers = () => {
  const router = useRouter();
  useEffect(() => {
    const data = localStorage.getItem('date');
    if (data === null) {
      router.push('/');
    }
}, []);
  return (
    <Layout bg_color="dark-blue" logo={logo} menu_text="#fff" single_wrapper="content-ourbeers">
      <OurBeersPage menu_text_color="#fff"/>
    </Layout>
  );
}

export default Our_beers;