import { locale } from './action';
import { fr, gb, de, es, cn, nl, ru } from '../../constants/single_beer_locale'
const InitialState = {
  selected_lang: {}
};

const  Locale = (state = InitialState, action) => {
  switch (action.type) {
    case locale.CHANGE_LANG:
      if(action.data === 'fr'){
        return {
          ...state,
          selected_lang: fr
        }
      }else if(action.data === 'gb') {
        return {
          ...state,
          selected_lang: gb
        }
      }
      else if(action.data === 'es') {
        return {
          ...state,
          selected_lang: es
        }
      }
      else if(action.data === 'de') {
        return {
          ...state,
          selected_lang: de
        }
      }
      else if(action.data === 'nl') {
        return {
          ...state,
          selected_lang: nl
        }
      }
      else if(action.data === 'cn') {
        return {
          ...state,
          selected_lang: cn
        }
      }
      else if(action.data === 'ru') {
        return {
          ...state,
          selected_lang: ru
        }
      }
      default:
        return {
          ...state,
        };
    }
  }

  export default Locale;