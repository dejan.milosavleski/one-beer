
export const locale = {
 
  CHANGE_LANG: 'CHANGE_LANG',
};

export const change_lang = data => async dispatch => {
  dispatch({
    type: locale.CHANGE_LANG,
    data
  });
};
