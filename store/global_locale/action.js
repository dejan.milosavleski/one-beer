
export const global_locale = {
 
  CHANGE_GLOBAL_LANG: 'CHANGE_GLOBAL_LANG',
};

export const change_global_lang = data => async dispatch => {
  dispatch({
    type: global_locale.CHANGE_GLOBAL_LANG,
    data
  });
};
