import { global_locale } from './action';
import { fr, gb } from '../../constants/global_locale'
const InitialState = {
  selected_lang: {}
};

const  Locale = (state = InitialState, action) => {
  switch (action.type) {
    case global_locale.CHANGE_GLOBAL_LANG:
      
      if(action.data === 'fr'){
        return {
          ...state,
          selected_lang: fr
        }
      }else if(action.data === 'gb') {
        return {
          ...state,
          selected_lang: gb
        }
      }
      default:
        return {
          ...state,
        };
    }
  }

  export default Locale;