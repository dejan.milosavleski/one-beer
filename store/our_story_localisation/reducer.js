import { our_story_localisation } from './action';
import { notre_misions_fr } from '../../constants/text/NotreMision';
const InitialState = {
  selected_lang: []
};

const  Localisation_our_story = (state = InitialState, action) => {
  switch (action.type) {
    case our_story_localisation.LOCALISATION_FR:
      console.log(action.payload, 'payload');
      return {
        ...state,
        selected_lang: action.payload
      };
      case our_story_localisation.LOCALISATION_GB:
      return {
        ...state,
        selected_lang: action.payload
      };
      default:
        return {
          ...state,
          selected_lang: notre_misions_fr
        };
    }
  }

  export default Localisation_our_story