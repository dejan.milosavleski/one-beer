export const our_story_localisation = {
  LOCALISATION_GB: 'LOCALISATION_GB',
  LOCALISATION_FR: 'LOCALISATION_FR',
};

export const localisation_fr = data => async dispatch => {
  dispatch({
    type: our_story_localisation.LOCALISATION_FR,
    payload: data
  });
};
export const localisation_gb = data => async dispatch => {
  dispatch({
    type: our_story_localisation.LOCALISATION_GB,
    payload: data
  });
};
