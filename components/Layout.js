import { useEffect } from 'react';
import Head from 'next/head';
import Menu from '../containers/menu/Menu';
import { useDispatch, useSelector } from 'react-redux';
import { is_Spinner_loading_false } from '../store/spinner/action';
import { change_global_lang } from '../store/global_locale/action';
import { useRouter } from 'next/router';
import favicon from '../public/onebeer-favicon-black.svg';
import { useWindowSize } from '../hooks/UseWindowSize';
import { isEmpty } from 'lodash';
import Cookies from 'js-cookie';
import MetaPic from '../styles/assets/images/ONEBEER_6SAVEURS.jpeg';
import CookieConsent from 'react-cookie-consent';
import { replaceBrTag } from '../hooks/useBrTag';
import Link from 'next/link';

const Layout = props => {
  const { bg_color, menu_text, menu_icons, single_beer_logo, single_wrapper } =
    props;
  const spinner = useSelector(state => state.spinner.is_spinner_loading);
  const selected_lang = useSelector(state => state.global_locale.selected_lang);
  const dispatch = useDispatch();
  const router = useRouter();
  const { pathname } = router;
  const windowSize = useWindowSize();
  const lang = Cookies.get('lang');
  const cookie_agreement = useSelector(
    state => state.global_locale.selected_lang.cookie_agreement
  );
  useEffect(() => {
    dispatch(is_Spinner_loading_false());

    if (isEmpty(selected_lang)) {
      dispatch(change_global_lang(lang));
    }
  }, []);

  useEffect(() => {
    if (
      (windowSize.width && windowSize.width <= 768 && pathname === '/') ||
      pathname === '/home' ||
      pathname === '/our-beers'
    ) {
      document.querySelector('body').classList.remove('prevent-scroll');
      document.querySelector('body').classList.add('prevent-scroll');
    } else if (
      (windowSize.width && windowSize.width <= 768 && pathname !== '/') ||
      pathname !== '/home' ||
      pathname !== '/our-beers'
    ) {
      document.querySelector('body').classList.remove('prevent-scroll');
    }
  }, [pathname, windowSize]);

  return (
    <div className={`content-wrapper ${single_wrapper} m-0 p-0 ${bg_color}`}>
      <Head>
        <meta
          name="description"
          content={
            lang === 'fr'
              ? 'Une bière, 6 saveurs, Une histoire sans fin …'
              : 'One beer, 6 flavors, An endless story…'
          }
        />
        <meta
          name="robots"
          content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large"
        />
        <meta property="og:locale" content="fr_FR" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="One Beer" />
        <meta property="og:image" content={MetaPic} />
        <meta
          property="og:description"
          content={
            lang === 'fr'
              ? 'Une bière, 6 saveurs, Une histoire sans fin …'
              : 'One beer, 6 flavors, An endless story…'
          }
        />
        <meta property="og:url" content="https://onebeer.fr/" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="One Beer" />
        <meta name="twitter:image" content="/ONEBEER_6SAVEURS.jpeg" />
        <meta
          name="twitter:description"
          content={
            lang === 'fr'
              ? 'Une bière, 6 saveurs, Une histoire sans fin …'
              : 'One beer, 6 flavors, An endless story…'
          }
        />
        <meta name="twitter:url" content="https://onebeer.fr/" />
        <title>One Beer</title>
        

 <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png"/>
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png"/>
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png"/>
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png"/>
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png"/>
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png"/>
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png"/>
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png"/>
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png"/>
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png"/>
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png"/>
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
<link rel="manifest" href="/manifest.json"/>
<meta name="msapplication-TileColor" content="#ffffff"/>
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png"/>
<meta name="theme-color" content="#ffffff">


</meta>
        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-2J4P9WXED0"
        ></script>
        <script
          dangerouslySetInnerHTML={{
            __html: `
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'G-2J4P9WXED0');
      `,
          }}
        />
      
       
      </Head>
      <CookieConsent
          location="top"
          buttonText={cookie_agreement && cookie_agreement.button_text}
          cookieName="myAwesomeCookieName2"
          style={{
            background: '#fff',
            color: '#080b33',
            width: '100%',
            maxWidth: '100%',
            left: '50%',
            transform: 'translate(-50%, 0)',
            alignItems:'center',
            zIndex: "99999",
            justifyContent: 'space-evenly'
          }}
          buttonWrapperClasses="button-wrapper"
          buttonStyle={{
            color: 'white',
            fontSize: '0.8rem',
            backgroundColor: '#1a2c7c',
            borderRadius: '4px'
          }}
          expires={365}
          enableDeclineButton
          declineButtonText={<> <i
            className="icon close-icon"
          /></>}
        >
          {cookie_agreement && replaceBrTag(cookie_agreement.text, '<br/>', <br />)}
          <Link href="/politique">
            <a style={{ color: '#080b33' }}>
              {lang && lang === 'fr'
                ? 'Politique de Confidentialité'
                : 'Privacy Policy'}
            </a>
          </Link>
        </CookieConsent>
      <Menu
        menu_icons={menu_icons}
        menu_text={menu_text}
        single_beer_logo={single_beer_logo}
      />
      {spinner && (
        <div className="spinner-wrapper rounded-circle">
          <div className="eclipse_spinner"></div>
        </div>
      )}
      <main className="main-content" >{props.children}</main>
    </div>
  );
};

export default Layout;
