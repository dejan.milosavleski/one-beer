import Link from 'next/link';

export const Social_icons = () => {
  const social_icons = [
  
    {
      title: 'instagram-icon',
      slug: 'https://www.instagram.com/onebeer_off',
    },
    // {
    //   title: 'youtube-icon',
    //   slug: '#',
    // },
  ];
 
  return (
    <div className="social-icons">
      {social_icons.map((item, index) => (
        <Link key={index * 12237} href={item.slug}>
          <a>
            <i className={`icon ${item.title}`} />
          </a>
        </Link>
      ))}
    </div>
  );
};

export const Social_blue_icons = () => {
  const social_blue_icons = [
    // {
    //   title: 'youtube-blue-icon',
    //   slug: '#',
    // },
    {
      title: 'instagram-blue-icon',
      slug: 'https://www.instagram.com/onebeer_off',
    },
  
  ];
  return (
    <div className="social-blue-icons">
      {social_blue_icons.map((item, index) => (
        <Link key={index * 127} href={item.slug}>
          <a>
            <i className={`icon ${item.title}`} />
          </a>
        </Link>
      ))}
    </div>
  );
};
