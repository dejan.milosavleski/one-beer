import React from 'react';


function range(min, max) {
    return min + (max - min) * Math.random();
  }

  function round(num, precision) {
    var decimal = Math.pow(10, precision);
    return Math.round(decimal * num) / decimal;
  }

  function weightedRange(to, from, decimalPlaces, weightedRange, weightStrength) {
    if (typeof from === 'undefined' || from === null) {
      from = 0;
    }
    if (typeof decimalPlaces === 'undefined' || decimalPlaces === null) {
      decimalPlaces = 0;
    }
    if (typeof weightedRange === 'undefined' || weightedRange === null) {
      weightedRange = 0;
    }
    if (typeof weightStrength === 'undefined' || weightStrength === null) {
      weightStrength = 0;
    }
  
    var ret;
    if (to == from) {
      return to;
    }
  
    if (weightedRange && Math.random() <= weightStrength) {
      ret = round(
        Math.random() * (weightedRange[1] - weightedRange[0]) + weightedRange[0],
        decimalPlaces
      );
    } else {
      ret = round(Math.random() * (to - from) + from, decimalPlaces);
    }
    return ret;
  }

const ParticleEngine = ({ canvas_id }) => {

    const compositeStyle = 'lighter';
 
    const particleSettings = [
        {
          id: 'small',
          num: 300,
          fromX: 0,
          toX: totalWidth,
          ballwidth: 3,
          alphamax: 0.4,
          areaHeight: 0.5,
          color: '#0cdbf3',
          fill: false,
        },
        {
          id: 'medium',
          num: 100,
          fromX: 0,
          toX: totalWidth,
          ballwidth: 8,
          alphamax: 0.3,
          areaHeight: 1,
          color: '#6fd2f3',
          fill: true,
        },
        {
          id: 'large',
          num: 10,
          fromX: 0,
          toX: totalWidth,
          ballwidth: 30,
          alphamax: 0.2,
          areaHeight: 1,
          color: '#93e9f3',
          fill: true,
        },
      ];
      particleArray = [];
      lights = [
        {
          ellipseWidth: 400,
          ellipseHeight: 100,
          alpha: 0.6,
          offsetX: 0,
          offsetY: 0,
          color: '#6ac6e8',
        },
        {
          ellipseWidth: 350,
          ellipseHeight: 250,
          alpha: 0.3,
          offsetX: -50,
          offsetY: 0,
          color: '#54d5e8',
        },
        {
          ellipseWidth: 100,
          ellipseHeight: 80,
          alpha: 0.2,
          offsetX: 80,
          offsetY: -50,
          color: '#2ae8d8',
        },
      ];
    return (
      <div className="App">
      
      </div>
    );
  }
  
  export default ParticleEngine;