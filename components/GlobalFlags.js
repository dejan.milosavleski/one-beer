import { useDispatch } from 'react-redux';
import { change_global_lang } from '../store/global_locale/action';
import { change_lang } from '../store/single_beer_locale/action'
import Cookies from 'js-cookie';

const GlobalFlags = () => {
  const dispatch = useDispatch();

  const changeLang = lang => {
    Cookies.remove('lang');

    dispatch(change_global_lang(lang));
    dispatch(change_lang(lang));
    Cookies.set('lang', lang);
  };
  return (
    <div className="global-flags">
      <i className="icon fr_flag-icon" onClick={() => changeLang('fr')} />
      <i className="icon gb_flag-icon" onClick={() => changeLang('gb')} />
    </div>
  );
};

export default GlobalFlags;
