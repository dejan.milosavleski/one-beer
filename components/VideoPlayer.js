import { useState, useEffect, useRef } from 'react';
import { useWindowSize } from '../hooks/UseWindowSize';
import proba from '../public/proba_zamena.png';

const VideoPlayer = ({ url, setIsVideoFinished, setIsVideoStarted }) => {
  const [isPlay, setIsPlay] = useState(false);
  const windowSize = useWindowSize();
  const mrRef = useRef()
  // useEffect(() => {
  //   const myVideo = myRef.current;
  //   if (windowSize.width <= 576) {
  //     myVideo.play();
  //   }
  // }, [windowSize.width]);

  // const myRef = useRef();
  // useEffect(() => {
  //   const myVideo = myRef.current;
  //   if (isVideoPlaying) {
  //     myVideo.play();
  //   } else {
  //     myVideo.pause();
  //     myVideo.currentTime = 0;
  //   }
  // }, [isVideoPlaying]);

  return (
    <>
      <video
      // ref={myRef}
        muted
        autoPlay={true}
        playsInline 
        preload="auto"
        // poster={proba}
        onPlay={()=> setIsVideoStarted(true) }
     
        onEnded={() => setIsVideoFinished(true)}
        id="myVideo"
        className="video-insert"
        
      >
        <source src={url} type="video/mp4" />
      </video>
    </>
  );
};

export default VideoPlayer;
