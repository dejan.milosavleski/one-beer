import React from 'react';

const Toast = ({ message, right_px }) => {
  return (
    <div
      className="toast-msg"
      style={{
        right: message === '' ? '-360px' : `${right_px}`,
        transition: 'all .5s',
      }}
    >
      <h2>{message}</h2>
    </div>
  );
};

export default Toast;
